# X-ray simulation
The project is built upon the mathematical model that was developed by Prof. Dr. Robert Heß.

## Technologies
Project is created with:
* C++ 20
* wxWidgets 3.1.6
* OpenGL 3.3
* GLM
* GLEW 2.1.0
* GLAD

## Setup

OpenGL more or less comes with all major desktop platforms and usually does not require manual installation. However you need to ensure that you have latest driver for your graphics 
hardware. To view information about your current OpenGL accelerator first install OpenGL Extensions Viewer and then run it. 

Some platforms have a C++ compiler , like GNU, already installed. To check if GNU compiler is already installed on your machine run:
```
g++ --version
```
You can also install Visual Studio (IDE) which comes with a compiler.

Please refer to the official documentation on how to install wxWidgets.

After downloading and setting up dependencies, you need to include them:

![include](/Integration/screenshots/include.png?raw=true)

Then link necessary OpenGL, GLEW and wxWidggets files:

![include](/Integration/screenshots/link.png?raw=true)

![include](/Integration/screenshots/link2.png?raw=true)

## Example of use:

On application start a user first has to enter or select general input setting in the general dialog window:

![include](/Integration/screenshots/general_dialog.png?raw=true)

Main frame window will depict phantom and x-ray system depending on general dialog settings:

![include](/Integration/screenshots/rad_male.png?raw=true)

In the screenshot above male phantom and rad system type were selected.
When an uninterrupted simulation call is made two more windows will be added in order to illustrate list of organ and tissues with corresponding dose and accurracy.

![include](/Integration/screenshots/simulation_results.png?raw=true)

It is possible to visualize certain organ(s) of a human body by checking them in the list of organs window, that will pop up when `human icon` is being pressed. 
By default all organ will be displayed. In the following example only ribs were selected:

![include](/Integration/screenshots/ribs.png?raw=true)

It is possible to move camera forward, left, backwards and to the right in the 3D space using `wasd` keys. To move camera up and down `y` and `x` keys have to be pressed. 
You can also move and rotate the phantom with simulaions system. To move left click with mouse on the phantom and then drag it using arrows (to rotate right click).

![include](/Integration/screenshots/movement_arrows.png?raw=true) ![include](/Integration/screenshots/rotation_arrows.png?raw=true)

You can change general and simulation specific settings in the `Settings` menu:

![include](/Integration/screenshots/simulation_dialog_settings.png?raw=true)

Furthermore, in the `File` menu it is possible to save and import simulation setting. Importing the settings can be helpful if you want to restore previous setting and simulate it again.

![include](/Integration/screenshots/file_menu.png?raw=true)

