#ifndef APP_H_INCLUDED
#define APP_H_INCLUDED

#include <wx/wx.h>
#include <wx/display.h>
#include <wx/event.h>
#include "Controllers/Presenter.h"

class App : public wxApp
{
public:
	App();
	~App();
	virtual bool OnInit();

private:
	Presenter* presenter = nullptr;
};

#endif // APP_H_INCLUDED