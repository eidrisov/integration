#version 330 core

layout (location = 0) in vec3 pos;
out vec4 vert_color;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform vec4 color;

void main()
{
	vert_color = color;
	gl_Position = projection * view * model * vec4(pos.x, pos.y, pos.z, 1.0);
}
