#version 330 core

layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 normal;
out vec4 vert_color;
out vec3 Normal;
out vec3 FragPos;

uniform vec3 phantomCenterCoor;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 initialRotationMatrix;
uniform mat4 phantomRotationMatrix;
uniform vec4 color;

void main()
{
	vert_color = color;
	// gl_Position = projection * view * model * initialRotationMatrix * vec4(pos.x - phantomCenterCoor.x, pos.y - phantomCenterCoor.y, pos.z - phantomCenterCoor.z, 1.0);
	gl_Position = projection * view * model * inverse(initialRotationMatrix * phantomRotationMatrix) * vec4(pos.x - phantomCenterCoor.x, pos.y - phantomCenterCoor.y, pos.z - phantomCenterCoor.z, 1.0);
	FragPos = vec3(model * initialRotationMatrix * phantomRotationMatrix * vec4(pos, 1.0));
	// Normal = vec3(transform * vec4(normal, 0.0));
	Normal = vec3(transpose(initialRotationMatrix * phantomRotationMatrix) * vec4(normal, 0.0));
}