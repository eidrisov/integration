#version 330 core

layout (location = 0) in vec3 pos;
out vec4 vert_color;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 initialRotationMatrix;
uniform mat4 rotationMatrix;
uniform mat4 rotaionMatrixAroundPhantom = mat4(1.0f);
uniform vec4 color;

void main()
{
	vert_color = color;
	gl_Position = projection * view * model * inverse(initialRotationMatrix * rotationMatrix * rotaionMatrixAroundPhantom ) * vec4(pos.x, pos.y, pos.z, 1.0);
}