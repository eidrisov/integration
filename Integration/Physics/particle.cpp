//=============================================================================
// Filename: Particle.cpp
// Author: Robert Hess
// Version: 1.01
// Date: Jun 24th 2020
// Description: Class to handle a particle for Monte-Carlo simulation.
//=============================================================================

#define _USE_MATH_DEFINES
#include <math.h>
#include "particle.h"


//=============================================================================
/// Does nothing.
cParticle::cParticle()
//=============================================================================
{
}


//=============================================================================
/// Copies all attributes of the given particle.
cParticle::cParticle(
    const cParticle &part   ///< [in] particle to be copied
)
//=============================================================================
{
    copy(part);
}


//=============================================================================
/// Does nothing.
cParticle::~cParticle()
//=============================================================================
{
}


//=============================================================================
/// Assigns all attributes of the given particle to the current particle.
cParticle &cParticle::operator=(
    const cParticle &part   ///< [in] particle to be copied
)
//=============================================================================
{
    copy(part);
    return *this;
}


//=============================================================================
void cParticle::setDirection(
    double x,  ///< [in] cosine between beam and positive x-axis
    double y,  ///< [in] cosine between beam and positive y-axis
    double z   ///< [in] cosine between beam and positive z-axis
)
//=============================================================================
{
	// set direction
	u[0] = x;
	u[1] = y;
	u[2] = z;
}


//=============================================================================
void cParticle::setDirection(
    double u[3]   ///< [in] new direction unit vector
)
//=============================================================================
{
	// set direction
	this->u[0] = u[0];
	this->u[1] = u[1];
	this->u[2] = u[2];
}


//=============================================================================
void cParticle::setDirection(
    double polar,    ///< [in] new polar direction in rad
    double azimuth   ///< [in] new azimuth direction in rad
)
//=============================================================================
{
	// set direction (not efficient...)
	setDirection(sin(polar)*cos(azimuth), sin(polar)*sin(azimuth), cos(polar));
}


//=============================================================================
void cParticle::setPosition(
    double x,  ///< [in] x-coordinate for the new position of the particle
    double y,  ///< [in] y-coordinate for the new position of the particle
    double z   ///< [in] z-coordinate for the new position of the particle
)
//=============================================================================
{
	// set position
	p[0] = x;
	p[1] = y;
	p[2] = z;
}


//=============================================================================
void cParticle::setPosition(
    double p[3]   ///< [in] Cartesian coordinate for new position of the particle as vector with three elements
)
//=============================================================================
{
	// set position
	this->p[0] = p[0];
	this->p[1] = p[1];
	this->p[2] = p[2];
}


//=============================================================================
void cParticle::setVoxelIndex(
    int index[3]   ///< [in] index of voxel as vector with three elements
)
//=============================================================================
{
	voxelIndex[0] = index[0];
	voxelIndex[1] = index[1];
	voxelIndex[2] = index[2];
}


//=============================================================================
void cParticle::setOrganIndex(
    uint8_t index   ///< [in] new organ index of the voxel the particle is in
)
{
    organIndex = index;
}


//=============================================================================
void cParticle::setEnergy(
    double value   ///< [in] new particle energy in keV
)
//=============================================================================
{
	// set energy
	energy = value;
}


//=============================================================================
void cParticle::setParticleType(
    tParticleType newType   ///< [in] new particle type
)
//=============================================================================
{
	// set particle type
	type = newType;
}


//=============================================================================
void cParticle::setAtomicNumber(
    unsigned newZ   ///< [in] new atomic number for interactions
)
//=============================================================================
{
	// set atomic number
	Z = newZ;
}


//=============================================================================
void cParticle::setSubshell(
    unsigned newDesig   ///< [in] new sub-shell designator
)
//=============================================================================
{
	// set subshell ENDL designator
	subshell = newDesig;
}


//=============================================================================
void cParticle::getDirection(
    double &x,  ///< [out] x-component of direction unit vector
    double &y,  ///< [out] y-component of direction unit vector
    double &z   ///< [out] z-component of direction unit vector
) const
//=============================================================================
{
	// copy direction to parameter
	x = u[0];
	y = u[1];
	z = u[2];
}


//=============================================================================
void cParticle::getDirection(
    double u[3]  ///< [out] direction unit vector as vector with three elements
) const
//=============================================================================
{
	// copy direction to parameter
	u[0] = this->u[0];
	u[1] = this->u[1];
	u[2] = this->u[2];
}


//=============================================================================
void cParticle::getDirection(
    double &polar,   ///< [out] polar-angle of particle direction in rad
    double &azimuth  ///< [out] azimuth-angle of particle direction in rad
) const
//=============================================================================
{
	// if not parallel to z-axis
	if (u[0] * u[0] + u[1] * u[1] > 0) {
		if (u[2] >= 1) polar = 0;
		else if (u[2] <= -1) polar = M_PI;
		else polar = acos(u[2]);
		azimuth = atan2(u[1], u[0]);
	}

	// if parallel to z-axis
	else {
		polar = u[2] >= 0 ? 0 : M_PI;
		azimuth = 0;
	}
}


//=============================================================================
void cParticle::getPosition(
    double &x,  ///< [out] x-coordinate of particle position
    double &y,  ///< [out] y-coordinate of particle position
    double &z   ///< [out] z-coordinate of particle position
) const
//=============================================================================
{
	// copy position to parameter
	x = p[0];
	y = p[1];
	z = p[2];
}


//=============================================================================
void cParticle::getPosition(
    double p[3]  ///< [out] particle position as vector with three elements
) const
//=============================================================================
{
	// copy position to parameter
	p[0] = this->p[0];
	p[1] = this->p[1];
	p[2] = this->p[2];
}


//=============================================================================
void cParticle::getVoxelIndex(
    int index[3]  ///< [out] index of the voxel the particle is in as vector with three elements
) const
//=============================================================================
{
	index[0] = voxelIndex[0];
	index[1] = voxelIndex[1];
	index[2] = voxelIndex[2];
}


//=============================================================================
/// \return organ index of the voxel the particle is in
uint8_t cParticle::getOrganIndex() const
//=============================================================================
{
    return organIndex;
}


//=============================================================================
/// \return Particle energy in keV.
double cParticle::getEnergy() const
//=============================================================================
{
	// return particle energy
	return energy;
}


//=============================================================================
/// \return current type of particle, see enum cParticle::tParticleType
cParticle::tParticleType cParticle::getParticleType() const
//=============================================================================
{
	// retrun type of particle
	return type;
}


//=============================================================================
/// \return atomic number of element the interaction takes place with
unsigned cParticle::getAtomicNumber() const
//=============================================================================
{
	// return atomic number
	return Z;
}


//=============================================================================
/// \return ENDL atomic subshell designator for ionized atom
unsigned cParticle::getSubshell() const
//=============================================================================
{
	// return ENDL subshell designator
	return subshell;
}


//=============================================================================
/// \return distance between point (x,y,z)^T and current position of particle
double cParticle::dist2Point(
    double x,  ///< [in] x-coordinate of point to evaluate distance to
    double y,  ///< [in] y-coordinate of point to evaluate distance to
    double z   ///< [in] z-coordinate of point to evaluate distance to
) const
//=============================================================================
{
	// evaluate and retrun distance to the given point
	return sqrt((p[0] - x)*(p[0] - x) + (p[1] - y)*(p[1] - y) + (p[2] - z)*(p[2] - z));
}


//=============================================================================
/// \return distance between point (x,y,z)^T and current position of particle
double cParticle::dist2Point(
    double p[3]   ///< [in] point to evaluate distance to as vector with three elements
) const
//=============================================================================
{
	// retrun distance to the given point
	return dist2Point(p[0], p[1], p[2]);
}


//=============================================================================
/// \return minimum distance of particle beam to given point
double cParticle::minDist2Point(
    double x,  ///< [in] x-coordinate of point to find minimum distance to
    double y,  ///< [in] y-coordinate of point to find minimum distance to
    double z   ///< [in] z-coordinate of point to find minimum distance to
) const
//=============================================================================
{
	double lambda;	// parameter on line

	// evaluate parameter for point on line closest to parameter
	lambda = -(u[0] * (p[0] - x) + u[1] * (p[1] - y) + u[2] * (p[2] - z));

	// evalaute vector of minimum distance to line
	x -= p[0] + lambda*u[0];
	y -= p[1] + lambda*u[1];
	z -= p[2] + lambda*u[2];

	// evalauate and return absolute of vector
	return sqrt(x*x + y*y + z*z);
}


//=============================================================================
/// \return minimum distance of particle beam to given point
double cParticle::minDist2Point(
    double p[3]   ///< [in] point to find minimum distance to as vector with three elements
) const
//=============================================================================
{
	// retrun minimum distance to point
	return minDist2Point(p[0], p[1], p[2]);
}


//=============================================================================
/// \return minimum distance of particle beam to origin of coordinate system
double cParticle::minDist2Origin() const
//=============================================================================
{
	// return minimum distance to origin
	return minDist2Point(0, 0, 0);
}


//=============================================================================
void cParticle::move(
    double d   ///< [in] distance to move particle
)
//=============================================================================
{
	// move particle by distance d (u must be a unit vector, i.e. |u| = 1)
	p[0] += d * u[0];
	p[1] += d * u[1];
	p[2] += d * u[2];
}


//=============================================================================
void cParticle::changeDirection(
    double x,  ///< [in] cosine between beam and positive x-axis
    double y,  ///< [in] cosine between beam and positive y-axis
    double z   ///< [in] cosine between beam and positive z-axis
)
//=============================================================================
{
	double ut[3];	// teomporary direction vector
	double r;		// radius on xy-plane

	// if forward
	if (u[2] >= 1) {
		u[0] = x;
		u[1] = y;
		u[2] = z;
	}

	// if backward
	else if (u[2] <= -1) {
		u[0] = -x;
		u[1] =  y;
		u[2] = -z;
	}

	// if not parallel to z-axis
	else {
		// radius on xy-plane
		r = sqrt(u[0] * u[0] + u[1] * u[1]);

		// rotate direction of parameter by current direction
		ut[0] = u[0];
		ut[1] = u[1];
		ut[2] = u[2];
		u[0] = ut[0] * ut[2] / r * x
			- ut[1] / r * y
			+ ut[0] * z;
		u[1] = ut[1] * ut[2] / r * x
			+ ut[0] / r * y
			+ ut[1] * z;
		u[2] = -r * x
			+ ut[2] * z;
	}
}


//=============================================================================
void cParticle::changeDirection(
    double scatter[3]   ///< [in] direction unit vector as vactor with three elements
)
//=============================================================================
{
	// change direction
	changeDirection(scatter[0], scatter[1], scatter[2]);
}


//=============================================================================
void cParticle::changeDirection(
    double polar,    ///< [in] polar angle for change of direction in rad
    double azimuth   ///< [in] azimuth angle for change of direction in rad
)
//=============================================================================
{
	// change direction (not very efficient)
	changeDirection(sin(polar)*cos(azimuth), sin(polar)*sin(azimuth), cos(polar));
}


//=============================================================================
void cParticle::copy(
    const cParticle &part   ///< [in] particle to be copied
)
//=============================================================================
{
	// copy position
	p[0] = part.p[0];
	p[1] = part.p[1];
	p[2] = part.p[2];

	// copy direction
	u[0] = part.u[0];
	u[1] = part.u[1];
	u[2] = part.u[2];

	// copy voxel index
	voxelIndex[0] = part.voxelIndex[0];
	voxelIndex[1] = part.voxelIndex[1];
	voxelIndex[2] = part.voxelIndex[2];
	organIndex = part.organIndex;

	// copy energy and type
	energy = part.energy;
	type = part.type;

	// copy atomic number and subshell designator
	Z = part.Z;
	subshell = part.subshell;
}

