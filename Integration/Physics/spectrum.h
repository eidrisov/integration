//=============================================================================
// Filename: spectrum.h
// Author: Robert Hess
// Version: 1.0
// Date: August 26th 2017
// Description: A class for calculations with x-ray spectra.
//=============================================================================

#pragma once
#ifndef HEADER_SPECTRUM_INCLUDED
#define HEADER_SPECTRUM_INCLUDED

#include <vector>
#include <string>

class cSpectrum
{
private:
	std::vector<double> spec;
public:
	cSpectrum();
	cSpectrum(const cSpectrum & newSpec);
	~cSpectrum();
	unsigned size();
	void resize(unsigned size);
	double &operator[](unsigned index);
	cSpectrum &operator=(double value);
	cSpectrum &operator=(const std::vector<double> & newSpec);
	cSpectrum &operator=(const cSpectrum & newSpec);
	cSpectrum &operator+=(const cSpectrum& summand);
	cSpectrum &operator*=(const cSpectrum& factor);
	cSpectrum &operator*=(double factor);
	cSpectrum &exp();
	double sum();
	void readSpectrum(const std::string &filename, double tubeVoltage, double &minEnergy, std::string &spectrumName);
	static void getVoltages(const std::string &filename, std::string &spectrumName, std::vector<double> &voltage);
};

#endif // #ifndef HEADER_SPECTRUM_INCLUDED
