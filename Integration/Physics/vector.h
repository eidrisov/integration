//=============================================================================
// Filename: vector.h
// Author: Robert Hess
// Version: 1.02
// Date: June 7th 2020
// Description: template classes for 1d-, 2d- and 3d-vectors
//=============================================================================

#ifndef CLASS_VECTOR_INCLUDED
#define CLASS_VECTOR_INCLUDED

#ifndef NULL
#define NULL 0
#endif

namespace vec {

	//=========================================================================
	//=========================================================================
	//=========================================================================
	/// \brief Class for a 1d vector field.
    ///
	/// It does a similar job as the vector class of standard C++. However, it belongs to a set of classes for 1d, 2d and 3d vectors.
	template<class T> class vector1d {
	public:
		T *data;  ///< pointer for data structure
		int nx;     ///< size of vector for first dimension

		//=====================================================================
		/// Default constructor initializing an empty vector.
		vector1d()
		{
			// initialize data pointer
			data = NULL;
			nx = 0;
		};

		//=====================================================================
		/// Copy-constructor copying the parameter *vector* into the current vector. The size and all vector elements are copied.
		vector1d(
		    const vector1d<T> &vector)  ///< [in] source vector to be copied
		{
			data = NULL;
			nx = 0;
			if(set(vector)) throw;
		}

		//=====================================================================
		/// Constructor defining the size of the vector.
		vector1d(
		    int nx) ///< [in] size of first dimension
		{
			// initialize data pointer
			data = NULL;
			this->nx = nx;

			// get memory for 2d vector
			if(setSize(nx)) throw;
		};

		//=====================================================================
		/// Destructor to free memory.
		~vector1d()
		{
			// free memory
			setSize(0);
		};

		//=====================================================================
		/// Sets the size of the 1d-vector.
		bool setSize(
		    int nx,                 ///< [in] number of elements of the vector
		    bool keepData=false)    ///< [in] true to keep previous data
		{
			bool error=false;	// error flag
			T *tmp=NULL;		// temporary pointer for new data
			int i;				// local index variable

			// check for valid size
			error = error || nx<0;

			// if old data shall not be kept, free previous memory
			if(!keepData && data) {
				delete[] data;
				data = NULL;
				this->nx = 0;
			}

			// get memory for data only for valid size
			if(nx>0) {
				error = error || (tmp=new T[nx])==NULL;
			}

			// if old data shall be kept, copy data
			if(!error && keepData) {
				try {
					i = (this->nx<nx?this->nx:nx)-1;
					for(i=(this->nx<nx?this->nx:nx)-1; i>=0; i--)
						tmp[i] = data[i];
				} catch(...) {
					error = true;
				}
				delete[] data;
				data = tmp;
				this->nx = nx;
			}

			// if old data shall not be kept, copy pointer only
			if(!error && !keepData) {
				data = tmp;
				this->nx = nx;
			}

			return error;
		};

		//=====================================================================
		/// Gives direct access to the vector elements to treat it as a non-dynamic 1d-vector.
		T &operator[](
		    int ix) ///< [in] index to access vector element
		{
			return data[ix];
		};

		//=====================================================================
		/// Append an element to the end of the vector.
		bool append(
		    const T &item)  ///< [in] element to append at end of vector
		{
			bool error=false;	// error flag

			error = setSize(nx+1, true);
			try {
				if(!error) data[nx-1] = item;
			} catch(...) {
				error = true;
			}

			return error;
		};

		//=====================================================================
		/// Copies the parameter *value* to all vector elements.
		bool set(
		    const T &value) ///< [in] value to be copied to all vector elements
		{
			int x;				// local index variables
			bool error=false;	// error flag

			try {
				for(x=0; x<nx; x++) {
					data[x] = value;
				}
			} catch(...) {
				error = true;
			}

			return error;
		}

		//=====================================================================
		/// Sets a vector given as parameter to the current vector. The size and all vector elements are copied.
		bool set(
		    const vector1d &vector) ///< [in] source vector to be copied
		{
			bool error=false;	// error flag
			int i;				// local index variable

			// check for valid vector
			error = error || vector.data==NULL;
			error = error || vector.nx<0;

			// get memory for new vector
			error = error || setSize(vector.nx);

			// copy content
			if(!error) {
				try {
					for(i=0; i<nx; i++) data[i] = vector.data[i];
				} catch(...) {
					error = true;
				}
			}

			return error;
		};

		//=====================================================================
		/// Sets a vector given as parameter to the current vector. The size and all vector elements are copied.
		vector1d &operator=(
		    const vector1d &vector) ///< [in] source vector to be copied
		{
			if(set(vector)) throw;

			return *this;
		}
	};

	//=========================================================================
	//=========================================================================
	//=========================================================================
	/// Class for a 2d vector field
	template<class T> class vector2d {
	public:
		T **data;   ///< pointer for data structure
		int nx;     ///< size of vector for first dimension
		int ny;     ///< size of vector for second dimension

		//=====================================================================
		/// Default constructor initializing an empty vector.
		vector2d()
		{
			// initialize data pointer
			data = NULL;
			nx = ny = 0;
		};

		//=====================================================================
		/// Copy-constructor copying the parameter *vector* into the current vector. The size and all vector elements are copied.
		vector2d(
		    const vector2d<T> &vector)  ///< [in] source vector to be copied
		{
			data = NULL;
			nx = ny = 0;
			if(set(vector)) throw;
		}

		//=====================================================================
		/// Constructor defining the size of the vector.
		vector2d(
		    int nx, ///< [in] size of first dimension
		    int ny) ///< [in] size of second dimension
		{
			// initialize data pointer
			data = NULL;
			this->nx = this->ny = 0;

			// get memory for 2d vector
			if(setSize(nx, ny)) throw;
		};

		//=====================================================================
		/// Destructor to free memory.
		~vector2d()
		{
			// free memory
			setSize(0, 0);
		};

		//=====================================================================
		/// Sets the size of the 2d-vector.
		bool setSize(
		    int nx, ///< [in] size of first dimension
		    int ny) ///< [in] size of second dimension
		{
			bool error=false;	// error flag
			int i;				// local index variable

			// free previous memory
			if(data) {
				if(*data) delete[] *data;
				delete[] data;
				data = NULL;
				this->nx = this->ny = 0;
			}

			// only for valid size
			if(nx>0 && ny>0) {
				// get memory for pointer array
				error = error || (data=new T*[nx])==NULL;

				// get memory for content
				error = error || (*data=new T[nx*ny])==NULL;

				// initialize pointer array
				for(i=1; !error&&i<nx; i++) data[i] = data[i-1]+ny;

				// keep size
				if(!error) {
					this->nx = nx;
					this->ny = ny;
				}
			}

			return error;
		};

		//=====================================================================
		/// Gives direct access to the vector elements. A second pair of square brackets may be added to treat it as a non-dynamic 2d-vector.
		T *operator[](
		    int ix) const ///< [in] index for first dimension
		{
			return data[ix];
		};

		//=====================================================================
		/// Copies the parameter *value* to all vector elements.
		bool set(
		    const T &value) ///< [in] value to be copied to all vector elements
		{
			int x, y;			// local index variables
			bool error=false;	// error flag

			try {
				for(x=0; x<nx; x++) {
					for(y=0; y<ny; y++) {
						data[x][y] = value;
					}
				}
			} catch(...) {
				error = true;
			}

			return error;
		}

		//=====================================================================
		/// Sets a vector given as parameter to the current vector. The size and all vector elements are copied.
		bool set(
		    const vector2d &vector) ///< [in] source vector to be copied
		{
			bool error=false;	// error flag
			int x, y;			// local index variables

			// check for valid vector
			error = error || vector.data==NULL;

			// get memory for new vector
			error = error || setSize(vector.nx, vector.ny);

			// copy content
			if(!error) {
				try {
					for(x=0; x<nx; x++)
						for(y=0; y<ny; y++)
							data[x][y] = vector.data[x][y];
				} catch(...) {
					error = true;
				}
			}

			return error;
		};

		//=====================================================================
		/// Add parameter *value* to all vector elements.
		bool add(
		    const T &value) ///< [in] summand to be added to all vector elements
		{
			int x, y;			// local index variables
			bool error=false;	// error flag

			try {
				for(x=0; x<nx; x++) {
					for(y=0; y<ny; y++) {
						data[x][y] += value;
					}
				}
			} catch(...) {
				error = true;
			}

			return error;
		}

		//=====================================================================
		/// Multiply all elements of the vector by parameter *value*.
		bool multiply(
		    const T &value) ///< [in] factor to multiply all elments with
		{
			int x, y;			// local index variables
			bool error=false;	// error flag

			try {
				for(x=0; x<nx; x++) {
					for(y=0; y<ny; y++) {
						data[x][y] *= value;
					}
				}
			} catch(...) {
				error = true;
			}

			return error;
		}

		//=====================================================================
		/// Cut vector to the given limits. The new vector includes the lower and excludes the upper limit.
		bool cut(
		    int x1, ///< [in] lower limit for first dimension
		    int y1, ///< [in] lower limit for second dimension
		    int x2, ///< [in] upper limit for first dimension
		    int y2) ///< [in] upper limit for second dimension
		{
			bool error=false;	// error flag
			vector2d<T> cut;	// new image
			int x, y;			// local index variables

			// adjust limits
			if(x1<0) x1 = 0;
			if(y1<0) y1 = 0;
			if(x2>nx) x2 = nx;
			if(y2>ny) y2 = ny;

			// swap limits if required
			if(x1>x2) {
				x = x1;
				x1 = x2;
				x2 = x;
			}
			if(y1>y2) {
				y = y1;
				y1 = y2;
				y2 = y;
			}

			// get memory for new image
			error = error || cut.setSize(x2-x1, y2-y1);

			// copy elements
			try {
				for(x=0; !error && x<cut.nx; x++) {
					for(y=0; y<cut.ny; y++) {
						cut.data[x][y] = data[x1+x][y1+y];
					}
				}
			} catch(...) {
				error = true;
			}

			// copy new image over old one
			error = error || set(cut);

			// return error flag
			return error;
		}

		//=====================================================================
		/// Shift all elements of the vector by *x0* and *y0*. The gaps are filled by parameter *fill*.
		bool shift(
		    int x0,         ///< [in] shift in positive *x*-direction
		    int y0,         ///< [in] shift in positive *y*-direction
		    const T &fill)  ///< [in] value to fill the gaps
		{
			bool error=false;	// error flag
			int x, y;			// local index variables

			// check shift range
			error = error || x0>nx || x0<-nx;
			error = error || y0>ny || y0<-ny;

			// if no shift, return
			if(x0==0 && y0==0) return false;

			// shift negative x and negative y
			if(!error && x0<0 && y0<0) {
				try {
					for(y=0; y<ny+y0; y++) {
						// shift line
						for(x=0; x<nx+x0; x++)
							data[x][y] = data[x-x0][y-y0];
						// fill rest of line
						for(x=nx+x0; x<nx; x++)
							data[x][y] = fill;
					}
					// fill all other lines
					for(y=ny+y0; y<ny; y++) {
						for(x=0; x<nx; x++)
							data[x][y] = fill;
					}
				} catch(...) {
					error = true;
				}
			}

			// shift negative x and positive y
			if(!error && x0<0 && y0>=0) {
				try {
					for(y=ny-1; y>=y0; y--) {
						// shift line
						for(x=0; x<nx+x0; x++)
							data[x][y] = data[x-x0][y-y0];
						// fill rest of line
						for(x=nx+x0; x<nx; x++)
							data[x][y] = fill;
					}
					// fill all other lines
					for(y=y0-1; y>=0; y--) {
						for(x=0; x<nx; x++)
							data[x][y] = fill;
					}
				} catch(...) {
					error = true;
				}
			}

			// shift positive x and negative y
			if(!error && x0>=0 && y0<0) {
				try {
					for(y=0; y<ny+y0; y++) {
						// shift line
						for(x=nx-1; x>=x0; x--)
							data[x][y] = data[x-x0][y-y0];
						// fill rest of line
						for(x=x0-1; x>=0; x--)
							data[x][y] = fill;
					}
					// fill all other lines
					for(y=ny+y0; y<ny; y++) {
						for(x=0; x<nx; x++)
							data[x][y] = fill;
					}
				} catch(...) {
					error = true;
				}
			}

			// shift positive x and positive y
			if(!error && x0>=0 && y0>=0) {
				try {
					for(y=ny-1; y>=y0; y--) {
						// shift line
						for(x=nx-1; x>=x0; x--)
							data[x][y] = data[x-x0][y-y0];
						// fill rest of line
						for(x=x0-1; x>=0; x--)
							data[x][y] = fill;
					}
					// fill all other lines
					for(y=y0-1; y>=0; y--) {
						for(x=0; x<nx; x++)
							data[x][y] = fill;
					}
				} catch(...) {
					error = true;
				}
			}

			return error;
		}

		//=====================================================================
		/// Copies the parameter *value* to all vector elements.
		vector2d &operator=(
		    const T &value) ///< [in] value to be copied to all vector elements
		{
			if(set(value)) throw;

			return *this;
		}

		//=====================================================================
		/// Assigns a vector to the current vector. The size and all vector elements are copied.
		vector2d &operator=(
		    const vector2d &vector) ///< [in] source vector to be copied
		{
			if(set(vector)) throw;

			return *this;
		}

		//=====================================================================
		/// Add parameter *value* to all vector elements.
		vector2d &operator+=(
		    const T &value) ///< [in] summand to be added to all vector elements
		{
			if(add(value)) throw;

			return *this;
		}

		//=====================================================================
		/// Multiply all elements of the vector by parameter *value*.
		vector2d &operator*=(
		    const T &value) ///< [in] factor to multiply all elments with
		{
			if(multiply(value)) throw;

			return *this;
		}
	};

	//=========================================================================
	//=========================================================================
	//=========================================================================
	/// Class for a 3d vector field
	template<class T> class vector3d {
	public:
		T ***data;  ///< pointer for data structure
		int nx;     ///< size of vector for first dimension
		int ny;     ///< size of vector for second dimension
		int nz;     ///< size of vector for third dimension

		//=====================================================================
		/// Default constructor initializing an empty vector.
		vector3d()
		{
			// initialize data pointer
			data = NULL;
			nx = ny = nz = 0;
		};

		//=====================================================================
		/// Copy-constructor copying the parameter *vector* into the current vector. The size and all vector elements are copied.
		vector3d(
		    const vector3d<T> &vector)  ///< [in] source vector to be copied
		{
			data = NULL;
			nx = ny = nz = 0;
			if(set(vector)) throw;
		}

		//=====================================================================
		/// Constructor defining the size of the vector.
		vector3d(
		    int nx, ///< [in] size of first dimension
		    int ny, ///< [in] size of second dimension
		    int nz) ///< [in] size of third dimension
		{
			// initialize data pointer
			data = NULL;
			this->nx = this->ny = this->nz = 0;

			// get memory for 2d vector
			if(setSize(nx, ny, nz)) throw;
		};

		//=====================================================================
		/// Destructor to free memory.
		~vector3d()
		{
			// free memory
			setSize(0, 0, 0);
		};

		//=====================================================================
		/// Sets the size of the 3d-vector.
		bool setSize(
		    int nx, ///< [in] size of first dimension
		    int ny, ///< [in] size of second dimension
		    int nz) ///< [in] size of third dimension
		{
			bool error=false;	// error flag
			int i, j;			// local index variables

			// free previous memory
			if(data) {
				if(*data) {
					if(**data) delete[] **data;
					delete[] *data;
				}
				delete[] data;
				data = NULL;
				this->nx = this->ny = this->nz = 0;
			}

			// only for valid size
			if(nx>0 && ny>0 && nz>0) {
				// get memory for pointer pointer array
				error = error || (data=new T**[nx])==NULL;

				// get memory for pointer pointer arrays
				error = error || (*data=new T*[nx*ny])==NULL;

				// get memory for content
				error = error || (**data=new T[nx*ny*nz])==NULL;

				// initialize pointer arrays
				for(i=1; !error&&i<nx; i++) {
					data[i] = data[i-1]+ny;
					data[i][0] = data[i-1][0]+ny*nz;
				}
				for(i=0; !error&&i<nx; i++) {
					for(j=1; j<ny; j++) data[i][j] = data[i][j-1]+nz;
				}

				// keep size
				if(!error) {
					this->nx = nx;
					this->ny = ny;
					this->nz = nz;
				}
			}

			return error;
		};

		//=====================================================================
		/// Gives direct access to the vector elements. A second and third pair of square brackets may be added to treat it as a non-dynamic 3d-vector.
		T **operator[](
		    int ix) const ///< [in] index for first dimension
		{
			return data[ix];
		};

		//=====================================================================
//		T &operator[](int i[3])
//		{
//			return data[i[0]][i[1]][i[2]];
//		};

		//=====================================================================
		/// Copies the parameter *value* to all vector elements.
		bool set(
		    const T &value) ///< [in] value to be copied to all vector elements
		{
			int x, y, z;		// local index variables
			bool error=false;	// error flag

			try {
				for(x=0; x<nx; x++) {
					for(y=0; y<ny; y++) {
						for(z=0; z<nz; z++) data[x][y][z] = value;
					}
				}
			} catch(...) {
				error = true;
			}

			return error;
		}

		//=====================================================================
		/// Sets a vector given as parameter to the current vector. The size and all vector elements are copied.
		bool set(
		    const vector3d<T> &vector) ///< [in] source vector to be copied
		{
			bool error=false;	// error flag
			int x, y, z;		// local index variables

			// check for valid vector
			error = error || vector.data==NULL;

			// get memory for new vector
			error = error || setSize(vector.nx, vector.ny, vector.nz);

			// copy content
			if(!error) {
				try {
					for(x=0; x<nx; x++)
						for(y=0; y<ny; y++)
							for(z=0; z<nz; z++) data[x][y][z] = vector.data[x][y][z];
				} catch(...) {
					error = true;
				}
			}

			return error;
		};

		//=====================================================================
		/// Add parameter *value* to all vector elements.
		bool add(
		    const T &value) ///< [in] summand to be added to all vector elements
		{
			int x, y, z;		// local index variables
			bool error=false;	// error flag

			try {
				for(x=0; x<nx; x++) {
					for(y=0; y<ny; y++) {
						for(z=0; z<nz; z++) data[x][y][z] += value;
					}
				}
			} catch(...) {
				error = true;
			}

			return error;
		}

		//=====================================================================
		/// Multiply all elements of the vector by parameter *value*.
		bool multiply(
		    const T &value) ///< [in] factor to multiply all elments with
		{
			int x, y, z;		// local index variables
			bool error=false;	// error flag

			try {
				for(x=0; x<nx; x++) {
					for(y=0; y<ny; y++) {
						for(z=0; z<nz; z++) data[x][y][z] *= value;
					}
				}
			} catch(...) {
				error = true;
			}

			return error;
		}

		//=====================================================================
		/// Cut vector to the given limits. The new vector includes the lower and excludes the upper limit.
		bool cut(
		    int x1, ///< [in] lower limit for first dimension
		    int y1, ///< [in] lower limit for second dimension
		    int z1, ///< [in] lower limit for third dimension
		    int x2, ///< [in] upper limit for first dimension
		    int y2, ///< [in] upper limit for second dimension
		    int z2) ///< [in] upper limit for third dimension
		{
			bool error=false;	// error flag
			vector3d<T> cut;	// new image
			int x, y, z;		// local index variables

			// adjust limits
			if(x1<0) x1 = 0;
			if(y1<0) y1 = 0;
			if(z1<0) z1 = 0;
			if(x2>nx) x2 = nx;
			if(y2>ny) y2 = ny;
			if(z2>nz) z2 = nz;

			// swap limits if required
			if(x1>x2) {
				x = x1;
				x1 = x2;
				x2 = x;
			}
			if(y1>y2) {
				y = y1;
				y1 = y2;
				y2 = y;
			}
			if(z1>z2) {
				z = z1;
				z1 = z2;
				z2 = z;
			}

			// get memory for new image
			error = error || cut.setSize(x2-x1, y2-y1, z2-z1);

			// copy elements
			try {
				for(x=0; !error && x<cut.nx; x++) {
					for(y=0; y<cut.ny; y++) {
						for(z=0; z<cut.nz; z++) {
							cut.data[x][y][z] = data[x1+x][y1+y][z1+z];
						}
					}
				}
			} catch(...) {
				error = true;
			}

			// copy new image over old one
			error = error || set(cut);

			// return error flag
			return error;
		}

		//=====================================================================
		/// Shift all elements of the vector by *x0* and *y0*. The gaps are filled by parameter *fill*.
		bool shift(
		    int x0,         ///< [in] shift in positive *x*-direction
		    int y0,         ///< [in] shift in positive *y*-direction
		    const T &fill)  ///< [in] value to fill the gaps
		{
			bool error=false;	// error flag
			int x, y;			// local index variables

			// check shift range
			error = error || x0>nx || x0<-nx;
			error = error || y0>ny || y0<-ny;

			// if no shift, return
			if(x0==0 && y0==0) return false;

			// shift negative x and negative y
			if(!error && x0<0 && y0<0) {
				try {
					for(y=0; y<ny+y0; y++) {
						// shift line
						for(x=0; x<nx+x0; x++)
							data[x][y] = data[x-x0][y-y0];
						// fill rest of line
						for(x=nx+x0; x<nx; x++)
							data[x][y] = fill;
					}
					// fill all other lines
					for(y=ny+y0; y<ny; y++) {
						for(x=0; x<nx; x++)
							data[x][y] = fill;
					}
				} catch(...) {
					error = true;
				}
			}

			// shift negative x and positive y
			if(!error && x0<0 && y0>=0) {
				try {
					for(y=ny-1; y>=y0; y--) {
						// shift line
						for(x=0; x<nx+x0; x++)
							data[x][y] = data[x-x0][y-y0];
						// fill rest of line
						for(x=nx+x0; x<nx; x++)
							data[x][y] = fill;
					}
					// fill all other lines
					for(y=y0-1; y>=0; y--) {
						for(x=0; x<nx; x++)
							data[x][y] = fill;
					}
				} catch(...) {
					error = true;
				}
			}

			// shift positive x and negative y
			if(!error && x0>=0 && y0<0) {
				try {
					for(y=0; y<ny+y0; y++) {
						// shift line
						for(x=nx-1; x>=x0; x--)
							data[x][y] = data[x-x0][y-y0];
						// fill rest of line
						for(x=x0-1; x>=0; x--)
							data[x][y] = fill;
					}
					// fill all other lines
					for(y=ny+y0; y<ny; y++) {
						for(x=0; x<nx; x++)
							data[x][y] = fill;
					}
				} catch(...) {
					error = true;
				}
			}

			// shift positive x and positive y
			if(!error && x0>=0 && y0>=0) {
				try {
					for(y=ny-1; y>=y0; y--) {
						// shift line
						for(x=nx-1; x>=x0; x--)
							data[x][y] = data[x-x0][y-y0];
						// fill rest of line
						for(x=x0-1; x>=0; x--)
							data[x][y] = fill;
					}
					// fill all other lines
					for(y=y0-1; y>=0; y--) {
						for(x=0; x<nx; x++)
							data[x][y] = fill;
					}
				} catch(...) {
					error = true;
				}
			}

			return error;
		}

		//=====================================================================
		/// Copies the parameter *value* to all vector elements.
		vector3d &operator=(
		    const T &value) ///< [in] value to be copied to all vector elements
		{
			if(set(value)) throw;

			return *this;
		}

		//=====================================================================
		/// Assigns a vector to the current vector. The size and all vector elements are copied.
		vector3d &operator=(
		    const vector3d &vector) ///< [in] source vector to be copied
		{
			if(set(vector)) throw;

			return *this;
		}

		//=====================================================================
		/// Add parameter *value* to all vector elements.
		vector3d &operator+=(
		    const T &value) ///< [in] summand to be added to all vector elements
		{
			if(add(value)) throw;

			return *this;
		}

		//=====================================================================
		/// Multiply all vector elements by parameter *value*.
		vector3d &operator*=(
		    const T &value) ///< [in] factor to multiply all elments with
		{
			if(multiply(value)) throw;

			return *this;
		}

	};

} // namespace vec

#endif // #ifndef CLASS_VECTOR_INCLUDED
