//=============================================================================
// Filename: phantom.cpp
// Author: Robert Hess
// Version: 0.3
// Date: June 27th 2020
// Description: A set of classes to handle x-ray phantoms for Monte Carlo simulation.
//=============================================================================

#include "phantom.h"
#include <cfloat>
#include <cmath>
#include <fstream>
#include <iostream>
#include <iomanip>

using namespace std;

//=============================================================================
// Implementation of class cEffectiveDoseTissue
//=============================================================================

//=============================================================================
/// Default constructor. Resets name of tissue and its weighting factor.
cEffectiveDoseTissue::cEffectiveDoseTissue()
//=============================================================================
{
	reset();
}

//=============================================================================
/// Copy constructor. Copies all attributes of this class.
cEffectiveDoseTissue::cEffectiveDoseTissue(
    const cEffectiveDoseTissue &tissue)	///< [in] tissue to be copied
//=============================================================================
{
	copy(tissue);
}

//=============================================================================
/// Resets all attributes of this class.
void cEffectiveDoseTissue::reset()
//=============================================================================
{
	name = "";
	weightingFactor = 0;
}

//=============================================================================
/// Copies all attributes of this class.
void cEffectiveDoseTissue::copy(
    const cEffectiveDoseTissue &tissue)	///< [in] tissue to be copied
//=============================================================================
{
	name = tissue.name;
	weightingFactor = tissue.weightingFactor;
}

//=============================================================================
/// Assignemt operator. Copies all attributes of this class.
cEffectiveDoseTissue &cEffectiveDoseTissue::operator=(
    const cEffectiveDoseTissue &tissue)	///< [in] tissue to be copied
//=============================================================================
{
	copy(tissue);

	return *this;
}

//=============================================================================
// Implementation of class cOrganTissue
//=============================================================================

//=============================================================================
/// Default constructor. Resets all attributes of this class.
cOrganTissue::cOrganTissue()
//=============================================================================
{
	reset();
}

//=============================================================================
/// Copy constructor. Copies all attributes of this class.
cOrganTissue::cOrganTissue(
    const cOrganTissue &tissue)	///< [in] tissue to be copied
//=============================================================================
{
	copy(tissue);
}

//=============================================================================
/// Resets all attributes of this class.
void cOrganTissue::reset()
//=============================================================================
{
	name = "";
	Z.clear();
	massFraction.clear();
}

//=============================================================================
void cOrganTissue::copy(
    const cOrganTissue &tissue)	///< [in] tissue to be copied
//=============================================================================
{
	name = tissue.name;
	Z = tissue.Z;
	massFraction = tissue.massFraction;
}

//=============================================================================
/// Assignemt operator. Copies all attributes of this class.
cOrganTissue &cOrganTissue::operator=(
    const cOrganTissue &tissue)	///< [in] tissue to be copied
//=============================================================================
{
	copy(tissue);

	return *this;
}

//=============================================================================
// Implementation of class cOrgan
//=============================================================================

//=============================================================================
/// Default constructor. Resets all attributes of this class.
cOrgan::cOrgan()
//=============================================================================
{
	reset();
}

//=============================================================================
/// Copy constructor. Copies all attributes of this class.
cOrgan::cOrgan(
    const cOrgan &organ)	///< [in] organ to be copied
//=============================================================================
{
	copy(organ);
}

//=============================================================================
/// Resets all attributes of this class.
void cOrgan::reset()
//=============================================================================
{
	name = "";
	tissueIndex = 0;
	density = 0;
	effTissueID.resize(0);
	contrib.resize(0);
}

//=============================================================================
/// Copies all attributes of this class.
void cOrgan::copy(
    const cOrgan &organ)	///< [in] organ to be copied
//=============================================================================
{
	name = organ.name;
	tissueIndex = organ.tissueIndex;
	density = organ.density;
	effTissueID = organ.effTissueID;
	contrib = organ.contrib;
}

//=============================================================================
/// Assignemt operator. Copies all attributes of this class.
cOrgan &cOrgan::operator=(
    const cOrgan &organ)	///< [in] organ to be copied
//=============================================================================
{
	copy(organ);

	return *this;
}

//=============================================================================
// Implementation of class cPhantom
//=============================================================================

//=============================================================================
/// Default constructor. Resets all attributes of this class.
cPhantom::cPhantom()
//=============================================================================
{
	version = 0;
	subversion = 0;

	phantomSize[0] = 0;
	phantomSize[1] = 0;
	phantomSize[2] = 0;
}


//=============================================================================
/// Copy constructor. Copies all attributes of this class.
cPhantom::cPhantom(
    const cPhantom &phantom)    ///< [in] phantom to be copied
//=============================================================================
{
	copy(phantom);
}


//=============================================================================
/// Destructor, does nothing.
cPhantom::~cPhantom()
//=============================================================================
{
}


//=============================================================================
/// Assignemt operator. Copies all attributes of this class.
cPhantom &cPhantom::operator=(
    const cPhantom &phantom)    ///< [in] phantom to be copied
//=============================================================================
{
	copy(phantom);

	return *this;
}


//=============================================================================
/// Reads the first part of the binary phantom file and returns the phantom info only.
void cPhantom::getPhantomInfo(
    const std::string &filename,    ///< [in] filename of phantom file
    std::string &phantomInfo)       ///< [out] phantom info read from file
//=============================================================================
{
	ifstream inp;	// input file stream
	string text;
	char ch;
	uint32_t filetype;

	// open file
	inp.open(filename, ios_base::binary);
	if(!inp.is_open()) throw runtime_error(string("Could not open input file >")+filename+string("<."));

	// check first string "phantom data"
	text = "";
	for(unsigned i=0; i<12; i++) {
		inp.read(&ch, 1);
		text.push_back(ch);
	}
	if(text!="phantom data")
		throw runtime_error("Unexpected file type. Phantom data expected.");

    // skip version and filetype
    inp.seekg(sizeof(version)+sizeof(subversion), ios_base::cur);

    // check filetype
	inp.read((char*)&filetype, sizeof(filetype));
	if(filetype!=0)
		throw runtime_error("Unexpected phantom type (yet only type 0 supported).");

    // read phantom info
	readStringBin(inp, phantomInfo);
}


//=============================================================================
/// Reads the binary phantom file.
void cPhantom::readPhantomBin(
    const std::string &filename)	///< [in] name of input file
//=============================================================================
{
	ifstream inp;	// input file stream
	string text;
	char ch;
	uint32_t filetype;
	uint16_t nt;			// number of table entries
	uint16_t ne;			// number of elements in tissue
	float f4;				// 4 byte floating point to read file
	uint8_t u1;				// 1 byte unsigned integer to write in file
	uint16_t u2;			// 2 byte unsigned integer to write in file
	uint32_t nx, ny, nz;	// size of phantom
	uint32_t ix, iy, iz;	// index in x-, y- and z-direction in phantom
	uint64_t totalCount;	// total number of voxels
	uint8_t count, value;	// number and value of same voxels

	// open file
	inp.open(filename, ios_base::binary);
	if(!inp.is_open()) throw runtime_error(string("Could not open input file >")+filename+string("<."));

	// preliminary stuff
	text = "";
	for(unsigned i=0; i<12; i++) {
		inp.read(&ch, 1);
		text.push_back(ch);
	}
	if(text!="phantom data")
		throw runtime_error("Unexpected file type. Phantom data expected.");
	inp.read((char*)&version, sizeof(version));
	inp.read((char*)&subversion, sizeof(subversion));
	inp.read((char*)&filetype, sizeof(filetype));
	if(filetype!=0)
		throw runtime_error("Unexpected phantom type (yet only type 0 supported).");
	readStringBin(inp, phantomInfo);

	// effective dose data
	text = "";
	for(unsigned i=0; i<19; i++) {
		inp.read(&ch, 1);
		text.push_back(ch);
	}
	if(text!="effective dose data")
		throw runtime_error("Error in phantom data file.");
	readStringBin(inp, effDoseInfo);
	inp.read((char*)&nt, sizeof(nt));
	effDoseTissue.resize(nt);
	for(unsigned i=0; i<nt; i++) {
		readStringBin(inp, effDoseTissue[i].name);
		inp.read((char*)&f4, sizeof(f4));
		effDoseTissue[i].weightingFactor = f4;
	}

	// tissue list
	text = "";
	for(unsigned i=0; i<11; i++) {
		inp.read(&ch, 1);
		text.push_back(ch);
	}
	if(text!="tissue data")
		throw runtime_error("Error in phantom data file.");
	inp.read((char*)&nt, sizeof(nt));
	organTissue.resize(nt);
	for(unsigned i=0; i<nt; i++) {
		readStringBin(inp, organTissue[i].name);
		inp.read((char*)&ne, sizeof(ne));
		organTissue[i].Z.resize(ne);
		organTissue[i].massFraction.resize(ne);
		for(unsigned j=0; j<ne; j++) {
			inp.read((char*)&u1, sizeof(u1));
			organTissue[i].Z[j] = u1;
			inp.read((char*)&f4, sizeof(f4));
			organTissue[i].massFraction[j] = f4;
		}
	}

	// organ list
	text = "";
	for(unsigned i=0; i<10; i++) {
		inp.read(&ch, 1);
		text.push_back(ch);
	}
	if(text!="organ data")
		throw runtime_error("Error in phantom data file.");
	inp.read((char*)&nt, sizeof(nt));
	organ.resize(nt);
	for(unsigned i=0; i<nt; i++) {
		readStringBin(inp, organ[i].name);
		inp.read((char*)&u2, sizeof(u2));
		organ[i].tissueIndex = u2;
		inp.read((char*)&f4, sizeof(f4));
		organ[i].density = f4;
		inp.read((char*)&ne, sizeof(ne));
		organ[i].effTissueID.resize(ne);
		organ[i].contrib.resize(ne);
		for(unsigned j=0; j<ne; j++) {
			inp.read((char*)&u2, sizeof(u2));
			organ[i].effTissueID[j] = u2;
			inp.read((char*)&f4, sizeof(f4));
			organ[i].contrib[j] = f4;
		}
	}

	// voxel data
	text = "";
	for(unsigned i=0; i<10; i++) {
		inp.read(&ch, 1);
		text.push_back(ch);
	}
	if(text!="voxel data")
		throw runtime_error("Error in phantom data file.");
	for(unsigned i=0; i<3; i++) {
		inp.read((char*)&f4, sizeof(f4));
		phantomSize[i] = f4;
	}
	inp.read((char*)&nx, sizeof(nx));
	totalCount = nx;
	inp.read((char*)&ny, sizeof(ny));
	totalCount *= ny;
	inp.read((char*)&nz, sizeof(nz));
	totalCount *= nz;
	voxel.setSize(nx, ny, nz);
	ix = iy = iz = 0;
	while(!inp.eof() && totalCount>0 && ix<nx) {
		inp.read((char*)&count, sizeof(count));
		inp.read((char*)&value, sizeof(value));
		totalCount -= count;
		while(count-- && ix<nx) {
			voxel[ix][iy][iz] = value;
			iz++;
			if(iz>=nz) {
				iz = 0;
				iy++;
			}
			if(iy>=ny) {
				iy = 0;
				ix++;
			}
		}
	}
	if(ix!=nx || iy!=0 || iz!=0 || totalCount!=0)
		throw runtime_error("Bad voxel data in phantom file.");

	// closure
	text = "";
	for(unsigned i=0; i<19; i++) {
		inp.read(&ch, 1);
		text.push_back(ch);
	}
	if(text!="end of phantom data")
		throw runtime_error("Error in phantom data file.");
}


//=============================================================================
/// \brief Checks the phantom for consistency
///
/// Throws an exception for possible errors found.
void cPhantom::checkPhantom()
{
    int maxIndex; // largest index found

    // find and check maximum organ index
    maxIndex = 0;
    for(int x=0; x<voxel.nx; x++)
        for(int y=0; y<voxel.ny; y++)
            for(int z=0; z<voxel.nz; z++)
                if(maxIndex<voxel[x][y][z] && voxel[x][y][z]!=255)
                    maxIndex = voxel[x][y][z];
    if(maxIndex>=(int)organ.size())
        throw runtime_error("Found organ index which is too large.");

    // check maximum organ tissue index
    maxIndex = 0;
    for(int i=0; i<(int)organ.size(); i++)
        if(maxIndex<organ[i].tissueIndex)
            maxIndex = organ[i].tissueIndex;
    if(maxIndex>=(int)organTissue.size())
        throw runtime_error("Found an organ tissue index which is too large.");

    // check maximum effective dose tissue index
    maxIndex = 0;
    for(int i=0; i<(int)organ.size(); i++)
        for(int j=0; j<(int)organ[i].effTissueID.size(); j++)
            if(maxIndex<organ[i].effTissueID[j])
                maxIndex = organ[i].effTissueID[j];
    if(maxIndex>=(int)effDoseTissue.size())
        throw runtime_error("Found an effective dose tissue index which is too large.");
}


//=============================================================================
/// \brief Evaluates for a given position the corresponding indices in x-, y- and z-direction.
///
/// \return Returns true if outside phantom cube.
bool cPhantom::getVoxelIndex(
		double position[3],	    ///< [in] position of particle in m
		int index[3])		    ///< [out] index of voxel in the order slice/row/column
//=============================================================================
{
    // cehck if outside phantom
    if(position[0]<0 || position[1]<0 || position[2]<0) return true;
    if(position[0]>=phantomSize[0] || position[1]>=phantomSize[1] || position[2]>=phantomSize[2]) return true;

    // evaluate index
	index[0] = (int)floor(voxel.nx*position[0]/phantomSize[0]);
	index[1] = (int)floor(voxel.ny*position[1]/phantomSize[1]);
	index[2] = (int)floor(voxel.nz*position[2]/phantomSize[2]);

	return false;
}


//=============================================================================
/// \brief Creates a image to get an impression of the phantom.
void cPhantom::getImage(
		cMedImage<double> &image,	///< [out] image for given projection
		double pixelSize,			///< [in] pixel size in m
		int direction)				///< [in] direction of projection 0 - 2
//=============================================================================
{
	uint8_t value;

	switch(direction) {
	case 0:	// x-y plane
		image.create(voxel.ny, voxel.nx, 0.0);
		for(unsigned r=0; r<image.nRow; r++) {
			for(unsigned c=0; c<image.nCol; c++) {
				for(unsigned i=0; i<(unsigned)voxel.nz; i++) {
					value = voxel[c][r][i];
					value++;
					image[image.nRow-r-1][c] += value;
				}
			}
		}
		break;
	case 1:	// y-z plane
		image.create(voxel.nz, voxel.ny, 0.0);
		for(unsigned r=0; r<image.nRow; r++) {
			for(unsigned c=0; c<image.nCol; c++) {
				for(unsigned i=0; i<(unsigned)voxel.nx; i++) {
					value = voxel[i][c][r];
					value++;
					image[image.nRow-r-1][c] += value;
				}
			}
		}
		break;
	case 2:	// z-x plane
		image.create(voxel.nx, voxel.nz, 0.0);
		for(unsigned r=0; r<image.nRow; r++) {
			for(unsigned c=0; c<image.nCol; c++) {
				for(unsigned i=0; i<(unsigned)voxel.ny; i++) {
					value = voxel[r][i][c];
					value++;
					image[image.nRow-r-1][c] += value;
				}
			}
		}
		break;
	}

	image.findMinMax(image.pixelValueForWhite, image.pixelValueForBlack);
}


//=============================================================================
/// Copies all attributes of this class.
void cPhantom::copy(
    const cPhantom &phantom)    ///< [in] phantom to be copied
//=============================================================================
{
	version       = phantom.version;
	subversion    = phantom.subversion;
	phantomInfo   = phantom.phantomInfo;
	effDoseInfo   = phantom.effDoseInfo;
	effDoseTissue = phantom.effDoseTissue;
	organTissue   = phantom.organTissue;
	organ         = phantom.organ;
	voxel         = phantom.voxel;
	memcpy(phantomSize, phantom.phantomSize, 3*sizeof(*phantomSize));
}


//=============================================================================
/// Reads a string from a binary file: A 2 byte unsigned n followed by n characters.
void cPhantom::readStringBin(
    std::ifstream &inp,	///< [in,out] input stream
    std::string &text)	///< [out] string to be read
//=============================================================================
{
	uint16_t n;	// number of characters in string
	char ch;	// actual character

	// delete previous string
	text = "";

	// read number of characters
	inp.read((char*)&n, sizeof(n));

	// read characters
	for(unsigned i=0; i<n; i++) {
		inp.read(&ch, 1);
		text.push_back(ch);
	}
}

