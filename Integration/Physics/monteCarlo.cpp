//=============================================================================
// Filename: monteCarlo.h
// Author: Robert Hess
// Version: 0.3
// Date: June 27th 2020
// Description: Core class for the Monte Carlo simulation of effective dose
//              in human bodies.
//=============================================================================

#define _USE_MATH_DEFINES
#include <cmath>
#include "monteCarlo.h"
#include <cfloat>
#include <vector>

//#define _DEBUG

#ifdef _DEBUG
#include <iostream>
#endif

using namespace std;

//=============================================================================
/// Initialises a few attributes.
cMonteCarlo::cMonteCarlo()
//=============================================================================
{
    progress = 0;
    threshold = 1;
	remaining = 0;
}


//=============================================================================
/// Does nothing.
cMonteCarlo::~cMonteCarlo()
//=============================================================================
{
}


//=============================================================================
void cMonteCarlo::setInputData(
    const cInputData &newInput) ///< [in] new input data
//=============================================================================
{
    // copy new input data
    input = newInput;

    // set progress to: input data set
    progress = 1;
}


//=============================================================================
/// Loads the phantom and atomic data. Prepares all materials, random generators
/// and transformation matrices. Gets memory for the results and initializes
/// other attributes. Evaluates the mass of all organs and effective dose tissues.
void cMonteCarlo::prepare()
//=============================================================================
{
	vector<cMaterial> mat;      // list of materials

	// check if input data present
    if(progress<1) throw runtime_error("No input data present for preparation.");

	// read and check phantom
	phantom.readPhantomBin(input.phantomFilename);
	phantom.checkPhantom();
//	if(input.female) phantom.readPhantomBin("Icrp110AdultFemale.ptm");
//	else phantom.readPhantomBin("Icrp110AdultMale.ptm");

	// prepare atomic data
	ele.prepare();

	// prepare materials, i.e. organ tissues
	mat.resize(phantom.organ.size());
	for(unsigned i=0; i<mat.size(); i++) {
		mat[i].setName(phantom.organ[i].name);
		unsigned tissueID = phantom.organ[i].tissueIndex;
		for(unsigned j=0; j<phantom.organTissue[tissueID].Z.size(); j++) {
			unsigned Z = phantom.organTissue[tissueID].Z[j];
			double fraction = phantom.organTissue[tissueID].massFraction[j];
			fraction /= ele.getRelAtomicWeight(Z);
			mat[i].addElement(Z, fraction);
		}
		mat[i].setDensity(phantom.organ[i].density);
	}

	// prepare random generators
	randMC.setRandBase(kiss);
	double photonsPerCharge = randMC.prepare(mat, input.spectrumFilename, input.tubeVoltage);
	kiss.srand(input.randomSeed);

	// prepare transformation matrix for focal spot
	moveSpot.reset();
	if(input.systemType==input.rad) {
	    moveSpot.rotateAroundX(input.tubeRot[0]);
	    moveSpot.rotateAroundY(input.tubeRot[1]);
	    moveSpot.rotateAroundZ(input.tubeRot[2]);
	    moveSpot.shift(input.tubePos[0], input.tubePos[1], input.tubePos[2]);
    }
	if(input.systemType==input.ct) {
	    moveSpot.rotateAroundX(input.gantryRot[0]);
	    moveSpot.rotateAroundY(input.gantryRot[1]);
	    moveSpot.rotateAroundZ(input.gantryRot[2]);
	    moveSpot.shift(input.gantryPos[0], input.gantryPos[1], input.gantryPos[2]);
    }

	// prepare transformation of direction
	changeDirection.reset();
	if(input.systemType==input.rad) {
    	changeDirection.rotateAroundX(input.tubeRot[0]);
	    changeDirection.rotateAroundY(input.tubeRot[1]);
	    changeDirection.rotateAroundZ(input.tubeRot[2]);
    }
	if(input.systemType==input.ct) {
    	changeDirection.rotateAroundX(input.gantryRot[0]);
	    changeDirection.rotateAroundY(input.gantryRot[1]);
	    changeDirection.rotateAroundZ(input.gantryRot[2]);
    }

	// misc
	remaining = input.noOfPhotons;

	// prepare results
	results.prepare(phantom);
	results.photonsPerCharge = photonsPerCharge;

	// prepare mass of argans and effective dose tissues
	prepareMass();

	// set progress to: simulation prepared
	progress = 2;
}


//=============================================================================
/// Does the same as setInputData() and prepare().
void cMonteCarlo::prepare(
    const cInputData &newInput) ///< [in] new input data
//=============================================================================
{
    // set new input data
    setInputData(newInput);

    // prepare simulation
    prepare();
}


//=============================================================================
/// \return true if remaining photons left
bool cMonteCarlo::simulate(
    uint64_t n   ///< next number of incident photons to be simulated
)
//=============================================================================
{
	cParticle incidentParticle;	// incident particle

	// check if input data set
    if(progress<2) throw runtime_error("Simulation not prepared.");

	// set progress to: simulation running
	progress = 3;

	// set particle type
	incidentParticle.setParticleType(cParticle::photon);

	// loop over all remaining photons
	if(n>remaining) n = remaining;
	remaining -= n;
	while (n--) {

		// set incident particle energy
		incidentParticle.setEnergy(randMC.getPhotonEnergy());
		results.totalEnergy += incidentParticle.getEnergy();

		// position and direction for CT-system
		if(input.systemType==input.ct) {
		    // angle for fan thickness
		    double u[3];    // direction of particle
		    double angle = input.fanAngleThickness*(kiss.randU()-0.5);
		    u[0] = -cos(angle);
		    u[2] = -sin(angle);
		    // angle for fan width
		    angle = input.fanAngleWidth*(kiss.randU()-0.5);
		    u[1] = u[0]*sin(angle);
		    u[0] *= cos(angle);
		    // angle for gantry rotation
		    angle = 2*M_PI*(kiss.randU()-0.5);
		    double cosA = cos(angle);
		    double sinA = sin(angle);
		    double tmp = u[0];
		    u[0] = tmp*cosA - u[1]*sinA;
		    u[1] = tmp*sinA + u[1]*cosA;
		    changeDirection.apply(u);
		    incidentParticle.setDirection(u);
		    // position for gantry rotation
		    double p[3];    // position of particle
		    p[0] = input.radiusCt*cosA;
		    p[1] = input.radiusCt*sinA;
		    p[2] = 0;
		    moveSpot.apply(p);
		    incidentParticle.setPosition(p);
		}

		// position and direction for RAD-system
		if(input.systemType==input.rad) {
			// set direction
			double u[3];    // direction of particle
			u[0] = input.collOpening[0]*(kiss.randU()-0.5);
			u[1] = input.collOpening[1]*(kiss.randU()-0.5);
			u[2] = input.collDistance;
			double d = sqrt(u[0]*u[0] + u[1]*u[1] + u[2]*u[2]);
			u[0] /= d;
			u[1] /= d;
			u[2] /= d;
			changeDirection.apply(u);
			incidentParticle.setDirection(u);
			// set position
    		incidentParticle.setPosition(input.tubePos);
		}

		// evaluate the intersection point with phantom
		if(moveToPhantom(incidentParticle)) {
		    // if it intersects the phantom add to stack
			stack.push(incidentParticle);
		} else {
			// if it does not intersect phantom
			results.neverInEnergy += incidentParticle.getEnergy();
		}

		// handle particle
		while (!stack.empty())
			handleParticle();
	}

	// if no photons left set progress to: simulation done
	if(remaining==0) progress = 4;

	return remaining!=0;
}


//=============================================================================
/// Evaluates the dose for all organs and effective dose tissues and the
/// effective dose *E*. Analyses the statistical uncertaincy for all dose
/// values in terms of variance (organs and effective dose tissues) and
/// standard deviation (effective dose). The values for organs and effective
/// dose tissues are scaled w.r.t. the desired current-time-product.
void cMonteCarlo::postProcessing()
//=============================================================================
{
	// check if input data set
    if(progress<4) throw runtime_error("Simulation not finished.");

    // reset old data
    for(int i=0; i<(int)results.energyO.size(); i++) results.energyO[i] = 0;
    for(int i=0; i<(int)results.energyOVar.size(); i++) results.energyOVar[i] = 0;
	for(int i=0; i<(int)results.energyT.size(); i++) results.energyT[i] = 0;
	for(int i=0; i<(int)results.energyTVar.size(); i++) results.energyTVar[i] = 0;
	results.E = 0;
	results.EStdev = 0;
	results.totalAbsorption = 0;

    // get number of voxels and energy in keV per organ
    for(int ix=0; ix<phantom.voxel.nx; ix++) {
        for(int iy=0; iy<phantom.voxel.ny; iy++) {
            for(int iz=0; iz<phantom.voxel.nz; iz++) {
                results.totalAbsorption += results.voxel[ix][iy][iz];
                uint8_t io = phantom.voxel[ix][iy][iz];
                if(io<phantom.organ.size()) {
                    results.energyO[io] += results.voxel[ix][iy][iz];
                    results.energyOVar[io] += results.voxelVar[ix][iy][iz];
                }
            }
        }
    }

    // convert energy from keV to J and adjust w.r.t. current-time-product and solid angle
    double Omega;   // solid angle of x-ray beam
    if(input.systemType==input.ct) {
        Omega = 4*input.fanAngleWidth*sin(input.fanAngleThickness/2);
    } else {
        // solid angle in sr by Oosterom-Strackee equation
        Omega = 4*input.collDistance*input.collDistance;
        Omega += input.collOpening[0]*input.collOpening[0];
        Omega += input.collOpening[1]*input.collOpening[1];
        Omega = input.collOpening[0]*input.collOpening[1]/(2*input.collDistance*sqrt(Omega));
        Omega = 4*atan(Omega);
    }
    double adjust;  // adjustment w.r.t. current-time-product, solid angle and unit
    adjust = results.photonsPerCharge;  // in number of photons per mAs µsr
    adjust *= 1e6;                      // in number of photons per mAs sr
    adjust *= Omega;                    // in number of photons per mAs
    adjust *= input.currentTimeProduct; // in number of photons
    adjust /= input.noOfPhotons;        // adjustment factor for energy
    adjust *= 1000;                     // keV -> eV;
    adjust *= 1.602176634e-19;          // eV -> J;
    for(int i=0; i<(int)results.energyO.size(); i++) {
        results.energyO[i] *= adjust;           // adjust w.r.t. current-time-product and solid angle
        results.energyOVar[i] *= adjust*adjust; // adjust w.r.t. current-time-product and solid angle
    }

    // evaluate energy for effective dose tissues
    for(int i=0; i<(int)phantom.organ.size(); i++) {
        for(int j=0; j<(int)phantom.organ[i].effTissueID.size(); j++) {
            results.energyT[phantom.organ[i].effTissueID[j]] +=
                results.energyO[i]*phantom.organ[i].contrib[j];
            results.energyTVar[phantom.organ[i].effTissueID[j]] +=
                results.energyOVar[i]*phantom.organ[i].contrib[j]*phantom.organ[i].contrib[j];
        }
    }

    // evaluate effective dose
    for(int i=0; i<(int)phantom.effDoseTissue.size(); i++) {
        double tmp = phantom.effDoseTissue[i].weightingFactor / results.massT[i];
        results.E += tmp*results.energyT[i];
        results.EStdev += tmp*tmp*results.energyTVar[i];
    }
    results.EStdev = sqrt(results.EStdev);

	// set progress to: prost-processing done
	progress = 5;
}


//=============================================================================
/// \return number of incident photons that still needs to be simulated
uint64_t cMonteCarlo::getRemainingPhotons()
//=============================================================================
{
    return remaining;
}


//=============================================================================
/// \return simulation results as one object of class cResults
const cResults &cMonteCarlo::getResults()
//=============================================================================
{
    return results;
}


//=============================================================================
/// \return loaded phantom as object of class cPhantom
const cPhantom &cMonteCarlo::getPhantom()
//=============================================================================
{
    return phantom;
}


//=============================================================================
/// Adds all voxels for each organ and scales the sum to evaluate their mass.
/// Then distributes the masses to the effective dose tissues.
void cMonteCarlo::prepareMass()
//=============================================================================
{
    // reset old mass data
    results.massO.resize(phantom.organ.size(), 0.0);
    results.massT.resize(phantom.effDoseTissue.size(), 0.0);

    // get number of voxels per organ
    for(int ix=0; ix<phantom.voxel.nx; ix++) {
        for(int iy=0; iy<phantom.voxel.ny; iy++) {
            for(int iz=0; iz<phantom.voxel.nz; iz++) {
                uint8_t io = phantom.voxel[ix][iy][iz];
                if(io<phantom.organ.size()) {
                    results.massO[io] += 1.0;
                }
            }
        }
    }

    // convert number of voxels to mass
    double voxelVolume; // volume of one voxel in cm^3;
    voxelVolume = phantom.phantomSize[0]/phantom.voxel.nx * 100;
    voxelVolume *= phantom.phantomSize[1]/phantom.voxel.ny * 100;
    voxelVolume *= phantom.phantomSize[2]/phantom.voxel.nz * 100;
    for(int i=0; i<(int)phantom.organ.size(); i++) {
        results.massO[i] *= voxelVolume;                // number of voxels -> cm^3
        results.massO[i] *= phantom.organ[i].density;   // cm^3 -> g
        results.massO[i] *= 1e-3;                       // g -> kg
    }

    // evaluate mass for effective dose tissues
    for(int i=0; i<(int)phantom.organ.size(); i++) {
        for(int j=0; j<(int)phantom.organ[i].effTissueID.size(); j++) {
            results.massT[phantom.organ[i].effTissueID[j]] +=
                results.massO[i]*phantom.organ[i].contrib[j];
        }
    }
}


//=============================================================================
/// Switches by the particle type between the associated methods.
void cMonteCarlo::handleParticle()
//=============================================================================
{
    // handle different particle types
	switch (stack.top().getParticleType()) {
	case cParticle::photon:
		handlePhoton();
		break;
	case cParticle::interaction:
		handleInteraction();
		break;
	case cParticle::photo:
		handlePhotoEffect();
		break;
	case cParticle::ion:
		handleIon();
		break;
	case cParticle::Compton:
		handleCompton();
		break;
	case cParticle::Rayleigh:
		handleRayleigh();
		break;
	case cParticle::absorption:
		handleAbsorption();
		break;
	}
}


//=============================================================================
/// Moves a photon by a random free path depending on the material it is in.
void cMonteCarlo::handlePhoton()
//=============================================================================
{
	double d;           // free path
	double dVoxel;      // distance to next voxel
	int voxelIndex[3];  // index of next voxel
	bool inside;        // true if next voxel is in phantom

	// get distance to next voxel
	inside = distanceToNextVoxel(stack.top(), dVoxel, voxelIndex);

	// if current voxel is empty move photon to next voxel
	if(stack.top().getOrganIndex()==255) {
		stack.top().move(dVoxel);
		if(inside) {
			stack.top().setVoxelIndex(voxelIndex);
			stack.top().setOrganIndex(phantom.voxel[voxelIndex[0]][voxelIndex[1]][voxelIndex[2]]);
        } else {
            results.escapedEnergy += stack.top().getEnergy();
            stack.pop();
        }
	}

	// handle photon in matter
	else {

		// get free path
		d = randMC.getFreePath(stack.top().getOrganIndex(), stack.top().getEnergy());

		// if next interaction within current voxel
		if(d<dVoxel) {
			stack.top().move(d);
			stack.top().setParticleType(cParticle::interaction);

        // if photon moved to next voxel
		} else {
            stack.top().move(dVoxel);
            if(inside) {
                stack.top().setVoxelIndex(voxelIndex);
                stack.top().setOrganIndex(phantom.voxel[voxelIndex[0]][voxelIndex[1]][voxelIndex[2]]);
            } else {
                results.escapedEnergy += stack.top().getEnergy();
                stack.pop();
            }
		}
	}
}


//=============================================================================
/// Choses the atomic number of the interacting element and the interaction
/// effect, i.e. photo electric effect, Compton effect or Rayleigh effect.
void cMonteCarlo::handleInteraction()
//=============================================================================
{
	unsigned Z;				// atomic number
	unsigned interaction;	// interaction type

	// choose interacting element
	Z = randMC.getInteractingElement(stack.top().getOrganIndex(), stack.top().getEnergy());
	stack.top().setAtomicNumber(Z);

	// choose interaction
	interaction = randMC.getInteraction(Z, stack.top().getEnergy());
	switch (interaction) {
	case 1:
		stack.top().setParticleType(cParticle::photo);
		break;
	case 2:
		stack.top().setParticleType(cParticle::Compton);
		break;
	case 3:
		stack.top().setParticleType(cParticle::Rayleigh);
		break;
	}
}


//=============================================================================
/// Chooses the ionized sub-shell and treats the recoil electron as absorbed.
void cMonteCarlo::handlePhotoEffect()
//=============================================================================
{
	cParticle part;		// actual particle
	unsigned subshell;	// new ionized subshell

	// copy particle from stack
	part = stack.top();

	// choose subshell to be ionized
	subshell = randMC.getPhotoSubshell(part.getAtomicNumber(), part.getEnergy());

	// store new ion
	stack.top().setSubshell(subshell);
	stack.top().setParticleType(cParticle::ion);

	// store recoil electron as absorbed
	part.setEnergy(part.getEnergy() - ele.getBindingEnegry(part.getAtomicNumber(), subshell));
	part.setParticleType(cParticle::absorption);
	stack.push(part);
}


//=============================================================================
/// Chooses between characteristic and Auger effect, stores the new vacancies
/// and treats Auger electrons as aborbed.
void cMonteCarlo::handleIon()
//=============================================================================
{
	cParticle part;	// actual particle
	double energy;	// energy of secondary particle
	unsigned vac1;	// subshell designator of first vacancy
	unsigned vac2;	// subshell designator of second vacancy
	double u[3];	// new direction of secondary photon

	// copy particle from stack
	part = stack.top();

	// choose de-excitation
	energy = randMC.getAugerChar(part.getAtomicNumber(), part.getSubshell(), vac1, vac2);

	// store secondary particle
	stack.top().setEnergy(energy);
	stack.top().setParticleType(vac1 && !vac2 ? cParticle::photon : cParticle::absorption);
	if (energy < threshold) stack.top().setParticleType(cParticle::absorption);
	if (stack.top().getParticleType() == cParticle::photon) {
		u[2] = cos(randMC.getIsotropicPolar());
		randMC.getIsotropicAzimuth(u);
		stack.top().setDirection(u);
	}

	// push vacancies
	//part.setParticleType(cParticle::ion);
	if (vac1) {
		part.setSubshell(vac1);
		stack.push(part);
	}
	if (vac2) {
		part.setSubshell(vac2);
		stack.push(part);
	}
}


//=============================================================================
/// Evaluates the scattering angle of the secondary photon and treats the
/// electron as absorbed.
void cMonteCarlo::handleCompton()
//=============================================================================
{
	double polar;		// polar scatter angel of secondary photon
	cParticle part;		// actual particle
	double energy;		// energy of secondary photon
	double u[3];		// direction of secondary photon

	// copy top particle on stack
	part = stack.top();

	// get direction of secondary photon
	polar = randMC.getComptonPolar(part.getAtomicNumber(), part.getEnergy());
	u[2] = cos(polar);
	randMC.getIsotropicAzimuth(u);

	// evalaute energy of secondary photon
	energy = part.getEnergy() / (1 + part.getEnergy() / 510.9989461 * (1 - u[2]));

	// energy of secondary photon below threshold treat as absorbed
	if (energy < threshold) {
		stack.top().setParticleType(cParticle::absorption);
	}

	// store secondary photon and electron separately
	else {

		// store secondary photon
		stack.top().changeDirection(u);
		stack.top().setParticleType(cParticle::photon);
		stack.top().setEnergy(energy);

		// store secondary electron
		part.setParticleType(cParticle::absorption);
		part.setEnergy(part.getEnergy() - energy);
		stack.push(part);
	}
}


//=============================================================================
/// Evaluates the scattering angle without changing the energy of the particle.
void cMonteCarlo::handleRayleigh()
//=============================================================================
{
	double polar;	// polar scatter angel of secondary photon
	double u[3];	// direction of secondary photon

#ifdef _DEBUG
	cout << "    handle Rayleigh" << endl;
#endif

	// get new direction
	polar = randMC.getRayleighPolar(stack.top().getAtomicNumber(), stack.top().getEnergy());
	u[2] = cos(polar);
	randMC.getIsotropicAzimuth(u);

	// store in new particle
	stack.top().changeDirection(u);
	stack.top().setParticleType(cParticle::photon);
}


//=============================================================================
/// Stores the absorbed energy into the corresponding voxel.
void cMonteCarlo::handleAbsorption()
//=============================================================================
{
	int voxelIndex[3];
#ifdef _DEBUG
	cout << "    handle absorption: " << stack.top().getEnergy() << " keV" << endl;
#endif

	// store energy in voxel
	stack.top().getVoxelIndex(voxelIndex);
	double energy = stack.top().getEnergy();
	results.voxel[voxelIndex[0]][voxelIndex[1]][voxelIndex[2]] += energy;
	results.voxelVar[voxelIndex[0]][voxelIndex[1]][voxelIndex[2]] += energy*energy;

	// take particle from stack
	stack.pop();
}


//=============================================================================
/// If outside the phantom this method moves the particle along the particle
/// beam towards the phantom cube and within the phantom to the first non-empty
/// voxel.
/// \return true if moved to surface or inside, false if no intersection with phantom
bool cMonteCarlo::moveToPhantom(
		cParticle &part)        /// [in,out] particle to be moved
//=============================================================================
{
	double p[3];		// position of particle
	double u[3];		// direction of particle
	int voxelIndex[3];	// index of current voxel
	double d;			// distance to all six bounding planes of patient cube
	double x, y, z;     // new position
	bool inside=false;  // true if particle within phantom

	// get particle position and direction
	part.getPosition(p);
	part.getDirection(u);

	// if already in phantom
	if(!phantom.getVoxelIndex(p, voxelIndex)) {
		inside = true;
	}

	// from left side
	if(!inside && p[0]<=0 && u[0]>0) {
		d = (0-p[0])/u[0];
		y = p[1]+d*u[1];
		z = p[2]+d*u[2];
		if(y>=0 && y<phantom.phantomSize[1] && z>=0 && z<phantom.phantomSize[2]) {
			voxelIndex[0] = 0;
			voxelIndex[1] = (int)(phantom.voxel.ny*y/phantom.phantomSize[1]);
			if(voxelIndex[1]<0) voxelIndex[1] = 0;
			if(voxelIndex[1]>=phantom.voxel.ny) voxelIndex[1] = phantom.voxel.ny-1;
			voxelIndex[2] = (int)(phantom.voxel.nz*z/phantom.phantomSize[2]);
			if(voxelIndex[2]<0) voxelIndex[2] = 0;
			if(voxelIndex[2]>=phantom.voxel.nz) voxelIndex[2] = phantom.voxel.nz-1;
			part.setPosition(0, y, z);
			inside = true;
		}
	}

	// from right side
	if(!inside && p[0]>=phantom.phantomSize[0] && u[0]<0) {
		d = (phantom.phantomSize[0]-p[0])/u[0];
		y = p[1]+d*u[1];
		z = p[2]+d*u[2];
		if(y>=0 && y<phantom.phantomSize[1] && z>=0 && z<phantom.phantomSize[2]) {
			voxelIndex[0] = phantom.voxel.nx-1;
			voxelIndex[1] = (int)(phantom.voxel.ny*y/phantom.phantomSize[1]);
			if(voxelIndex[1]<0) voxelIndex[1] = 0;
			if(voxelIndex[1]>=phantom.voxel.ny) voxelIndex[1] = phantom.voxel.ny-1;
			voxelIndex[2] = (int)(phantom.voxel.nz*z/phantom.phantomSize[2]);
			if(voxelIndex[2]<0) voxelIndex[2] = 0;
			if(voxelIndex[2]>=phantom.voxel.nz) voxelIndex[2] = phantom.voxel.nz-1;
			part.setPosition(phantom.phantomSize[0], y, z);
			inside = true;
		}
	}

	// from back side
	if(!inside && p[1]<=0 && u[1]>0) {
		d = (0-p[1])/u[1];
		x = p[0]+d*u[0];
		z = p[2]+d*u[2];
		if(x>=0 && x<phantom.phantomSize[0] && z>=0 && z<phantom.phantomSize[2]) {
			voxelIndex[0] = (int)(phantom.voxel.nx*x/phantom.phantomSize[0]);
			if(voxelIndex[0]<0) voxelIndex[0] = 0;
			if(voxelIndex[0]>=phantom.voxel.nx) voxelIndex[0] = phantom.voxel.nx-1;
			voxelIndex[1] = 0;
			voxelIndex[2] = (int)(phantom.voxel.nz*z/phantom.phantomSize[2]);
			if(voxelIndex[2]<0) voxelIndex[2] = 0;
			if(voxelIndex[2]>=phantom.voxel.nz) voxelIndex[2] = phantom.voxel.nz-1;
			part.setPosition(x, 0, z);
			inside = true;
		}
	}

	// from front side
	if(!inside && p[1]>=phantom.phantomSize[1] && u[1]<0) {
		d = (phantom.phantomSize[1]-p[1])/u[1];
		x = p[0]+d*u[0];
		z = p[2]+d*u[2];
		if(x>=0 && x<phantom.phantomSize[0] && z>=0 && z<phantom.phantomSize[2]) {
			voxelIndex[0] = (int)(phantom.voxel.nx*x/phantom.phantomSize[0]);
			if(voxelIndex[0]<0) voxelIndex[0] = 0;
			if(voxelIndex[0]>=phantom.voxel.nx) voxelIndex[0] = phantom.voxel.nx-1;
			voxelIndex[1] = phantom.voxel.ny-1;
			voxelIndex[2] = (int)(phantom.voxel.nz*z/phantom.phantomSize[2]);
			if(voxelIndex[2]<0) voxelIndex[2] = 0;
			if(voxelIndex[2]>=phantom.voxel.nz) voxelIndex[2] = phantom.voxel.nz-1;
			part.setPosition(x, phantom.phantomSize[1], z);
			inside = true;
		}
	}

	// from bottom side
	if(!inside && p[2]<=0 && u[2]>0) {
		d = (0-p[2])/u[2];
		x = p[0]+d*u[0];
		y = p[1]+d*u[1];
		if(x>=0 && x<phantom.phantomSize[0] && y>=0 && y<phantom.phantomSize[1]) {
			voxelIndex[0] = (int)(phantom.voxel.nx*x/phantom.phantomSize[0]);
			if(voxelIndex[0]<0) voxelIndex[0] = 0;
			if(voxelIndex[0]>=phantom.voxel.nx) voxelIndex[0] = phantom.voxel.nx-1;
			voxelIndex[1] = (int)(phantom.voxel.ny*y/phantom.phantomSize[1]);
			if(voxelIndex[1]<0) voxelIndex[1] = 0;
			if(voxelIndex[1]>=phantom.voxel.ny) voxelIndex[1] = phantom.voxel.ny-1;
			voxelIndex[2] = 0;
			part.setPosition(x, y, 0);
			inside = true;
		}
	}

	// from top side
	if(!inside && p[2]>=phantom.phantomSize[2] && u[2]<0) {
		d = (phantom.phantomSize[2]-p[2])/u[2];
		x = p[0]+d*u[0];
		y = p[1]+d*u[1];
		if(x>=0 && x<phantom.phantomSize[0] && y>=0 && y<phantom.phantomSize[1]) {
			voxelIndex[0] = (int)(phantom.voxel.nx*x/phantom.phantomSize[0]);
			if(voxelIndex[0]<0) voxelIndex[0] = 0;
			if(voxelIndex[0]>=phantom.voxel.nx) voxelIndex[0] = phantom.voxel.nx-1;
			voxelIndex[1] = (int)(phantom.voxel.ny*y/phantom.phantomSize[1]);
			if(voxelIndex[1]<0) voxelIndex[1] = 0;
			if(voxelIndex[1]>=phantom.voxel.ny) voxelIndex[1] = phantom.voxel.ny-1;
			voxelIndex[2] = phantom.voxel.nz-1;
			part.setPosition(x, y, phantom.phantomSize[2]);
			inside = true;
		}
	}

	// skip empty voxels
    part.setVoxelIndex(voxelIndex);
	while(inside && phantom.voxel[voxelIndex[0]][voxelIndex[1]][voxelIndex[2]]==255) {
	    inside = moveToNextVoxel(part);
	    if(inside) part.getVoxelIndex(voxelIndex);
	}

	if(inside) {
	    part.setVoxelIndex(voxelIndex);
		part.setOrganIndex(phantom.voxel[voxelIndex[0]][voxelIndex[1]][voxelIndex[2]]);
	}

	return inside;
}


//=============================================================================
/// Finds the closest neighbour voxel along the particle beam and evaluates
/// the distance and the index of the neigbour voxel.
/// \return true if neighbour is inside phantom cuboid
bool cMonteCarlo::distanceToNextVoxel(
    const cParticle &part,  ///< [in] particle to be analysed
    double &dist,           ///< [out] distance to next voxel
    int newVoxelIndex[3]    ///< [out] index of next voxel
)
//=============================================================================
{
	double p[3];		// position of particle
	double u[3];		// direction of particle
	double d;			// current distance to next plane
	int direction=-1;	// direction towards: bottom, top, right, left, back, front
	bool inside=true;	// return value: true if neighbour is inside phantom, false otherwise

	// get position, direction and voxel index
	part.getPosition(p);
	part.getDirection(u);
	part.getVoxelIndex(newVoxelIndex);
	dist = DBL_MAX;

	// towards left
	if(u[0]<0) {
		d = (newVoxelIndex[0]*phantom.phantomSize[0]/phantom.voxel.nx-p[0])/u[0];
		if(dist>d) {
			dist = d;
			direction = 0;
		}
	}

	// towards right
	if(u[0]>0) {
		d = ((newVoxelIndex[0]+1)*phantom.phantomSize[0]/phantom.voxel.nx-p[0])/u[0];
		if(dist>d) {
			dist = d;
			direction = 1;
		}
	}

	// towards back
	if(u[1]<0) {
		d = (newVoxelIndex[1]*phantom.phantomSize[1]/phantom.voxel.ny-p[1])/u[1];
		if(dist>d) {
			dist = d;
			direction = 2;
		}
	}

	// towards front
	if(u[1]>0) {
		d = ((newVoxelIndex[1]+1)*phantom.phantomSize[1]/phantom.voxel.ny-p[1])/u[1];
		if(dist>d) {
			dist = d;
			direction = 3;
		}
	}

	// towards bottom
	if(u[2]<0) {
		d = (newVoxelIndex[2]*phantom.phantomSize[2]/phantom.voxel.nz-p[2])/u[2];
		if(dist>d) {
			dist = d;
			direction = 4;
		}
	}

	// towards top
	if(u[2]>0) {
		d = ((newVoxelIndex[2]+1)*phantom.phantomSize[2]/phantom.voxel.nz-p[2])/u[2];
		if(dist>d) {
			dist = d;
			direction = 5;
		}
	}

	// handle direction
	switch(direction) {
	case 0:	// towards left
		inside = --newVoxelIndex[0] >= 0;
		break;
	case 1:	// towards right
		inside = ++newVoxelIndex[0] < phantom.voxel.nx;
		break;
	case 2:	// towards back
		inside = --newVoxelIndex[1] >= 0;
		break;
	case 3:	// towards front
		inside = ++newVoxelIndex[1] < phantom.voxel.ny;
		break;
	case 4:	// towards bottom
		inside = --newVoxelIndex[2] >= 0;
		break;
	case 5:	// towards top
		inside = ++newVoxelIndex[2] < phantom.voxel.nz;
		break;
	}

	return inside;
}


//=============================================================================
/// Moves the particle to the closest neighbour voxel along the particle beam.
/// \return true if next voxel is still whithin phantom
bool cMonteCarlo::moveToNextVoxel(
    cParticle &part   ///< [in,out] particle to be moved
)
//=============================================================================
{
    double dist;
    int newVoxelIndex[3];

    if(distanceToNextVoxel(part, dist, newVoxelIndex)) {
        part.move(dist);
        part.setVoxelIndex(newVoxelIndex);
        return true;
    }
    part.move(dist);
    return false;
}
