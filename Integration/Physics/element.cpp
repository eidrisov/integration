//=============================================================================
// Filename: element.cpp
// Version: 0.3
// Date: June 27th 2020
// Author: Robert Hess
// Description: Provides all atomic data of elements with atomic number
// from 1 to 100 required for the Monte Carlo simulation.
//=============================================================================

// Avoid Microsoft specific warnings and errors
#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "element.h"
#include <cmath>

using namespace std;

// definition of static attributes
bool cElement::prepared = false;
float cElement::A[100];
float cElement::binding[100][61];
std::vector<cElement::tDataPoint> cElement::photoTotal[100];
std::vector<cElement::tDataPoint> cElement::ComptonTotal[100];
std::vector<cElement::tDataPoint> cElement::RayleighTotal[100];
std::vector<cElement::tDataPoint> cElement::photoSubshell[100][61];
std::vector<cElement::tCharEm> cElement::charEm[100][61];
std::vector<cElement::tAugerEm> cElement::AugerEm[100][61];
std::vector<cElement::tDataPoint> cElement::formFactor[100];
std::vector<cElement::tDataPoint> cElement::scatteringFunction[100];


//=============================================================================
cElement::cElement()
//=============================================================================
{
}


//=============================================================================
cElement::~cElement()
//=============================================================================
{
}


//=============================================================================
void cElement::clear()
//=============================================================================
{
	prepared = false;
	for(unsigned i=0; i<100; i++) {
		A[i] = 0;
		for(unsigned j=0; j<61; j++) {
			binding[i][j] = 0;
			photoSubshell[i][j].resize(0);
			charEm[i][j].resize(0);
			AugerEm[i][j].resize(0);
		}
		photoTotal[i].resize(0);
		ComptonTotal[i].resize(0);
		RayleighTotal[i].resize(0);
		formFactor[i].resize(0);
		scatteringFunction[i].resize(0);
	}
}


//=============================================================================
void cElement::prepare()
//=============================================================================
{
	if (!prepared) {
		prepareBinding();
		prepareTableOfPoints("./SimulationFiles/photoTotal.dat", "photoTotal", photoTotal);
		prepareTableOfPoints("./SimulationFiles/ComptonTotal.dat", "ComptonTotal", ComptonTotal);
		prepareTableOfPoints("./SimulationFiles/RayleighTotal.dat", "RayleighTotal", RayleighTotal);
		preparePhotoSubshell();
		prepareCharEmission();
		prepareAugerEmission();
		prepareTableOfPoints("./SimulationFiles/formFactor.dat", "formFactor", formFactor);
		prepareTableOfPoints("./SimulationFiles/scatteringFunction.dat", "scatteringFunction", scatteringFunction);
		prepared = true;
	}
}


//=============================================================================
double cElement::getRelAtomicWeight(unsigned Z)
//=============================================================================
{
	// check if prepared
	if (!prepared) return 0.0;

	// validate atomic number
	if (Z < 1 || Z>100) return 0.0;

	// return relative atomic weight
	return A[Z - 1];
}


//=============================================================================
double cElement::getBindingEnegry(unsigned Z, unsigned designator)
//=============================================================================
{
	// check if prepared
	if (!prepared) return 0.0;

	// validate atomic number
	if (Z < 1 || Z>100) return 0.0;

	// validate designator
	if (designator < 1 || designator>61) return 0.0;

	// return binding energy of given subshell
	return binding[Z - 1][designator - 1];
}


//=============================================================================
double cElement::getPhotoTotal(unsigned Z, double energy)
//=============================================================================
{
	uint32_t maxIndex;		// index of second last element
	uint32_t step;			// step size for finding energy index
	uint32_t i = 0;			// index of energy below desired value
	double crossSection;	// interpolated cross section

	// check if prepared
	if (!prepared) return 0.0;
	// validate atomic number
	if (Z < 1 || Z>100) return 0.0;
	// validate energy
	if (energy < 1 || energy > 1000) return 0.0;

	// get maximum index and find largest step to find energy
	maxIndex = photoTotal[Z - 1].size() - 2;
	for (step = 0x80000000; step && !(step&maxIndex); step >>= 1);

	// find index
	while (step) {
		if ((i | step) <= maxIndex && energy >= photoTotal[Z - 1][i | step].x)
			i |= step;
		step >>= 1;
	}

	// apply interpolation
	crossSection = photoTotal[Z - 1][i].y * exp(
		+ log(photoTotal[Z - 1][i + 1].y / photoTotal[Z - 1][i].y)
		* log(energy / photoTotal[Z - 1][i].x)
		/ log(photoTotal[Z - 1][i + 1].x / photoTotal[Z - 1][i].x));

	return crossSection;
}


//=============================================================================
double cElement::getComptonTotal(unsigned Z, double energy)
//=============================================================================
{
	uint32_t maxIndex;		// index of second last element
	uint32_t step;			// step size for finding energy index
	uint32_t i = 0;			// index of energy below desired value
	double crossSection;	// interpolated cross section

							// check if prepared
	if (!prepared) return 0.0;
	// validate atomic number
	if (Z < 1 || Z>100) return 0.0;
	// validate energy
	if (energy < 1 || energy > 1000) return 0.0;

	// get maximum index and find largest step to find energy
	maxIndex = ComptonTotal[Z - 1].size() - 2;
	for (step = 0x80000000; step && !(step&maxIndex); step >>= 1);

	// find index
	while (step) {
		if ((i | step) <= maxIndex && energy >= ComptonTotal[Z - 1][i | step].x)
			i |= step;
		step >>= 1;
	}

	// apply interpolation
	crossSection = ComptonTotal[Z - 1][i].y * exp(
		+log(ComptonTotal[Z - 1][i + 1].y / ComptonTotal[Z - 1][i].y)
		* log(energy / ComptonTotal[Z - 1][i].x)
		/ log(ComptonTotal[Z - 1][i + 1].x / ComptonTotal[Z - 1][i].x));

	return crossSection;
}


//=============================================================================
double cElement::getRayleighTotal(unsigned Z, double energy)
//=============================================================================
{
	uint32_t maxIndex;		// index of second last element
	uint32_t step;			// step size for finding energy index
	uint32_t i = 0;			// index of energy below desired value
	double crossSection;	// interpolated cross section

	// check if prepared
	if (!prepared) return 0.0;
	// validate atomic number
	if (Z < 1 || Z>100) return 0.0;
	// validate energy
	if (energy < 1 || energy > 1000) return 0.0;

	// get maximum index and find largest step to find energy
	maxIndex = RayleighTotal[Z - 1].size() - 2;
	for (step = 0x80000000; step && !(step&maxIndex); step >>= 1);

	// find index
	while (step) {
		if ((i | step) <= maxIndex && energy >= RayleighTotal[Z - 1][i | step].x)
			i |= step;
		step >>= 1;
	}

	// apply interpolation
	crossSection = RayleighTotal[Z - 1][i].y * exp(
		+log(RayleighTotal[Z - 1][i + 1].y / RayleighTotal[Z - 1][i].y)
		* log(energy / RayleighTotal[Z - 1][i].x)
		/ log(RayleighTotal[Z - 1][i + 1].x / RayleighTotal[Z - 1][i].x));

	return crossSection;
}


//=============================================================================
double cElement::getTotalCrossSection(unsigned Z, double energy)
//=============================================================================
{
	return getPhotoTotal(Z, energy) + getComptonTotal(Z, energy) + getRayleighTotal(Z, energy);
}


//=============================================================================
double cElement::getPhotoSubshell(unsigned Z, unsigned designator, double energy)
//=============================================================================
{
	uint32_t maxIndex;		// index of second last element
	uint32_t step;			// step size for finding energy index
	uint32_t i = 0;			// index of energy below desired value
	double crossSection;	// interpolated cross section

	// check if prepared
	if (!prepared) return 0.0;
	// validate atomic number
	if (Z < 1 || Z>100) return 0.0;
	// validate designator
	if (designator < 1 || designator>61) return 0.0;
	// validate energy
	if (energy < 1 || energy > 1000) return 0.0;
	// if below lowest energy return zero
	if (energy < photoSubshell[Z - 1][designator - 1][0].x) return 0.0;

	// get maximum index and find largest step to find energy
	maxIndex = photoSubshell[Z - 1][designator-1].size() - 2;
	for (step = 0x80000000; step && !(step&maxIndex); step >>= 1);

	// find index
	while (step) {
		if ((i | step) <= maxIndex && energy >= photoSubshell[Z - 1][designator - 1][i | step].x)
			i |= step;
		step >>= 1;
	}

	// apply interpolation
	crossSection = photoSubshell[Z - 1][designator - 1][i].y * exp(
		+log(photoSubshell[Z - 1][designator - 1][i + 1].y / photoSubshell[Z - 1][designator - 1][i].y)
		* log(energy / photoSubshell[Z - 1][designator - 1][i].x)
		/ log(photoSubshell[Z - 1][designator - 1][i + 1].x / photoSubshell[Z - 1][designator - 1][i].x));

	return crossSection;
}


//=============================================================================
void cElement::getPhotoSubshells(unsigned Z, std::vector<unsigned> &desig)
//=============================================================================
{
	unsigned is;	// index for subshell

	// if not prepared return
	if (!prepared) return;

	desig.clear();

	for (is = 0; is < 61; is++)
		if (photoSubshell[Z - 1][is].size() > 0)
			desig.push_back(is + 1);
}


//=============================================================================
void cElement::getCharEmissionTable(unsigned Z, unsigned subshell, std::vector<tCharEm> &table)
//=============================================================================
{
	// check if prepared
	if (!prepared) return;

	// validate atomic number
	if (Z < 1 || Z>100) return;

	// validate designator
	if (subshell < 1 || subshell>61) return;

	table = charEm[Z - 1][subshell - 1];
}


//=============================================================================
void cElement::getAugerEmissionTable(unsigned Z, unsigned subshell, std::vector<tAugerEm> &table)
//=============================================================================
{
	// check if prepared
	if (!prepared) return;

	// validate atomic number
	if (Z < 1 || Z>100) return;

	// validate designator
	if (subshell < 1 || subshell>61) return;

	table = AugerEm[Z - 1][subshell - 1];
}


//=============================================================================
double cElement::getFormFactor(	// [out] form factor without unit
	unsigned Z,					// [in] atomic number
	double x)					// [in] angular variable in 1/cm
//=============================================================================
{
	uint32_t maxIndex;		// index of second last element
	uint32_t step;			// step size for finding energy index
	uint32_t i = 0;			// index of energy below desired value
	double crossSection;	// interpolated cross section

	// check if prepared
	if (!prepared) return 0.0;
	// validate atomic number
	if (Z < 1 || Z>100) return 0.0;
	// validate X
	if (x < 0 || x > 1e12) return 0.0;

	// special treatment between first two table entries
	if (x <= formFactor[Z - 1][1].x) return formFactor[Z - 1][1].y;

	// get maximum index and find largest step to find energy
	maxIndex = formFactor[Z - 1].size() - 2;
	for (step = 0x80000000; step && !(step&maxIndex); step >>= 1);

	// find index
	while (step) {
		if ((i | step) <= maxIndex && x >= formFactor[Z - 1][i | step].x)
			i |= step;
		step >>= 1;
	}

	// apply interpolation
	crossSection = formFactor[Z - 1][i].y * exp(
		+log(formFactor[Z - 1][i + 1].y / formFactor[Z - 1][i].y)
		* log(x / formFactor[Z - 1][i].x)
		/ log(formFactor[Z - 1][i + 1].x / formFactor[Z - 1][i].x));

	return crossSection;
}


//=============================================================================
double cElement::getScatteringFunction(	// [out] scattering function without unit
	unsigned Z,							// [in] atomic number
	double x)							// [in] angular variable in 1/cm
//=============================================================================
{
	uint32_t maxIndex;		// index of second last element
	uint32_t step;			// step size for finding energy index
	uint32_t i = 0;			// index of energy below desired value
	double sf;				// interpolated scatter function

	// check if prepared
	if (!prepared) return 0.0;
	// validate atomic number
	if (Z < 1 || Z>100) return 0.0;
	// validate x
	if (x <= 0) return 0.0;
	if (x > scatteringFunction[Z - 1].back().x) x = scatteringFunction[Z - 1].back().x;

	// get maximum index and find largest step to find x
	maxIndex = scatteringFunction[Z - 1].size() - 2;
	for (step = 1 << 31; step && !(step&maxIndex); step >>= 1);

	// find index
	while (step) {
		if ((i | step) <= maxIndex && x >= scatteringFunction[Z - 1][i | step].x)
			i |= step;
		step >>= 1;
	}

	// apply log-log interpolation
	if (i == 0) i++;	// for i=0 a logarithmic exptrapolation is applied
	if (x == 0) sf = 0;
	else {
		sf = scatteringFunction[Z - 1][i].y * exp(
			log(scatteringFunction[Z - 1][i + 1].y / scatteringFunction[Z - 1][i].y)
			* log(x / scatteringFunction[Z - 1][i].x)
			/ log(scatteringFunction[Z - 1][i + 1].x / scatteringFunction[Z - 1][i].x));
	}

	return sf;
}


//=============================================================================
double cElement::getKleinNishina(	// [out] differential cross section in barn
	double energy,					// [in] photon energy in keV
	double theta)					// [in] polar angle in rad
//=============================================================================
{
//	const static double alpha = 1 / 137.036;	// fine structure constant
//	const static double rce = 3.861592675e-13;	// reduced Compton wave length for electrons in m
//	const static double mec2 = 510.9989461;		// electron rest mass energy in keV
	double epsilon;

	epsilon = 1 / (1 + energy / 510.9989461 * (1 - cos(theta)));

	return 3.97039377947e-2 * epsilon * epsilon * (epsilon + 1 / epsilon - sin(theta) * sin(theta));

//	return alpha * alpha * rce * rce * epsilon * epsilon / 2
//		* (epsilon + 1 / epsilon - sin(theta) * sin(theta)) * 1e28;
}


//=============================================================================
double cElement::getComptonDiff(unsigned Z, double energy, double theta)
//=============================================================================
{
	return getScatteringFunction(Z, 8.065544e6*energy*sin(theta / 2))
		* getKleinNishina(energy, theta);
}


//=============================================================================
double cElement::getRayleighDiff(unsigned Z, double energy, double theta)
//=============================================================================
{
	double ts;		// Thompson scattering
	double ff;		// form factor

	// Thompson scattering
	ts = cos(theta);
	ts = 0.039704 * (1 + ts * ts);

	// form factor
	ff = 8.065544e6 * energy * sin(theta / 2);
	ff = getFormFactor(Z, ff);

	// return differential cross section of Rayleigh effect
	return ts *ff * ff;
}


//=============================================================================
void cElement::prepareBinding()
//=============================================================================
{
	ifstream inp;	// input file with atomic data
	uint32_t Z;		// actual atomic number
	uint32_t tmpU;	// temporary unsigned value
	float flt;		// temporary float value
	uint32_t nt;	// number of table entires
	uint32_t it;	// index for table entry

	// start reading input file
	startReadingFile("./SimulationFiles/bindingEnergy.dat", "bindingEnergy", inp);

	// loop over all elements
	for (Z = 1; Z <= 100; Z++) {

		// read and check atomic number
		inp.read((char*)&tmpU, sizeof(uint32_t));
		if (tmpU != Z) throw runtime_error("Unexpected atomic number in file >bindingEnergy.dat<.");

		// read atomic weight
		inp.read((char*)(A + Z - 1), sizeof(float));

		// read number of table entries
		inp.read((char*)&nt, sizeof(uint32_t));

		// initialize vector for binding energy with zero
		for (it = 0; it < 61; it++) binding[Z - 1][it] = 0.0;

		// loop over all table entries
		for (it = 0; it < nt; it++) {

			// read and check sub shell designator
			inp.read((char*)&tmpU, sizeof(uint32_t));
			if (tmpU < 1 || tmpU>61) throw runtime_error("Bad subshell designator in file >bindingEnergy.dat<.");

			// read and store binding energy
			inp.read((char*)&flt, sizeof(float));
			binding[Z - 1][tmpU - 1] = flt;
		}
	}

	// close file
	inp.close();
}


//=============================================================================
void cElement::prepareTableOfPoints(std::string filename, std::string tablename, std::vector<tDataPoint> *table)
//=============================================================================
{
	ifstream inp;	// input file with atomic data
	uint32_t Z;		// actual atomic number
	uint32_t tmpU;	// temporary unsigned value
	float flt;		// temporary float value
	uint32_t nt;	// number of table entires
	uint32_t it;	// index for table entry
	char message[99];	// error message

	// start reading input file
	startReadingFile(filename, tablename, inp);

	// loop over all elements
	for (Z = 1; Z <= 100; Z++) {

		// read and check atomic number
		inp.read((char*)&tmpU, sizeof(uint32_t));
		if (tmpU != Z) {
			sprintf(message, "Unexpected atomic number in file >%s<.", filename.c_str());
			throw runtime_error(message);
		}

		// read number of table entries
		inp.read((char*)&nt, sizeof(uint32_t));

		// set size of vector
		table[Z - 1].resize(nt);

		// loop over all table entries
		for (it = 0; it < nt; it++) {

			// read and store photon energy
			inp.read((char*)&flt, sizeof(float));
			table[Z - 1][it].x = flt;

			// read and store cross section
			inp.read((char*)&flt, sizeof(float));
			table[Z - 1][it].y = flt;
		}
	}

	// close file
	inp.close();
}


//=============================================================================
void cElement::preparePhotoSubshell()
//=============================================================================
{
	ifstream inp;	// input file with atomic data
	uint32_t Z;		// actual atomic number
	uint32_t nZ;	// number of tables for actual element
	uint32_t iZ;	// index for table
	uint32_t desig;	// current subshell disignator
	uint32_t tmpU;	// temporary unsigned value
	float flt;		// temporary float value
	uint32_t nt;	// number of table entires
	uint32_t it;	// index for table entry

	// start reading input file
	startReadingFile("./SimulationFiles/photoSubshell.dat", "photoSubshell", inp);

	// loop over all elements
	for (Z = 1; Z <= 100; Z++) {

		// read and check atomic number
		inp.read((char*)&tmpU, sizeof(uint32_t));
		if (tmpU != Z) throw runtime_error("Unexpected atomic number in file >photoSubshell.dat<.");

		// read number of tables for actual element
		inp.read((char*)&nZ, sizeof(uint32_t));

		// loop over all table entries
		for (iZ = 0; iZ < nZ; iZ++) {

			// read subshell designator
			inp.read((char*)&desig, sizeof(uint32_t));

			// read number of table entries
			inp.read((char*)&nt, sizeof(uint32_t));

			// set size of vector
			photoSubshell[Z - 1][desig - 1].resize(nt);

			// loop over all table entries
			for (it = 0; it < nt; it++) {

				// read and store photon energy
				inp.read((char*)&flt, sizeof(float));
				photoSubshell[Z - 1][desig - 1][it].x = flt;

				// read and store cross section
				inp.read((char*)&flt, sizeof(float));
				photoSubshell[Z - 1][desig - 1][it].y = flt;
			}
		}
	}

	// close file
	inp.close();
}


//=============================================================================
void cElement::prepareCharEmission()
//=============================================================================
{
	ifstream inp;	// input file with atomic data
	uint32_t Z;		// actual atomic number
	uint32_t nZ;	// number of tables for actual element
	uint32_t iZ;	// index for table
	uint32_t desig;	// current subshell disignator
	uint32_t tmpU;	// temporary unsigned value
	float flt;		// temporary float value
	uint32_t nt;	// number of table entires
	uint32_t it;	// index for table entry

	// start reading input file
	startReadingFile("./SimulationFiles/charEmission.dat", "charEmission", inp);

	// loop over all elements
	for (Z = 1; Z <= 100; Z++) {

		// read and check atomic number
		inp.read((char*)&tmpU, sizeof(uint32_t));
		if (tmpU != Z) throw runtime_error("Unexpected atomic number in file >charEmission.dat<.");

		// read number of tables for actual element
		inp.read((char*)&nZ, sizeof(uint32_t));

		// loop over all tables of actual element
		for (iZ = 0; iZ < nZ; iZ++) {

			// read subshell designator
			inp.read((char*)&desig, sizeof(uint32_t));

			// read number of table entries and set table size
			inp.read((char*)&nt, sizeof(uint32_t));
			charEm[Z - 1][desig - 1].resize(nt);

			// loop over all table entries
			for (it = 0; it < nt; it++) {

				// read and store new subshell designator
				inp.read((char*)&tmpU, sizeof(uint32_t));
				charEm[Z - 1][desig - 1][it].vacancy = tmpU;

				// read and store probability
				inp.read((char*)&flt, sizeof(float));
				charEm[Z - 1][desig - 1][it].prob = flt;
			}
		}
	}

	// close file
	inp.close();
}


//=============================================================================
void cElement::prepareAugerEmission()
//=============================================================================
{
	ifstream inp;	// input file with atomic data
	uint32_t Z;		// actual atomic number
	uint32_t nZ;	// number of tables for actual element
	uint32_t iZ;	// index for table
	uint32_t desig;	// current subshell disignator
	uint32_t tmpU;	// temporary unsigned value
	float flt;		// temporary float value
	uint32_t nt;	// number of table entires
	uint32_t it;	// index for table entry

					// start reading input file
	startReadingFile("./SimulationFiles/AugerEmission.dat", "AugerEmission", inp);

	// loop over all elements
	for (Z = 1; Z <= 100; Z++) {

		// read and check atomic number
		inp.read((char*)&tmpU, sizeof(uint32_t));
		if (tmpU != Z) throw runtime_error("Unexpected atomic number in file >AugerEmission.dat<.");

		// read number of tables for actual element
		inp.read((char*)&nZ, sizeof(uint32_t));

		// loop over all tables of actual element
		for (iZ = 0; iZ < nZ; iZ++) {

			// read subshell designator
			inp.read((char*)&desig, sizeof(uint32_t));

			// read number of table entries and set table size
			inp.read((char*)&nt, sizeof(uint32_t));
			AugerEm[Z - 1][desig - 1].resize(nt);

			// loop over all table entries
			for (it = 0; it < nt; it++) {

				// read and store first new subshell designator
				inp.read((char*)&tmpU, sizeof(uint32_t));
				AugerEm[Z - 1][desig - 1][it].vacancy1 = tmpU;

				// read and store second new subshell designator
				inp.read((char*)&tmpU, sizeof(uint32_t));
				AugerEm[Z - 1][desig - 1][it].vacancy2 = tmpU;

				// read and store probability
				inp.read((char*)&flt, sizeof(float));
				AugerEm[Z - 1][desig - 1][it].prob = flt;
			}
		}
	}

	// close file
	inp.close();
}


//=============================================================================
void cElement::startReadingFile(std::string filename, std::string tablename, std::ifstream &inp)
//=============================================================================
{
	string text;		// actual text read
	uint32_t size;		// size of actual string
	char message[99];	// message to be thrown

	// open file
	inp.open(filename, ios::binary);
	if (!inp.is_open()) {
		sprintf(message, "Could not open file >%s< for reading.", filename.c_str());
		throw runtime_error(message);
	}

	// check file header
	text.resize(8);
	inp.read((char*)text.c_str(), 8);
	text.resize(7);
	if (text.compare("MC-data") != 0) {
		sprintf(message, "Bad title in file >%s<.", filename.c_str());
		throw runtime_error(message);
	}

	// check table header
	inp.read((char*)&size, sizeof(uint32_t));
	text.resize(size);
	inp.read((char*)text.c_str(), size);
	if (text.compare(tablename) != 0) {
		sprintf(message, "Bad table name in file >%s<.", filename.c_str());
		throw runtime_error(message);
	}
}
