#ifndef SIMULATIONRESULT_H_INCLUDED
#define SIMULATIONRESULT_H_INCLUDED

#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <algorithm>
#include "monteCarlo.h"
#include "inputData.h"

struct OrganDose
{
	std::vector<double> dose;
	std::vector<double> accuracy;
};

struct EffectiveDose
{
	std::vector<double> dose;
	std::vector<double> accuracy;
};

class SimulationResults
{
private:
	OrganDose organDose;
	EffectiveDose effectiveDose;
	std::vector<cOrgan> organ;
	std::vector<cEffectiveDoseTissue> effectiveDoseTissue;
	double E;
	double EStdev;

public:
	SimulationResults();
	~SimulationResults();

	void setDoseOfOrganDose(double value);
	void setAccuracyOfOrganDose(double value);
	double getDoseOfOrganDose(int i);
	double getAccuracyOfOrganDose(int i);
	void setDoseOfEffectiveDose(double value);
	void setAccuracyOfEffectiveDose(double value);
	double getDoseOfEffectiveDose(int i);
	double getAccuracyOfEffectiveDose(int i);
	void setOrgan(std::vector<cOrgan> organ);
	void setEffectiveDoseTissue(std::vector<cEffectiveDoseTissue> effectiveDoseTissue);
	int getSizeOfOrgan();
	int getSizeOfEffectiveDoseTissue();
	double getE();
	void setE(double value);
	double getEStdev();
	void setEStdev(double value);
	cOrgan getOrgan(int i);
	cEffectiveDoseTissue getEffectiveDoseTissue(int i);
	bool isEmpty();
	void saveToFile(std::string path);
	void clear();
};

#endif // COMMUNICATIONMODULE_H_INCLUDED

