//=============================================================================
// Filename: randMC.h
// Author: Robert Hess
// Version: 0.3
// Date: June 27th 2020
// Description: A set of random number generators for Monte Carlo simulation.
//=============================================================================

#pragma once
#ifndef HEADER_RANDOM_MC_INCLUDED
#define HEADER_RANDOM_MC_INCLUDED

#include "random.h"
#include "element.h"
#include "material.h"
#include <vector>

class cRandMC
{
private:
	// general attributes
	cRandBase *pRand;
	double minEnergy;	// in keV
	double maxEnergy;	// in keV
	unsigned noOfEnergySteps;
	cElement ele;

	// random generators
	cRandCont randPhotonEnergy;
	std::vector<double> energySteps;
	std::vector<std::vector<double>> meanFreePath;	// in m
	unsigned step0Mfp;
	cRandExponential randExp;
	std::vector<cRandDiscParam> randChooseElement;
	cRandDiscParam randInteraction[100];
	cRandDiscParam randSubshell[100];
	cRandDisc randAugerChar[100][61];
	cRandContParam randComPolAngle[100];
	cRandContParam randRayPolAngle[100];

public:
	cRandMC();
	~cRandMC();
	void clear();
	void setRandBase(cRandBase &rand);
	double prepare(const std::vector<cMaterial> &mat, const std::string &spectrumFileName, double tubeVoltage);
	double getPhotonEnergy();
	double getIsotropicPolar();
	double getIsotropicPolar(double thetaMax);
	void getIsotropicAzimuth(double u[3]);
	double getFreePath(unsigned index, double energy);
	unsigned getInteractingElement(unsigned tissueID, double energy);
	unsigned getInteraction(unsigned Z, double energy);
	unsigned getPhotoSubshell(unsigned Z, double energy);
	double getAugerChar(unsigned Z, unsigned vacancy, unsigned &newVac1, unsigned &newVac2);
	double getComptonPolar(unsigned Z, double energy);
/**/double getRayleighPolar(unsigned Z, double energy);
private:
	double preparePhotonEnergy(const std::string &spectrumFileName, double tubeVoltage);
	void prepareFreePath(const std::vector<cMaterial> &mat);
	void prepareInteractingElement(const std::vector<cMaterial> &mat);
	void prepareInteraction(unsigned Z);
	void prepareSubShellCS(unsigned Z);
	void prepareAugerChar(unsigned Z);
	void prepareComptonPolar(unsigned Z);
	void prepareRayleighPolar(unsigned Z);
};

#endif // #ifndef HEADER_RANDOM_MC_INCLUDED
