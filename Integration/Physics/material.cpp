//=============================================================================
// Filename: Material.cpp
// Author: Robert Hess
// Version: 1.01
// Date: September 30th 2017
// Description: Class to evaluate an absorption spectrum for any material.
//=============================================================================

#include "material.h"
#include "element.h"
#include <exception>

using namespace std;


//=============================================================================
cMaterial::cMaterial()
//=============================================================================
{
	clear();
}


//=============================================================================
cMaterial::~cMaterial()
//=============================================================================
{
}


//=============================================================================
void cMaterial::clear()
//=============================================================================
{
	Z.clear();
	fraction.clear();
	density = 1;
	name = "new material";
}


//=============================================================================
void cMaterial::setName(
	const std::string & newName)	// [in] new name of material
//=============================================================================
{
	name = newName;
}


//=============================================================================
const std::string &cMaterial::getName()
//=============================================================================
{
	return name;
}


//=============================================================================
void cMaterial::addElement(
	unsigned newZ,		// [in] atomic number of new element
	double newFraction)	// [in] fraction by number of new element
//=============================================================================
{
	Z.push_back(newZ);
	fraction.push_back(newFraction);
}


//=============================================================================
void cMaterial::setDensity(
	double newDensity)	// [in] density in g/cm^3
{
	density = newDensity;
}


//=============================================================================
unsigned cMaterial::getNumberOfElements() const
//=============================================================================
{
	return Z.size();
}


//=============================================================================
unsigned cMaterial::getAtomicNumber(unsigned index) const
//=============================================================================
{
	return index < Z.size() ? Z[index] : 0;
}


//=============================================================================
double cMaterial::getFraction(unsigned index) const
//=============================================================================
{
	return index < fraction.size() ? fraction[index] : 0;
}


//=============================================================================
double cMaterial::getMeanFreePath(double energy) const
//=============================================================================
{
	const static double NA = 6.022140857e23;	// Avogadro constant
	unsigned ie;			// index for element
	double meanA;			// mean atomic weight in g/mol
	cElement ele;			// cross section data for all elements
	double meanFreePath;	// mean free path to be evalauted

	// evaluate mean atomic weight
	ele.prepare();
	meanA = 0;
	for (ie = 0; ie < Z.size(); ie++)
		meanA += ele.getRelAtomicWeight(Z[ie])*fraction[ie];

	// evaluate free path
	meanFreePath = 0;
	for (ie = 0; ie < Z.size(); ie++)
		meanFreePath += ele.getTotalCrossSection(Z[ie], energy) * fraction[ie];
	meanFreePath *= density * NA / meanA;
	meanFreePath *= 1e-22;	// b/cm^2 => 1/m
	meanFreePath = 1 / meanFreePath;	// 1/m => m

	return meanFreePath;
}


//=============================================================================
void cMaterial::getAbsorption(
	std::vector<double> & spec,	// [out] absorption spectrum in 1/m
	double minEnergy,			// [in] minimum photon energy in keV
	double tubeVoltage,			// [in] tube voltage in kV (i.e. maximum photon energy in keV)
	unsigned energySteps)		// [in] number of energy steps (the maximum energy is neglected)
//=============================================================================
{
	const double NA = 6.022140857e23;	// Avogadro constant
	unsigned is;	// index in spectrum
	unsigned ie;	// index for element
	double energy;	// actual energy in keV
	double meanA;	// mean atomic weight in g/mol
	cElement ele;	// cross section data for all elements

	// validate data
	if (density <= 0) throw runtime_error("Density must be positive in class cMaterial.");
	if (Z.size() != fraction.size())
		throw runtime_error("Bead element data in class cMaterial.");
	if (Z.size() < 1) throw runtime_error("No element data present in class cMaterial.");

	// set size of spectrum
	spec.resize(energySteps);

	// evaluate mean atomic weight
	ele.prepare();
	meanA = 0;
	for (ie = 0; ie < Z.size(); ie++)
		meanA += ele.getRelAtomicWeight(Z[ie])*fraction[ie];

	// loop over all elements in spectrum
	for (is = 0; is < energySteps; is++) {
		// actual energy
		energy = minEnergy + is * (tubeVoltage - minEnergy) / energySteps;

		// evaluate cross section
		spec[is] = 0;
		for (ie = 0; ie < Z.size(); ie++) {
			spec[is] += ele.getTotalCrossSection(Z[ie], energy) * fraction[ie];
		}
		spec[is] *= density * NA / meanA;
		spec[is] *= 1e-22;	// b/cm^2 => 1/m
	}
}
