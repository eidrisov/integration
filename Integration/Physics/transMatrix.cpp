//=============================================================================
// Filename: transMatrix.cpp
// Author: Robert Hess
// Version: 1.00
// Date: June 27th 2020
// Description: Class to shift and rotate points in space.
//=============================================================================

#include "transMatrix.h"
#include <cstring>
#include <cmath>

//=============================================================================
/// Default constructor. Calls reset() to initialize the transformation matrix as identity matrix.
cTransMatrix::cTransMatrix()
{
	reset();
}

//=============================================================================
/// Destructor. Does nothing.
cTransMatrix::~cTransMatrix()
{
}

//=============================================================================
/// Performs a matrix multiplication.
void cTransMatrix::multiply(
    double mat[4][4])   ///< [in] matrix to multiply the transformation matrix with
{
	double tmp[4][4];

	memcpy(*tmp, *m, 16*sizeof(**m));
	for(unsigned r=0; r<4; r++) {
		for(unsigned c=0; c<4; c++) {
			m[r][c] = 0;
			for(unsigned i=0; i<4; i++)
				m[r][c] += mat[r][i]*tmp[i][c];
		}
	}
}

//=============================================================================
/// Sets the transformation matrix to identity matrix.
void cTransMatrix::reset()
{
	// set to identity matrix
	for(unsigned i=0; i<16; i++) m[i/4][i%4] = i%5?0:1;
}

//=============================================================================
/// Stores a shift in space in the transformation matrix.
void cTransMatrix::shift(
    double deltaX,  ///< [in] shift along positive *x*-axis
    double deltaY,  ///< [in] shift along positive *y*-axis
    double deltaZ)  ///< [in] shift along positive *z*-axis
{
	// prepare factor for matrix multiplication
	double mat[4][4] = {
			{ 1, 0, 0, deltaX },
			{ 0, 1, 0, deltaY },
			{ 0, 0, 1, deltaZ },
			{ 0, 0, 0, 1 }
	};

	// apply to main matrix
	multiply(mat);
}

//=============================================================================
/// Stores a rotation around the *x*-axis in the transformation matrix. A positive angle rotates from the positive *y*-axis towards the positive *z*-axis.
void cTransMatrix::rotateAroundX(
		double angle)   ///< [in] angle clockwise around *x*-axis in rad
{
	// prepare rotation matrix around x-axis
	double cosA=cos(angle);
	double sinA=sin(angle);
	double mat[4][4] = {
			{ 1, 0, 0, 0 },
			{ 0, cosA, -sinA, 0 },
			{ 0, sinA, cosA, 0 },
			{ 0, 0, 0, 1 }
	};

	// apply to main matrix
	multiply(mat);
}

//=============================================================================
/// Stores a rotation around the *y*-axis in the transformation matrix. A positive angle rotates from the positive *z*-axis towards the positive *x*-axis.
void cTransMatrix::rotateAroundY(
		double angle)   ///< [in] angle clockwise around *y*-axis in rad
{
	// prepare rotation matrix around y-axis
	double cosA=cos(angle);
	double sinA=sin(angle);
	double mat[4][4] = {
			{ cosA, 0, sinA, 0 },
			{ 0, 1, 0, 0 },
			{ -sinA, 0, cosA, 0 },
			{ 0, 0, 0, 1 }
	};

	// apply to main matrix
	multiply(mat);
}

//=============================================================================
/// Stores a rotation around the *z*-axis in the transformation matrix. A positive angle rotates from the positive *x*-axis towards the positive *y*-axis.
void cTransMatrix::rotateAroundZ(
		double angle)   ///< [in] angle clockwise around *z*-axis in rad
{
	// prepare rotation matrix around z-axis
	double cosA=cos(angle);
	double sinA=sin(angle);
	double mat[4][4] = {
			{ cosA, -sinA, 0, 0 },
			{ sinA, cosA, 0, 0 },
			{ 0, 0, 1, 0 },
			{ 0, 0, 0, 1 }
	};

	// apply to main matrix
	multiply(mat);
}

//=============================================================================
/// Applies the transformation matrix to a given point.
void cTransMatrix::apply(
    double p[3])    ///< [in,out] point to be transormed
{
	double tmp[4] = { p[0], p[1], p[2], 1.0 };  // store the old point

	// shift point to origin
	p[0] = p[1] = p[2] = 0.0;

	// apply transformation matrix
	for(unsigned r=0; r<3; r++)
		for(unsigned c=0; c<4; c++)
			p[r] += m[r][c]*tmp[c];
}
