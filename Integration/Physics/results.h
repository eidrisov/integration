//=============================================================================
// Filename: results.h
// Author: Robert Hess
// Version: 0.3
// Date: June 22nd 2020
// Description: Class to hold all simulation results.
//=============================================================================

#pragma once
#ifndef HEADER_RESULTS_INCLUDED
#define HEADER_RESULTS_INCLUDED

#include <vector>
#include "vector.h"
#include "medImage.h"
#include "phantom.h"

/// \brief Class to hold all simulation results.
///
/// This is the third of three classes to hold all data for the simulation of effective dose E in human phantoms.
/// The other two classes are cInputData and cPhantom.
class cResults {
public:
	// attributes
	double photonsPerCharge;        ///< number of photons per mAs and µsr
	double totalEnergy;             ///< total energy emitted by the x-ray tube in keV
	double neverInEnergy;           ///< total energy never reached the phantom in keV
	double totalAbsorption;         ///< total absorbed energy in keV
	double escapedEnergy;           ///< total energy that escaped the phantom in keV
	vec::vector3d<double> voxel;    ///< absorbed energy in all phantom voxels in keV
	vec::vector3d<double> voxelVar; ///< variance of absorbed energy in all phantom voxels in keV^2
	std::vector<double> energyO;    ///< scaled absorbed energies of organs in J
	std::vector<double> energyOVar; ///< variance of scaled absorbed energies of organs in J^2
	std::vector<double> massO;      ///< mass of organs in kg
	std::vector<double> energyT;    ///< absorbed energies in tissues for effective dose in J
	std::vector<double> energyTVar; ///< variance of absorbed energies in tissues for effective dose in J^2
	std::vector<double> massT;      ///< mass of tissues for effective dose in kg
	double E;                       ///< effective dose in Sv
	double EStdev;                  ///< standard deviation of effective dose in Sv

	// methods
	cResults();
	cResults(const cResults &res);
	virtual ~cResults();
	void clear();
	void reset();
	void prepare(const cPhantom &phan);
	cResults &operator=(const cResults &res);
	void copy(const cResults &res);
};

#endif // #ifndef HEADER_RESULTS_INCLUDED
