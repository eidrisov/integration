#include "SimulationResults.h"

SimulationResults::SimulationResults()
{

}

SimulationResults::~SimulationResults()
{

}

void SimulationResults::setDoseOfOrganDose(double value)
{
	organDose.dose.push_back(value);
}

void SimulationResults::setAccuracyOfOrganDose(double value)
{
	organDose.accuracy.push_back(value);
}

double SimulationResults::getDoseOfOrganDose(int i)
{
	return organDose.dose.at(i);
}

double SimulationResults::getAccuracyOfOrganDose(int i)
{
	return organDose.accuracy.at(i);
}

void SimulationResults::setDoseOfEffectiveDose(double value)
{
	effectiveDose.dose.push_back(value);
}

void SimulationResults::setAccuracyOfEffectiveDose(double value)
{
	effectiveDose.accuracy.push_back(value);
}

double SimulationResults::getDoseOfEffectiveDose(int i)
{
	return effectiveDose.dose.at(i);
}

double SimulationResults::getAccuracyOfEffectiveDose(int i)
{
	return effectiveDose.accuracy.at(i);
}

void SimulationResults::setOrgan(std::vector<cOrgan> organ)
{
	this->organ = organ;
}

void SimulationResults::setEffectiveDoseTissue(std::vector<cEffectiveDoseTissue> effectiveDoseTissue)
{
	this->effectiveDoseTissue = effectiveDoseTissue;
}

int SimulationResults::getSizeOfOrgan()
{
	return organ.size();
}

int SimulationResults::getSizeOfEffectiveDoseTissue()
{
	return effectiveDoseTissue.size();
}


double SimulationResults::getE()
{
	return E;
}

void SimulationResults::setE(double value)
{
	E = value;
}

double SimulationResults::getEStdev()
{
	return EStdev;
}

void SimulationResults::setEStdev(double value)
{
	EStdev = value;
}

bool SimulationResults::isEmpty()
{
	if (effectiveDose.dose.empty() && effectiveDose.accuracy.empty() &&
		organDose.dose.empty() && organDose.accuracy.empty())
	{
		return true;
	}

	return false;
}

void SimulationResults::saveToFile(std::string path)
{
	std::ofstream myfile;
	std::replace(path.begin(), path.end(), '\\', '/');
	myfile.open(path);

	for (int i = 0; i < effectiveDoseTissue.size(); i++)
	{
		myfile << effectiveDoseTissue.at(i).name << ";" << effectiveDose.dose.at(i) <<
			";" << effectiveDose.accuracy.at(i) << "\n";
	}

	myfile << "\n" << "\n";

	for (int i = 0; i < organ.size(); i++)
	{
		myfile << organ.at(i).name << ";" << organDose.dose.at(i) <<
			";" << organDose.accuracy.at(i) << "\n";
	}

	myfile.close();
}

cOrgan SimulationResults::getOrgan(int i)
{
	return organ.at(i);
}

cEffectiveDoseTissue SimulationResults::getEffectiveDoseTissue(int i)
{
	return effectiveDoseTissue.at(i);
}

void SimulationResults::clear()
{
	organ.clear();
	effectiveDoseTissue.clear();
	organDose.dose.clear();
	organDose.accuracy.clear();
	effectiveDose.dose.clear();
	effectiveDose.accuracy.clear();
}