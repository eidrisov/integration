// filename: Spectrum.h
// author: Robert Hess
// version 1.0
// date: August 26th 2017
// description: A class for calculations with x-ray spectra.

#include "spectrum.h"
#include <cmath>
#include <fstream>
#include <cstring>

using namespace std;


//=============================================================================
/// Default constructor. Does nothing.
cSpectrum::cSpectrum()
{
}


//=============================================================================
/// Copy contructor. Copies the spectrum privided as parameter.
cSpectrum::cSpectrum(
	const cSpectrum & newSpec)	///< [in] spectrum to be copied
{
	// copy spectrum
	spec = newSpec.spec;
}


//=============================================================================
/// Destructor. Does nothing.
cSpectrum::~cSpectrum()
{
}


//=============================================================================
unsigned cSpectrum::size()
{
	// return current size of spectrum
	return spec.size();
}


//=============================================================================
void cSpectrum::resize(
	unsigned size)	///< [in] new number of elements in spectrum
{
	// change size of spectrum
	spec.resize(size);
}



//=============================================================================
double &cSpectrum::operator[](
	unsigned index)	///< [in] index to select element
{
	// check index range
	if (index >= spec.size()) throw runtime_error("Index out of range in class cSpectrum");

	// return requested element of spectrum
	return spec[index];
}


//=============================================================================
cSpectrum &cSpectrum::operator=(
	double value)	///< [in] value to copy in all elements of spectrum
{
	unsigned i;	// index for spectrum elements

	// set all elements of spectrum to given value
	for (i = 0; i < spec.size(); i++)
		spec[i] = value;

	// return current object
	return *this;
}


//=============================================================================
cSpectrum &cSpectrum::operator=(
	const std::vector<double> & newSpec)	///< [in] spectrum to be copied
{
	// copy spectrum
	spec = newSpec;

	// return current object
	return *this;
}


//=============================================================================
cSpectrum &cSpectrum::operator=(
	const cSpectrum & newSpec)	///< [in] spectrum to be copied
{
	// copy spectrum
	spec = newSpec.spec;

	// return current object
	return *this;
}


//=============================================================================
cSpectrum &cSpectrum::operator+=(
	const cSpectrum& summand)	///< [in] spectrum to be added to current spectrum
{
	unsigned i;	// index for all elements in spectrum

	// check if same size
	if (spec.size() != summand.spec.size())
		throw runtime_error("Adding spectra of different size not possible");

	// add elements
	for (i = 0; i < spec.size(); i++) spec[i] += summand.spec[i];

	// return current object
	return *this;
}


//=============================================================================
cSpectrum &cSpectrum::operator*=(
	const cSpectrum& factor)	///< [in] spectrum for elementwise multiplication
{
	unsigned i;	// index for all elements in spectrum

	// check if same size
	if (spec.size() != factor.spec.size())
		throw runtime_error("Multiplying spectra of different size not possible");

	// multiply elements
	for (i = 0; i < spec.size(); i++) spec[i] *= factor.spec[i];

	// return current object
	return *this;
}


//=============================================================================
cSpectrum &cSpectrum::operator*=(
	double factor)	///< [in] factor to multiply all elements of the spectrum
{
	unsigned i;	// index for all elements in spectrum

	// multiply elements
	for (i = 0; i < spec.size(); i++) spec[i] *= factor;

	// return current object
	return *this;
}


//=============================================================================
cSpectrum &cSpectrum::exp()
{
	unsigned i;	// index for all elements in spectrum

	// apply exponential function on elements
	for (i = 0; i < spec.size(); i++) spec[i] = std::exp(spec[i]);

	// return current object
	return *this;
}


//=============================================================================
double cSpectrum::sum()
{
	unsigned i;		// index for all elements in spectrum
	double sum = 0;	// sum of all elements

	// sum up all elements
	for (i = 0; i < spec.size(); i++) sum += spec[i];

	// return sum
	return sum;
}


//=============================================================================
void cSpectrum::readSpectrum(
	const std::string &filename,	///< [in] name of spectrum data file
	double tubeVoltage,				///< [in] requested tube voltage
	double &minEnergy,				///< [out] minimum photon energy in spectrum
	std::string &spectrumName)		///< [out] name of spectrum
{
	ifstream inp;			// input stream for spectrum file
	uint32_t nn;			// number of chars in spectrum name
	uint32_t ns;			// number of spectra
	uint32_t nt;			// number of entries in table
	bool tableFound = false;// true if expected tube voltage found
	float voltage;			// tube voltage of current spectrum
	float minE;				// minimum energy of current spectrum
	float value;			// actual value in spectrum
	char title[15];			// title of file

	// open file
	inp.open(filename, ios::in | ios::binary);
	if (!inp.is_open()) throw runtime_error(string("Could not open spectrum file >")+filename+string("<."));

	// read and check title of file
	inp.read(title, 14);
	title[14] = 0;
	if (strcmp(title, "x-ray spectrum") != 0) throw runtime_error("Bad spectrum file.");

	// read name of spectra and number of spectra
	inp.read((char*)&nn, sizeof(uint32_t));
	spectrumName.resize(nn);
	inp.read(&spectrumName[0], nn);
	inp.read((char*)&ns, sizeof(uint32_t));

	// loop over all tables
	while (ns-- && !inp.eof() && !tableFound) {

		// read tube voltage, minimum energy and number of entries in table
		inp.read((char*)&voltage, sizeof(float));
		inp.read((char*)&minE, sizeof(float));
		inp.read((char*)&nt, sizeof(uint32_t));

		// if correct voltage within 0.1%
		if (abs(voltage / tubeVoltage - 1) < 0.001) {

			// read table
			spec.resize(nt);
			while (nt--) {
				inp.read((char*)&value, sizeof(float));
				spec[spec.size() - nt - 1] = value;
			}
			minEnergy = minE;
			tableFound = true;

		} else {

			// skip table
			inp.seekg(nt * sizeof(float), ios_base::cur);
		}
	}

	// check if table found
	if (!tableFound) throw runtime_error("No spectrum with expected tube voltage found.");
}


//=============================================================================
void cSpectrum::getVoltages(
    const std::string &filename,    ///< [in] name of spectrum data file
    std::string &spectrumName,      ///< [out] name of x-ray spectrum as given in the file
    std::vector<double> &voltage)   ///< [out] list of available tube voltages
{
	ifstream inp;			// input stream for spectrum file
	uint32_t nn;			// number of chars in spectrum name
	uint32_t ns;			// number of spectra
	uint32_t nt;			// number of entries in table
	float flt;  			// temporary storage for float value
	char title[15];			// title of file

	// open file
	inp.open(filename, ios::in | ios::binary);
	if (!inp.is_open()) throw runtime_error(string("Could not open spectrum file >")+filename+string("<."));

	// read and check title of file
	inp.read(title, 14);
	title[14] = 0;
	if (strcmp(title, "x-ray spectrum") != 0) throw runtime_error("Bad spectrum file.");

	// read name of spectra
	inp.read((char*)&nn, sizeof(uint32_t));
	spectrumName.resize(nn);
	inp.read((char*)&spectrumName[0], nn);

	// read number of spectra
	inp.read((char*)&ns, sizeof(uint32_t));
	voltage.resize(0);

	// loop over all tables
	while (ns-- && !inp.eof()) {

		// read tube voltage and skip minimum energy
		inp.read((char*)&flt, sizeof(float));
		voltage.push_back(flt);
    	inp.seekg(sizeof(float), ios_base::cur);

		// read number of entries in table and skip table
		inp.read((char*)&nt, sizeof(uint32_t));
		inp.seekg(nt * sizeof(float), ios_base::cur);
	}
}
