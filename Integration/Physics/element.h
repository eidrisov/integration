//=============================================================================
// Filename: element.h
// Version: 0.3
// Date: June 27th 2020
// Author: Robert Hess
// Description: Provides all atomic data of elements with atomic number
// from 1 to 100 required for the Monte Carlo simulation.
//=============================================================================

#pragma once
#ifndef HEADER_ELEMENT_INCLUDED
#define HEADER_ELEMENT_INCLUDED

#include <vector>
#include <fstream>
#include <string>

class cElement
{
public:
	typedef struct {
		uint32_t vacancy;
		float prob;
	} tCharEm;
	typedef struct {
		uint32_t vacancy1;
		uint32_t vacancy2;
		float prob;
	} tAugerEm;
//private:
	typedef struct {
		float x;
		float y;
	} tDataPoint;
	static bool prepared;
	static float A[100];
	static float binding[100][61];
	static std::vector<tDataPoint> photoTotal[100];
	static std::vector<tDataPoint> ComptonTotal[100];
	static std::vector<tDataPoint> RayleighTotal[100];
	static std::vector<tDataPoint> photoSubshell[100][61];
	static std::vector<tCharEm> charEm[100][61];
	static std::vector<tAugerEm> AugerEm[100][61];
	static std::vector<tDataPoint> formFactor[100];
	static std::vector<tDataPoint> scatteringFunction[100];
public:
	cElement();
	~cElement();
	void clear();
	void prepare();
	double getRelAtomicWeight(unsigned Z);
	double getBindingEnegry(unsigned Z, unsigned designator);
	double getPhotoTotal(unsigned Z, double energy);
	double getComptonTotal(unsigned Z, double energy);
	double getRayleighTotal(unsigned Z, double energy);
	double getTotalCrossSection(unsigned Z, double energy);
	double getPhotoSubshell(unsigned Z, unsigned designator, double energy);
	void getPhotoSubshells(unsigned Z, std::vector<unsigned> &desig);
	void getCharEmissionTable(unsigned Z, unsigned subshell, std::vector<tCharEm> &table);
	void getAugerEmissionTable(unsigned Z, unsigned subshell, std::vector<tAugerEm> &table);
	double getFormFactor(unsigned Z, double x);
	double getScatteringFunction(unsigned Z, double x);
	double getKleinNishina(double energy, double theta);
	double getComptonDiff(unsigned Z, double energy, double theta);
	double getRayleighDiff(unsigned Z, double energy, double theta);
private:
	void prepareBinding();
	void prepareTableOfPoints(std::string filename, std::string tablename, std::vector<tDataPoint> *table);
	void preparePhotoSubshell();
	void prepareCharEmission();
	void prepareAugerEmission();
	void startReadingFile(std::string filename, std::string tablename, std::ifstream &inp);
};

#endif // #ifndef HEADER_ELEMENT_INCLUDED
