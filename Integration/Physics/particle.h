//=============================================================================
// Filename: Particle.h
// Author: Robert Hess
// Version: 1.01
// Date: Jun 24th 2020
// Description: Class to handle a particle for Monte-Carlo simulation.
//=============================================================================

#pragma once
#ifndef HEADER_PARTICLE_INCLUDED
#define HEADER_PARTICLE_INCLUDED

#include <stdint.h>

/// \brief Class to handle a particle for Monte Carlo simulation
class cParticle
{
public:
    /// \brief particle type to be handled
	typedef enum eParticleType {
		photon,         ///< handle photon, i.e. move by free path
		interaction,    ///< handle interaction, choose between photo, Compton or Rayleigh
		photo,          ///< handle photo electric effect
		Compton,        ///< handle Compton effect (incoherent scattering)
		Rayleigh,       ///< handle Rayleigh effect (coherent scattering)
		ion,            ///< handle ion, i.e. Auger or characteristic effect
		absorption      ///< handle absorption
	} tParticleType;
private:
	double p[3];		///< position of particle in m
	double u[3];		///< direction of particle as unit vector
	int voxelIndex[3];	///< index of voxel the particle is in
	uint8_t organIndex;	///< index of organ in current voxel
	double energy;		///< Energy of particle in keV
	tParticleType type;	///< type of particle, see enum tParticleType
	unsigned Z;			///< atomic number (1 - 100)
	unsigned subshell;	///< ENDL subshell designator
public:
	// construction an destruction
	/// Default constructor
	cParticle();
	/// Copy constructor
	cParticle(const cParticle &part);
	/// Destructor
	~cParticle();

	// setters
	/// Assignment operator
	cParticle &operator=(const cParticle &part);
	/// Sets the direction of the particle by the given direction unit vector
	void setDirection(double x, double y, double z);
	/// Sets the direction of the particle by the given direction unit vector
	void setDirection(double u[3]);
	/// Sets the direction of the particle by the given polar and azimuth angle
	void setDirection(double polar, double azimuth);
	/// Sets the position of the particle by the given Cartesian coordinate
	void setPosition(double x, double y, double z);
	/// Sets the position of the particle by the given Cartesian coordinate
	void setPosition(double p[3]);
	/// Sets the index of the voxel the particle is in
	void setVoxelIndex(int index[3]);
	/// Sets the organ index of the voxel the particle is in
	void setOrganIndex(uint8_t index);
	/// Sets the energy of the particle
	void setEnergy(double value);
    /// Sets the type of the particle as defined in enum cParticle::eParticleType
	void setParticleType(tParticleType newType);
    /// Sets the atomic number of an element for specific interactions
	void setAtomicNumber(unsigned newZ);
	/// Sets the ENDL sub-shell designator for ions
	void setSubshell(unsigned newDesig);

	// getters
    /// Gets the direction of the particle as direction unit vector
	void getDirection(double &x, double &y, double &z) const;
    /// Gets the direction of the particle as direction unit vector
	void getDirection(double u[3]) const;
    /// Gets the direction of the particle as polar and azimuth angle
	void getDirection(double &polar, double &azimuth) const;
    /// Gets the position of the particle as Cartesian coordinate
	void getPosition(double &x, double &y, double &z) const;
    /// Gets the position of the particle as Cartesian coordinate
	void getPosition(double p[3]) const;
	/// Gets the index of the voxel the particle is in
	void getVoxelIndex(int index[3]) const;
	/// Gets the organ index of the voxel the particle is in
	uint8_t getOrganIndex() const;
	/// Gets the energy of the particle
	double getEnergy() const;
	/// Gets the particle type
	tParticleType getParticleType() const;
	/// Gets the atomic number of a particular interaction
	unsigned getAtomicNumber() const;
	/// Gets the ENDL sub-shell designator
	unsigned getSubshell() const;

	// geometric calculations
	/// Evaluates the distance between the particle and the given point
	double dist2Point(double x, double y, double z) const;
	/// Evaluates the distance between the particle and the given point
	double dist2Point(double p[3]) const;
	/// Evaluates the minimum distance between the particle beam and the given point
	double minDist2Point(double x, double y, double z) const;
	/// Evaluates the minimum distance between the particle beam and the given point
	double minDist2Point(double p[3]) const;
	/// Evaluates the minimum distance between the particle beam and the origin of the coordinate system
	double minDist2Origin() const;

	// geometric modifications
	/// Moves the particle by distance *d*
	void move(double d);
	/// Changes the direction of the particle by the given direction vector
	void changeDirection(double x, double y, double z);
	/// Changes the direction of the particle by the given direction vector
	void changeDirection(double scatter[3]);
	/// Changes the direction of the particle by the given polar- and atimuth angle
	void changeDirection(double polar, double azimuth);

private:
	/// Cpies a particle with all its attributes
	void copy(const cParticle &part);
};

#endif // #ifndef HEADER_PARTICLE_INCLUDED
