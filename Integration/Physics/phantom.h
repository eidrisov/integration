//=============================================================================
// Filename: phantom.h
// Author: Robert Hess
// Version: 0.3
// Date: June 27th 2020
// Description: A set of classes to handle x-ray phantoms for Monte Carlo simulation.
//=============================================================================

#pragma once
#ifndef HEADER_PHANTOM_INCLUDED
#define HEADER_PHANTOM_INCLUDED

#include "medImage.h"
#include "vector.h"
#include <string>
#include <vector>

//=============================================================================
/// Class for a tissues contributing to the effective dose E.
class cEffectiveDoseTissue {
public:
	std::string name;       ///< name of the tissue
	double weightingFactor; ///< weighting factor of the tissue

	cEffectiveDoseTissue();
	cEffectiveDoseTissue(const cEffectiveDoseTissue &tissue);
	void reset();
	void copy(const cEffectiveDoseTissue &tissue);
	cEffectiveDoseTissue &operator=(const cEffectiveDoseTissue &tissue);
};

//=============================================================================
/// Class for a tissue of an organ in the phantom. It contains the relevant of the tissue for the Monte Carlo simulation.
class cOrganTissue {
public:
	std::string name;                   ///< name of the organ tissue
	std::vector<int> Z;                 ///< atomic numbers of the elements contributing to the tissue
	std::vector<double> massFraction;   ///< fractions by mass of the elements contributing to the tissue

	cOrganTissue();
	cOrganTissue(const cOrganTissue &tissue);
	void reset();
	void copy(const cOrganTissue &tissue);
	cOrganTissue &operator=(const cOrganTissue &tissue);
};

//=============================================================================
/// Class for an organ of the phantom.
class cOrgan {
public:
	std::string name;               ///< name of organ
	int tissueIndex;                ///< zero-based index of the corresponding organ tissue
	double density;	                ///< mass density of organ in g/cm^3
	std::vector<int> effTissueID;   ///< zero based indices of effective dose tissues to which this organ contributes
	std::vector<double> contrib;    ///< contribution factors for effective dose tissues

	cOrgan();
	cOrgan(const cOrgan &organ);
	void reset();
	void copy(const cOrgan &newOrgan);
	cOrgan &operator=(const cOrgan &organ);
};

//=============================================================================
/// Class for all input data of an x-ray phantom to simulate effectove dose E.
class cPhantom {
private:
public:
	uint32_t version;               ///< main version number of phantom file
	uint32_t subversion;            ///< sub-version number of phantom file
	std::string phantomInfo;        ///< info text for phantom
	std::string effDoseInfo;        ///< info text for effective dose calculation with quote of standard (typ. ICRP-103)
	std::vector<cEffectiveDoseTissue> effDoseTissue;    ///< list of effective dose tissues
	std::vector<cOrganTissue> organTissue;  ///< list of organ tissues
	std::vector<cOrgan> organ;      ///< list of organs
	vec::vector3d<uint8_t> voxel;   ///< 3d-vector for all voxels of phantom
	double phantomSize[3];          ///< width (left to right), thickness (back to front) and height (bottom to top) of phantom in m
public:
	cPhantom();
	cPhantom(const cPhantom &phantom);
	virtual ~cPhantom();
	cPhantom &operator=(const cPhantom &phantom);
	static void getPhantomInfo(const std::string &filename, std::string &phantomInfo);
	void readPhantomBin(const std::string &filename);
	void checkPhantom();
	bool getVoxelIndex(double position[3], int index[3]);
	void getImage(cMedImage<double> &image, double pixelSize, int direction);
protected:
	void copy(const cPhantom &phantom);
	static void readStringBin(std::ifstream &inp, std::string &text);
};

#endif // #ifndef HEADER_PHANTOM_INCLUDED
