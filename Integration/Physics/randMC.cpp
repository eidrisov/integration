//=============================================================================
// Filename: randMC.cpp
// Author: Robert Hess
// Version: 0.3
// Date: June 27th 2020
// Description: A set of random number generators for Monte Carlo simulation.
//=============================================================================

#include "randMC.h"
#include "spectrum.h"
#include <string>
#define _USE_MATH_DEFINES
#include <math.h>
#include <vector>

using namespace std;


//=============================================================================
cRandMC::cRandMC()
//=============================================================================
{
	pRand = NULL;
	minEnergy = 1;		// in keV
	maxEnergy = 1000;	// in keV
	noOfEnergySteps = 100;
	step0Mfp = 0;
}


//=============================================================================
cRandMC::~cRandMC()
//=============================================================================
{
}


//=============================================================================
void cRandMC::clear()
//=============================================================================
{
	minEnergy = 1.0;
	maxEnergy = 1000.0;
	noOfEnergySteps = 100;
	ele.clear();
	randPhotonEnergy.reset();
	meanFreePath.resize(0);
//	meanFreePath[0].resize(0);
//	meanFreePath[1].resize(0);
	step0Mfp = 0;
	randExp.reset();
	randChooseElement.resize(0);
	for(unsigned i=0; i<100; i++) {
		randInteraction[i].reset();
		randSubshell[i].reset();
		randComPolAngle[i].reset();
		randRayPolAngle[i].reset();
		for(unsigned j=0; j<61; j++)
			randAugerChar[i][j].reset();
	}
}


//=============================================================================
void cRandMC::setRandBase(cRandBase &rand)
//=============================================================================
{
	pRand = &rand;
}


//=============================================================================
double cRandMC::prepare(const std::vector<cMaterial> &mat, const std::string &spectrumFileName, double tubeVoltage)
//=============================================================================
{
	unsigned im;	// index for material
	unsigned iz;	// index for element in material
	vector<unsigned> Z;
	bool found;
	double photonsPerCharge;    // number of photons per mAs and µsr

	// reset all generators
	clear();

	// prepare the atomic data of the elements
	ele.prepare();

	// if not random base provided throw an exception
	if (!pRand) throw runtime_error("No random generator base found.");

	// preepare generators which are not specific for elements
	photonsPerCharge = preparePhotonEnergy(spectrumFileName, tubeVoltage);
	prepareFreePath(mat);
	prepareInteractingElement(mat);

	// prepare all elements of the material
	for(im=0; im<mat.size(); im++)  {
		for (iz = 0; iz < mat[im].getNumberOfElements(); iz++) {
			found = false;
			for(unsigned i=0; !found && i<Z.size(); i++)
				found = Z[i]==mat[im].getAtomicNumber(iz);
			if(!found) {
				Z.push_back(mat[im].getAtomicNumber(iz));
				prepareInteraction(mat[im].getAtomicNumber(iz));
				prepareSubShellCS(mat[im].getAtomicNumber(iz));
				prepareAugerChar(mat[im].getAtomicNumber(iz));
				prepareComptonPolar(mat[im].getAtomicNumber(iz));
				prepareRayleighPolar(mat[im].getAtomicNumber(iz));
			}
		}
	}

	return photonsPerCharge;
}


//=============================================================================
double cRandMC::getPhotonEnergy()
//=============================================================================
{
	return randPhotonEnergy.rand();
}


//=============================================================================
double cRandMC::getIsotropicPolar()
//=============================================================================
{
	return acos(1 - 2 * pRand->randU());
}


//=============================================================================
double cRandMC::getIsotropicPolar(double thetaMax)
//=============================================================================
{
	return acos(1 - (1 - cos(thetaMax)) * pRand->randU());
}


//=============================================================================
void cRandMC::getIsotropicAzimuth(double u[3])
//=============================================================================
{
	double azimuth;
	double a;

	azimuth = 2 * M_PI * pRand->randU();
	a = sqrt(1 - u[2] * u[2]);
	u[0] = a * sin(azimuth);
	u[1] = a * cos(azimuth);
}


//=============================================================================
double cRandMC::getFreePath(unsigned index, double energy)
//=============================================================================
{
	unsigned i = 0;
	unsigned step;

	// find index i
	for (step = step0Mfp; step; step >>= 1)
		if ((i | step) < noOfEnergySteps && energy>energySteps[i | step])
			i |= step;

	// apply linear interpolation
	return (meanFreePath[index][i] + (meanFreePath[index][i + 1] - meanFreePath[index][i])
		*(energy - energySteps[i]) / (energySteps[i + 1] - energySteps[i])) * randExp.rand();
}


//=============================================================================
unsigned cRandMC::getInteractingElement(unsigned tissueID, double energy)
//=============================================================================
{
	return (unsigned)randChooseElement[tissueID].rand(energy);
}


//=============================================================================
unsigned cRandMC::getInteraction(unsigned Z, double energy)
//=============================================================================
{
	return (unsigned)randInteraction[Z - 1].rand(energy);
}


//=============================================================================
unsigned cRandMC::getPhotoSubshell(unsigned Z, double energy)
//=============================================================================
{
	if (Z <= 2) return 1;
	return (unsigned)randSubshell[Z - 1].rand(energy);
}


//=============================================================================
double cRandMC::getAugerChar(unsigned Z, unsigned vacancy, unsigned &newVac1, unsigned &newVac2)
//=============================================================================
{
	unsigned randValue;

	// if no generator present return binding energy
	if (!randAugerChar[Z - 1][vacancy - 1].isPrepared()) {
		newVac1 = newVac2 = 0;
		return ele.getBindingEnegry(Z, vacancy);
	}

	// get new vacancies
	randValue = (unsigned)randAugerChar[Z - 1][vacancy - 1].rand();
	newVac1 = randValue & 0x3F;
	newVac2 = randValue >> 6;

	// return energy of secondary particle
	return ele.getBindingEnegry(Z, vacancy) - ele.getBindingEnegry(Z, newVac1)
		- (newVac2>0 ? ele.getBindingEnegry(Z, newVac2) : 0);
}


//=============================================================================
double cRandMC::getComptonPolar(unsigned Z, double energy)
//=============================================================================
{
	return randComPolAngle[Z - 1].rand(energy);
}


//=============================================================================
double cRandMC::getRayleighPolar(unsigned Z, double energy)
//=============================================================================
{
	return randRayPolAngle[Z - 1].rand(energy);
}


//=============================================================================
double cRandMC::preparePhotonEnergy(const std::string &spectrumFileName, double tubeVoltage)
//=============================================================================
{
	cSpectrum xray;	// x-ray spectrum of x-ray tube
	double Emin;	// minimum energy in spectrum
	string name;	// temporary name
	unsigned i;		// local index variable
	double photonsPerCharge;    // number of photons per mAs and µsr

	// random number generator for x-ray spectrum
	xray.readSpectrum(spectrumFileName, tubeVoltage, Emin, name);
	randPhotonEnergy.reset();
	randPhotonEnergy.setRandomBase(*pRand);
	for (i = 0; i < xray.size(); i++)
		randPhotonEnergy.addDistPoint(Emin + i*(tubeVoltage - Emin) / xray.size(), xray[i]);
	randPhotonEnergy.addDistPoint(tubeVoltage, 0);
	randPhotonEnergy.prepare();

	// number of photons per mAs and µsr
	photonsPerCharge = 0.0;
	for(int i=0; i<(int)xray.size(); i++) photonsPerCharge += xray[i];
	photonsPerCharge *= (tubeVoltage-Emin)/xray.size();

	return photonsPerCharge;
}


//=============================================================================
void cRandMC::prepareFreePath(const std::vector<cMaterial> &mat)
//=============================================================================
{
	unsigned ie;			// index for energy

	// set number of free path vectors
	meanFreePath.resize(mat.size());

	// prepare energy vector
	energySteps.resize(noOfEnergySteps+1);
	for(ie=0; ie<=noOfEnergySteps; ie++)
		energySteps[ie] = minEnergy*exp(ie*log(maxEnergy/minEnergy)/noOfEnergySteps);
	for(step0Mfp=1<<31; (step0Mfp&(noOfEnergySteps-1))==0; step0Mfp>>=1);

	// loop over all materials
	for(unsigned im=0; im<mat.size(); im++) {

		// set size of mean free path
		meanFreePath[im].resize(noOfEnergySteps+1);

		// loop over all energies
		for(ie=0; ie<=noOfEnergySteps; ie++) {

			// mean free path
			meanFreePath[im][ie] = mat[im].getMeanFreePath(energySteps[ie]);
		}
	}

	// prepare exponential random generator
	randExp.setRandomBase(*pRand);
	// no preparation required for exponential distribution
}


//=============================================================================
void cRandMC::prepareInteractingElement(const std::vector<cMaterial> &mat)
//=============================================================================
{
	vector<double> dist;	// distribution to feed random generator
	unsigned im;			// index for material
	unsigned iz;			// index for element in material
	unsigned ie;			// index for energy
	double energy;			// current photon energy

	// prepare rondom generators to choose element
	randChooseElement.resize(mat.size());

	// loop over all materials
	for(im=0; im<mat.size(); im++) {
		randChooseElement[im].setRandomBase(*pRand);
		dist.resize(mat[im].getNumberOfElements());
		for (iz = 0; iz < dist.size(); iz++)
			dist[iz] = mat[im].getAtomicNumber(iz)+0.5;
		randChooseElement[im].setRandomValues(dist);
		for (ie = 0; ie <= noOfEnergySteps; ie++) {
			energy = minEnergy * exp(ie * log(maxEnergy/minEnergy) / noOfEnergySteps);
			for (iz = 0; iz < dist.size(); iz++)
				dist[iz] = mat[im].getFraction(iz)
					*ele.getTotalCrossSection(mat[im].getAtomicNumber(iz), energy);
			randChooseElement[im].addProbabilities(dist, energy);
		}
		randChooseElement[im].prepare();
	}
}


//=============================================================================
void cRandMC::prepareInteraction(unsigned Z)
//=============================================================================
{
	vector<double> dist(3);	// probabilities for interactions
	unsigned k;				// local index variable
	double energy;			// actual photon energy

	randInteraction[Z - 1].reset();
	randInteraction[Z - 1].setRandomBase(*pRand);
	for (k = 0; k < 3; k++) dist[k] = k + 1.5;
	randInteraction[Z - 1].setRandomValues(dist);
	for (k = 0; k <= noOfEnergySteps; k++) {
		energy = minEnergy * exp(k * log(maxEnergy / minEnergy) / noOfEnergySteps);
		dist[0] = ele.getPhotoTotal(Z, energy);
		dist[1] = ele.getComptonTotal(Z, energy);
		dist[2] = ele.getRayleighTotal(Z, energy);
		randInteraction[Z - 1].addProbabilities(dist, energy);
	}
	randInteraction[Z - 1].prepare();
}


//=============================================================================
void cRandMC::prepareSubShellCS(unsigned Z)
//=============================================================================
{
	vector<unsigned> subshell;	// subshell designators of element
	vector<double> dist;		// actual probability distribution
	unsigned is;				// index for subshell
	unsigned ie;				// index for energy
	double energy;				// actual photon energy

	// retrun for hydrogen and helium
	if (Z <= 2) return;

	// reset generator and set random base
	randSubshell[Z - 1].reset();
	randSubshell[Z - 1].setRandomBase(*pRand);

	// get subshells and set size of distiribution vector
	ele.getPhotoSubshells(Z, subshell);
	dist.resize(subshell.size());

	// set subshell designators as values of generator
	for (is = 0; is < subshell.size(); is++)
		dist[is] = subshell[is] + 0.5;
	randSubshell[Z - 1].setRandomValues(dist);

	// loop over all energies
	for (ie = 0; ie <= noOfEnergySteps; ie++) {

		// actual photon energy
		energy = minEnergy*exp(ie*log(maxEnergy / minEnergy) / noOfEnergySteps);

		// store the subshell cross sections as probabilities in generator
		for (is = 0; is < subshell.size(); is++)
			dist[is] = ele.getPhotoSubshell(Z, subshell[is], energy);
		randSubshell[Z - 1].addProbabilities(dist, energy);
	}

	// prepare the generator
	randSubshell[Z - 1].prepare();
}


//=============================================================================
void cRandMC::prepareAugerChar(unsigned Z)
//=============================================================================
{
	vector<unsigned> subshell;			// list of all subshells of given element
	vector<cElement::tAugerEm> AugerEm;	// probabilities for Auger emission
	vector<cElement::tCharEm> CharEm;	// probabilities for characteristic emission
	unsigned is;						// index for subshell
	unsigned it;						// index for Auger or characteristic emission table

	// get subshells and set size of distiribution vector
	ele.getPhotoSubshells(Z, subshell);

	// loop over all subshells
	for (is = 0; is < subshell.size(); is++) {

		// perform the following steps only if binding energy larger than 1 keV
		if (ele.getBindingEnegry(Z, subshell[is]) >= 1) {

			// Get Auger and characteristic emission tables
			ele.getAugerEmissionTable(Z, subshell[is], AugerEm);
			ele.getCharEmissionTable(Z, subshell[is], CharEm);

			// reset generator and set random base
			randAugerChar[Z - 1][subshell[is] - 1].reset();
			randAugerChar[Z - 1][subshell[is] - 1].setRandomBase(*pRand);

			// store probabilities for Auger emission in generator
			for (it = 0; it < AugerEm.size(); it++)
				randAugerChar[Z - 1][subshell[is] - 1].addDistPoint(AugerEm[it].vacancy1 + (AugerEm[it].vacancy2 << 6) + 0.5, AugerEm[it].prob);

			// store probabilities for characteristic emission in generator
			for (it = 0; it < CharEm.size(); it++)
				randAugerChar[Z - 1][subshell[is] - 1].addDistPoint(CharEm[it].vacancy + 0.5, CharEm[it].prob);

			// prepare generator
			randAugerChar[Z - 1][subshell[is] - 1].prepare();
		}
	}
}


//=============================================================================
void cRandMC::prepareComptonPolar(unsigned Z)
//=============================================================================
{
	unsigned ie;			// index for energy spectrum
	unsigned ia;			// index for angle
	double energy;			// actual energy
	double angle;			// actual angle
	double prob;			// actual probability

	// reset generator and set random base
	randComPolAngle[Z - 1].reset();
	randComPolAngle[Z - 1].setRandomBase(*pRand);

	// loop over all energies
	for (ie = 0; ie <= noOfEnergySteps; ie++) {
		// actual energy
		energy = minEnergy*exp(ie*log(maxEnergy / minEnergy) / noOfEnergySteps);

		// set first point of distribution
		randComPolAngle[Z - 1].add(0, 0, energy);

		// loop over all angles
		for (ia = 1; ia < 180; ia++) {
			// actual angle
			angle = ia * M_PI / 180;

			// actual probability
			prob = ele.getComptonDiff(Z, energy, angle);
			prob *= sin(angle);

			// set point in generator
			randComPolAngle[Z - 1].add(angle, prob, energy);
		}

		// set last point of distribution
		randComPolAngle[Z - 1].add(M_PI, 0, energy);
	}

	// prepare the random generator
	randComPolAngle[Z - 1].prepare();
}


//=============================================================================
void cRandMC::prepareRayleighPolar(unsigned Z)
//=============================================================================
{
	unsigned ie;			// index for energy spectrum
	unsigned ia;			// index for angle
	double energy;			// actual energy
	double angle;			// actual angle
	double prob;			// actual probability

	// reset generator and set random base
	randRayPolAngle[Z - 1].reset();
	randRayPolAngle[Z - 1].setRandomBase(*pRand);

	// loop over all energies
	for (ie = 0; ie <= noOfEnergySteps; ie++) {
		// actual energy
		energy = minEnergy*exp(ie*log(maxEnergy / minEnergy) / noOfEnergySteps);

		// set first point of distribution
		randRayPolAngle[Z - 1].add(0, 0, energy);

		// loop over all angles
		for (ia = 1; ia < 180; ia++) {
			// actual angle
			angle = ia * M_PI / 180;

			// actual probability
			prob = ele.getRayleighDiff(Z, energy, angle);
			prob *= sin(angle);

			// set point in generator
			randRayPolAngle[Z - 1].add(angle, prob, energy);
		}

		// set last point of distribution
		randRayPolAngle[Z - 1].add(M_PI, 0, energy);
	}

	// prepare the random generator
	randRayPolAngle[Z - 1].prepare();
}
