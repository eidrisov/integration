//=============================================================================
// Filename: Material.h
// Author: Robert Hess
// Version: 1.01
// Date: September 30th 2017
// Description: Class to evaluate an absorption spectrum for any material.
//=============================================================================

#pragma once
#ifndef HEADER_MATERIAL_INCLUDED
#define HEADER_MATERIAL_INCLUDED

#include <vector>
#include <string>

class cMaterial
{
private:
	std::vector<unsigned> Z;
	std::vector<double> fraction;
	double density;	// in g/cm^3
	std::string name;
public:
	cMaterial();
	~cMaterial();
	void clear();
	void setName(const std::string & newName);
	const std::string &getName();
	void addElement(unsigned newZ, double newFraction);
	void setDensity(double newDensity);
	double getDensity() { return density; };
	unsigned getNumberOfElements() const;
	unsigned getAtomicNumber(unsigned index) const;
	double getFraction(unsigned index) const;
	double getMeanFreePath(double energy) const;
	void getAbsorption(std::vector<double> & spec, double minEnergy, double tubeVoltage, unsigned energySteps);
};

#endif // #ifndef HEADER_MATERIAL_INCLUDED
