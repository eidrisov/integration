//=============================================================================
// Filename: inputData.cpp
// Version: 0.3
// Date: June 15th 2020
// Author: Robert Hess
// Description: Input settings for Monte-Carlo simulation of x-rays in human phantoms.
//=============================================================================

#define _USE_MATH_DEFINES

#include "inputData.h"
#include <iostream>
#include <cmath>
#include <cstring>

using namespace std;


//=============================================================================
/// Default constructor. Initializes all input data by sensible default values.
cInputData::cInputData()
{
    spectrumFilename = "./Spectrums/SRO33100ROT350.dat";
	tubeVoltage = 120.0;
	currentTimeProduct = 200.0;
	noOfPhotons = 100000;
	randomSeed = 1;
	randomSeedType = RANDOM_SEED_TYPE_DEFAULT;
	phantomFilename = "./Phantoms/Icrp110AdultFemale.ptm";
	systemType = rad;

	tubePos[0] = 0.265;
	tubePos[1] = -1.50;
	tubePos[2] = 1.25;
	tubeRot[0] = -M_PI/2;
	tubeRot[1] = 0.0;
	tubeRot[2] = 0.0;
	collOpening[0] = 0.16;
	collOpening[1] = 0.22;
	collDistance = 0.9;		// distance to focal spot

	radiusCt = 0.6;
	fanAngleWidth = 30*M_PI/180;
	fanAngleThickness = 2*M_PI/180;
	gantryPos[0] = 0.265;
	gantryPos[1] = 0.122;
	gantryPos[2] = 1.60;
	gantryRot[0] = 0.0;
	gantryRot[1] = 0.0;
	gantryRot[2] = 0.0;
}


//=============================================================================
/// Copy constructor. Copies the parameter into the curren object.
cInputData::cInputData(
    const cInputData &input) ///< [in] input data to be copied
{
    copy(input);
}


//=============================================================================
/// Destructor. Does nothing.
cInputData::~cInputData()
{
	// TODO Auto-generated destructor stub
}

//=============================================================================
/// Copies all input data.
void cInputData::copy(
    const cInputData &input) ///< [in] input data to be copied
{
    spectrumFilename = input.spectrumFilename;
	tubeVoltage = input.tubeVoltage;
	currentTimeProduct = input.currentTimeProduct;
	noOfPhotons = input.noOfPhotons;
	randomSeed = input.randomSeed;
	randomSeedType = input.randomSeedType;
	phantomFilename = input.phantomFilename;
	systemType = input.systemType;
	memcpy(tubePos, input.tubePos, 3*sizeof(*tubePos));
	memcpy(collOpening, input.collOpening, 2*sizeof(*tubePos));
	collDistance = input.collDistance;
	memcpy(tubeRot, input.tubeRot, 3*sizeof(*tubeRot));
	radiusCt = input.radiusCt;
	fanAngleWidth = input.fanAngleWidth;
	fanAngleThickness = input.fanAngleThickness;
	memcpy(gantryPos, input.gantryPos, 3*sizeof(*gantryPos));
	memcpy(gantryRot, input.gantryRot, 3*sizeof(*gantryRot));
	checkedOrgans = input.checkedOrgans;
	organStatusChnaged = input.organStatusChnaged;
}

void cInputData::setGeneralInput(cInputData input)
{
	spectrumFilename = "./Spectrums/" + input.spectrumFilename;
	tubeVoltage = input.tubeVoltage;
	currentTimeProduct = input.currentTimeProduct;
	noOfPhotons = input.noOfPhotons;
	randomSeed = input.randomSeed;
	randomSeedType = input.randomSeedType;
	phantomFilename = "./Phantoms/" + input.phantomFilename;
	systemType = input.systemType;
}

void cInputData::setRADInput(cInputData input)
{
	memcpy(tubePos, input.tubePos, 3 * sizeof(*tubePos));
	memcpy(collOpening, input.collOpening, 2 * sizeof(*tubePos));
	collDistance = input.collDistance;
	memcpy(tubeRot, input.tubeRot, 3 * sizeof(*tubeRot));
}

void cInputData::setCTInput(cInputData input)
{
	radiusCt = input.radiusCt;
	fanAngleWidth = input.fanAngleWidth;
	fanAngleThickness = input.fanAngleThickness;
	memcpy(gantryPos, input.gantryPos, 3 * sizeof(*gantryPos));
	memcpy(gantryRot, input.gantryRot, 3 * sizeof(*gantryRot));
}

void cInputData::setOrganInput(cInputData input)
{
	if (!checkedOrgans.empty()) { checkedOrgans.clear(); }
	checkedOrgans = input.checkedOrgans;
}

void cInputData::setOrganStatus(bool status)
{
	organStatusChnaged = status;
}

void cInputData::setAllOrgansChecked(std::vector<cOrgan> organs)
{
	if (!checkedOrgans.empty()) { checkedOrgans.clear(); }
	
	for (int i = 0; i < organs.size(); i++)
	{
		checkedOrgans.insert(std::pair<int, bool>(i, true));
	}
}

void cInputData::saveInAFile(std::string path)
{
	std::ofstream myfile;
	std::replace(path.begin(), path.end(), '\\', '/');
	myfile.open(path);

	myfile << SPECTRUM_FILE_NAME << ";" << spectrumFilename << "\n";
	myfile << TUBE_VOLTAGE << ";" << tubeVoltage << "\n";
	myfile << CURRENT_TIME_PRODUCT << ";" << currentTimeProduct << "\n";
	myfile << NUMBER_OF_PHOTONS << ";" << noOfPhotons << "\n";
	myfile << RANDOM_SEED << ";" << randomSeed << "\n";
	myfile << RANDOM_SEED_TYPE << ";" << randomSeedType << "\n";
	myfile << PHANTOM_FILE_NAME << ";" << phantomFilename << "\n";
	
	if (systemType == rad)
	{
		myfile << SYSTEM_TYPE << ";" << RAD << "\n";
		myfile << TUBE_POS << ";" << tubePos[0] << ";" << tubePos[1] << ";" << tubePos[2] << "\n";
		myfile << COLLIMATOR_OPENING << ";" << collOpening[0] << ";" << collOpening[1] << "\n";
		myfile << COLLIMATOR_DISTANCE << ";" << collDistance << "\n";
		myfile << TUBE_ROT << ";" << tubeRot[0] << ";" << tubeRot[1] << ";" << tubeRot[2] << "\n";
	}
	else
	{
		myfile << SYSTEM_TYPE << ";" << CT << "\n";
		myfile << RADIUS_CT << ";" << radiusCt << "\n";
		myfile << FAN_ANGLE_WIDTH << ";" << fanAngleWidth << "\n";
		myfile << FAN_ANGLE_THICKNESS << ";" << fanAngleThickness << "\n";
		myfile << GANTRY_POS << ";" << gantryPos[0] << ";" << gantryPos[1] << ";" << gantryPos[2] << "\n";
		myfile << GANTRY_ROT << ";" << gantryRot[0] << ";" << gantryRot[1] << ";" << gantryRot[2] << "\n";
	}

	myfile.close();
}

void cInputData::importInput(std::string path)
{
	std::replace(path.begin(), path.end(), '\\', '/');
	std::ifstream myfile(path);

	bool systemIsSet = false;
	std::string delimiter = ";";

	while (!myfile.fail()) {
		std::string input;
		std::stringstream inputStream;
		myfile >> input;

		int count = 0;
		std::string key, token;

		wxMessageOutputDebug().Printf("%s", input);
		for (int i = 0; i < input.length(); i++)
		{
			if (input[i] != delimiter && i <= (input.length() - 1))
			{
				token += input[i];
			}

			if (input[i] != delimiter && i != (input.length() - 1)) { continue; }
			if (count == 0){key = token;}

			wxMessageOutputDebug().Printf("TOKEN: %s", token);
			if (count != 0)
			{
				if (key == SPECTRUM_FILE_NAME)
				{
					spectrumFilename = token;
				}
				else if (key == TUBE_VOLTAGE)
				{
					tubeVoltage = stod(token);
				}
				else if (key == CURRENT_TIME_PRODUCT)
				{
					currentTimeProduct = stod(token);
				}
				else if (key == NUMBER_OF_PHOTONS)
				{
					noOfPhotons = std::stoull(token);
				}
				else if (key == RANDOM_SEED)
				{
					randomSeed = static_cast<uint32_t>(std::stoul(token));
				}
				else if (key == RANDOM_SEED_TYPE)
				{
					randomSeedType = token;
				}
				else if (key == PHANTOM_FILE_NAME)
				{
					phantomFilename = token;
				}
				else if (key == SYSTEM_TYPE)
				{
					if (token == RAD)
					{
						systemType = rad;
					}
					else
					{
						systemType = ct;
					}

					systemIsSet = true;
				}

				if (systemType == rad && systemIsSet)
				{
					if (key == TUBE_POS)
					{
						tubePos[count - 1] = stod(token);
					}
					else if (key == COLLIMATOR_OPENING)
					{
						collOpening[count - 1] = stod(token);
					}
					else if (key == COLLIMATOR_DISTANCE)
					{
						collDistance = stod(token);
					}
					else if (key == TUBE_ROT)
					{
						tubeRot[count - 1] = stod(token);
					}
				}
				else
				{
					if (key == RADIUS_CT && systemIsSet)
					{
						radiusCt = stod(token);
					}
					else if (key == FAN_ANGLE_WIDTH)
					{
						fanAngleWidth = stod(token);
					}
					else if (key == FAN_ANGLE_THICKNESS)
					{
						fanAngleThickness = stod(token);
					}
					else if (key == GANTRY_POS)
					{
						gantryPos[count - 1] = stod(token);
					}
					else if (key == GANTRY_ROT)
					{
						gantryRot[count - 1] = stod(token);
					}
				}
			}

			count++;
			token = "";
		}
	}

	myfile.close();
}