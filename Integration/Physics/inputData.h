//=============================================================================
// Filename: inputData.h
// Version: 0.3
// Date: June 15th 2020
// Author: Robert Hess
// Description: Input settings for Monte-Carlo simulation of x-rays in human phantoms.
//=============================================================================

#pragma once
#ifndef HEADER_INPUT_DATA_INCLUDED
#define HEADER_INPUT_DATA_INCLUDED

#include <wx/wx.h>
#include "../Helpers/Constants.h"
#include "phantom.h"
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <map>

/** Class to store all required data for Monte-Carlo simulations of x-ray in human phantoms.
*/
class cInputData {
public:
    // general input settings
    std::string spectrumFilename;   ///< filename of x-ray spectrum
	double tubeVoltage;			    ///< x-ray tube voltage in kV
	double currentTimeProduct;	    ///< product of tube current and exposure time in mAs
	uint64_t noOfPhotons;		    ///< number of photons to be simulated
	uint32_t randomSeed;		    ///< seed value for random generator
	std::string randomSeedType;
	std::string phantomFilename;    ///< filename of phantom to be simulated
	enum { ct, rad } systemType;    ///< type of x-ray system

	// RAD-system specific input settings
	double tubePos[3];			    ///< x/y/z position of focal spot in RAD-systems in m
	double collOpening[2];          ///< opening of collimator in x- and y-direction in m
	double collDistance;            ///< distance of collimator center to focal spot in m
	double tubeRot[3];			    ///< tube rotation around x-, y- and z-axis for RAD-systems in rad

	// CT-system specific input settings
	double radiusCt;                ///< distance of focal spot to gantry center for CT-systems in m
	double fanAngleWidth;		    ///< fan beam width for CT-systems in rad
	double fanAngleThickness;	    ///< fan beam thickness for CT-systems in rad
	double gantryPos[3];            ///< x/y/z position of gantry center for CT-systems in m
	double gantryRot[3];            ///< gantry rotation around x-, y- and z-axis for CT-systems in rad

	std::map<int, bool> checkedOrgans;
	bool organStatusChnaged = false;

public:
	cInputData();
	cInputData(const cInputData &input);
	virtual ~cInputData();
	void copy(const cInputData &input);
	void setGeneralInput(cInputData input);
	void setRADInput(cInputData input);
	void setCTInput(cInputData input);
	void setOrganInput(cInputData input);
	void saveInAFile(std::string path);
	void importInput(std::string path);
	void setAllOrgansChecked(std::vector<cOrgan> organs);
	void setOrganStatus(bool status);
};

#endif // #ifndef HEADER_INPUT_DATA_INCLUDED
