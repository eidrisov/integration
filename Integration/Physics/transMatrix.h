//=============================================================================
// Filename: transMatrix.h
// Author: Robert Hess
// Version: 1.00
// Date: June 27th 2020
// Description: Class to shift and rotate points in space.
//=============================================================================

#pragma once
#ifndef HEADER_TRANS_MATRIX_INCLUDED
#define HEADER_TRANS_MATRIX_INCLUDED

/// Transformation matrix to be applied on points in space. Shift- and rotation operations may be stored in this matrix and then applied in one step severeal times.
class cTransMatrix {
private:
	double m[4][4]; ///< transformation matrix
public:
	cTransMatrix();
	virtual ~cTransMatrix();
private:
	void multiply(double mat[4][4]);
public:
	void reset();
	void shift(double deltaX, double deltaY, double deltaZ);
	void rotateAroundX(double angle);
	void rotateAroundY(double angle);
	void rotateAroundZ(double angle);
	void apply(double p[3]);
};

#endif // #ifndef HEADER_TRANS_MATRIX_INCLUDED
