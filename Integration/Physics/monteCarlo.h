//=============================================================================
// Filename: monteCarlo.h
// Author: Robert Hess
// Version: 0.3
// Date: June 27th 2020
// Description: Core class for the Monte Carlo simulation of effective dose
// in human bodies.
//=============================================================================

#pragma once
#ifndef HEADER_MONTE_CARLO_INCLUDED
#define HEADER_MONTE_CARLO_INCLUDED

#include "element.h"
#include "material.h"
#include "particle.h"
#include "random.h"
#include "randMC.h"
#include "inputData.h"
#include "phantom.h"
#include "transMatrix.h"
#include "results.h"
#include <stack>

/// \brief Core class for Monte Carlo method to simulate x-rays in phantoms
class cMonteCarlo
{
private:
    int progress;   ///< state of simulation starting with nothing (0) to post-processing done (5)
                    // 0: nothing, 1: input data set, 2: simulation prepared,
                    // 3: simulation running, 4: simulation done, 5: prost-processing done
	cInputData input;   ///< input settings for simulation
	cResults results;   ///< simulation results

	cPhantom phantom;   ///< current phantom

	// local attributes for simulation
	cTransMatrix moveSpot;  ///< transformation matrix to move focal spot
	cTransMatrix changeDirection;   ///< transformation matrix to rotate x-ray tube
	double threshold;		///< threshold energy in keV, i.e. energies below are treated as absorbed
	uint64_t remaining;		///< remaining number of photons to be simulated
	cElement ele;			///< atomic data of all elements
	cRandKiss kiss;			///< core random generator
	cRandMC randMC;			///< all random number generators for Monte Carlo method
	std::stack<cParticle> stack;	///< stack of particles to be simulated
public:
    /// Constructor
	cMonteCarlo();
	/// Destructor
	~cMonteCarlo();
	/// Setter for input data
	void setInputData(const cInputData &newInput);
	/// Preparation of Monte Carlo simulation
	void prepare();
	/// Preparation of Monte Carlo simulation
	void prepare(const cInputData &newInput);
	/// Simulation of *n* incident particles
	bool simulate(uint64_t n);
	/// Calculation of effective dose *E*
	void postProcessing();
	/// Returns the number of remaining incident photons to be simulated
	uint64_t getRemainingPhotons();
	/// Returns the simulation results
	const cResults &getResults();
	/// Retruns the loaded phantom
	const cPhantom &getPhantom();
private:
    /// Evaluates the mass of all organs and effective dose tissues
	void prepareMass();
	/// Handles a particle (photon, ion, interaction etc.)
	void handleParticle();
	/// Simulates a photon until next interaction
	void handlePhoton();
	/// Simulates the interaction of a photon
	void handleInteraction();
	/// Handles the photo electric effect
	void handlePhotoEffect();
	/// Handles an ion (Characteristic and Auger effect)
	void handleIon();
	/// Handles Compton effect (incoherent scattering)
	void handleCompton();
	/// Handles Rayleigh effect (coherent scattering)
	void handleRayleigh();
	/// Handles the absorption of a particle
	void handleAbsorption();
	/// Moves a photon to the phantom
	bool moveToPhantom(cParticle &part);
	/// Evaluated the distance to the next voxel
	bool distanceToNextVoxel(const cParticle &part, double &dist, int newVoxelIndex[3]);
	/// Moves the particle to the next voxel
	bool moveToNextVoxel(cParticle &part);

	/// A testing class
	friend class cTestMonteCarlo;
};

#endif // #ifndef HEADER_MONTE_CARLO_INCLUDED
