//=============================================================================
// Filename: results.cpp
// Author: Robert Hess
// Version: 0.3
// Date: June 22nd 2020
// Description: Class to hold all simulation results.
//=============================================================================

#include "results.h"
#include <cstring>

using namespace std;

//=============================================================================
/// Default constructor. Resets all attributes.
cResults::cResults()
//=============================================================================
{
	reset();
}


//=============================================================================
/// Copy constructor. Copies all attributes of the given set of results.
cResults::cResults(
    const cResults &res)    ///< [in] results to be copied
//=============================================================================
{
	copy(res);
}


//=============================================================================
/// Destructor. Does nothing.
cResults::~cResults()
//=============================================================================
{
	// TODO Auto-generated destructor stub
}


//=============================================================================
/// Clears the whole class, i.e. frees allocated memory and resets all attributes.
void cResults::clear()
//=============================================================================
{
	voxel.setSize(0, 0, 0);
	voxelVar.setSize(0, 0, 0);
	energyO.clear();
	energyOVar.clear();
	massO.clear();
	energyT.clear();
	energyTVar.clear();
	massT.clear();
	reset();
}


//=============================================================================
/// Resets all attributes but keeps allocted memeory.
void cResults::reset()
//=============================================================================
{
	photonsPerCharge = 1;
	totalEnergy = 0.0;
	neverInEnergy = 0.0;
	totalAbsorption = 0.0;
	escapedEnergy = 0.0;
	voxel = 0.0;
	voxelVar = 0.0;
	for(int i=0; i<(int)energyO.size(); i++) energyO[i] = 0;
	for(int i=0; i<(int)energyOVar.size(); i++) energyOVar[i] = 0;
	for(int i=0; i<(int)massO.size(); i++) massO[i] = 0;
	for(int i=0; i<(int)energyT.size(); i++) energyT[i] = 0;
	for(int i=0; i<(int)energyTVar.size(); i++) energyTVar[i] = 0;
	for(int i=0; i<(int)massT.size(); i++) massT[i] = 0;
	E = 0;
	EStdev = 0;
}


//=============================================================================
/// Prepares the attributes, i.e. allocates the required memory and resets all attributes.
void cResults::prepare(
    const cPhantom &phan)   ///< [in] phantom to get the required vector sizes
//=============================================================================
{
	voxel.setSize(phan.voxel.nx, phan.voxel.ny, phan.voxel.nz);
	voxelVar.setSize(phan.voxel.nx, phan.voxel.ny, phan.voxel.nz);
	energyO.resize(phan.organ.size(), 0.0);
	energyOVar.resize(phan.organ.size(), 0.0);
	massO.resize(phan.organ.size(), 0.0);
	energyT.resize(phan.effDoseTissue.size(), 0.0);
	energyTVar.resize(phan.effDoseTissue.size(), 0.0);
	massT.resize(phan.effDoseTissue.size(), 0.0);
	reset();
}


//=============================================================================
/// Assignment operator to copy all given results.
cResults &cResults::operator=(
    const cResults &res)    ///< [in] results to be copied
//=============================================================================
{
	copy(res);

	return *this;
}


//=============================================================================
/// Copies all given results.
void cResults::copy(const cResults &res)    ///< [in] results to be copied
//=============================================================================
{
	photonsPerCharge = res.photonsPerCharge;
	totalEnergy = res.totalEnergy;
	neverInEnergy = res.neverInEnergy;
	totalAbsorption = res.totalAbsorption;
	escapedEnergy = res.escapedEnergy;
	voxel = res.voxel;
	voxelVar = res.voxelVar;
	energyO = res.energyO;
	energyOVar = res.energyOVar;
	massO = res.massO;
	energyT = res.energyT;
	energyTVar = res.energyTVar;
	massT = res.massT;
	E = res.E;
	EStdev = res.EStdev;
}
