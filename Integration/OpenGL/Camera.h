#ifndef CAMERA_H_INCLUDED
#define CAMERA_H_INCLUDED

#include <glm/gtc/constants.hpp>
#include <glm/glm.hpp>

class Camera
{
public:
	glm::mat4 getViewMatrix() const;
	glm::highp_dmat4 getProjectionMatrix(int windowWidth, int windowHeight) const;
	virtual void rotate(float yaw, float pitch) {};
	virtual void setPosition(const glm::vec3& position) {};
	virtual void move(const glm::vec3& offsetPos) {};

	const glm::vec3& getLook() const;
	const glm::vec3& getRight() const;
	const glm::vec3& getUp() const;

	float getFOV() const { return mFOV; }
	void setFOV(float fov) { mFOV = fov; }

protected:
	Camera();

	virtual void updateCameraVectors() {};

	glm::vec3 mPosition;
	glm::vec3 mTargetPos;
	glm::vec3 mUp;
	glm::vec3 mLook;
	glm::vec3 mRight;
	glm::vec3 WORLD_UP;

	float mYaw;
	float mPitch;
	float mFOV;
};

class FPSCamera : public Camera
{
public:
	FPSCamera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), float yaw = glm::pi<float>(), float pitch = 0.0f);

	virtual void setPosition(const glm::vec3& position);
	glm::vec3 getPosition();
	glm::vec3 getCameraDirection();
	virtual void rotate(float yaw, float pitch);
	virtual void move(const glm::vec3& offsetPos);

private:
	void updateCameraVectors();
};

#endif // CAMERA_H_INCLUDED

