#ifndef SHADER_PROGRAM_H
#define SHADER_PROGRAM_H

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include <map>
#include <wx/wx.h>
#include <wx/glcanvas.h>

using std::string;


class ShaderProgram
{
public:
	ShaderProgram();
	~ShaderProgram();

	enum ShaderType
	{
		VERTEX,
		FRAGMENT,
		PROGRAM
	};

	bool loadShaders(const char* vsFilename, const char* fsFilename);
	bool loadFragmentShader(const char* fsFilename);
	void use();
	void deleteProgram();

	void setUniform(const GLchar* name, const glm::vec2& v);
	void setUniform(const GLchar* name, const glm::vec3& v);
	void setUniform(const GLchar* name, const glm::vec4& v);
	void setUniform(const GLchar* name, const glm::mat4& v);

	GLint getUniformLocation(const GLchar* name);

private:

	string fileToString(const string& filename);
	void checkCompileErrors(GLuint shader, ShaderType type);

	GLuint mHandle;
	std::map<string, GLint> mUniformLocations;
};

#endif