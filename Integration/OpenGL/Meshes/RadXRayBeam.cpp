#include "RadXRayBeam.h"

RadXRayBeam::RadXRayBeam()
{

}

RadXRayBeam:: ~RadXRayBeam()
{
	glDeleteBuffers(1, &vbo);
}

void RadXRayBeam::setBeamStartPosition(glm::vec3 beamStartPosition)
{
	m_beamStartPosition = beamStartPosition;
}

void RadXRayBeam::setBeamEndPosition(glm::vec3 beamEndPosition)
{
	m_beamEndPosition = beamEndPosition;
}


void RadXRayBeam::draw()
{
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), NULL);
	glEnableVertexAttribArray(0);

	glDrawArrays(GL_TRIANGLES, 0, vertices.size());
}

void RadXRayBeam::setCollimatorOpening(glm::vec3 collimatorOpening)
{
	m_collimatorOpening = collimatorOpening;
}

void RadXRayBeam::generate()
{
	if (vertices.size() > 0)
	{
		vertices.clear();
		glDeleteBuffers(1, &vbo);
	}

	GLfloat pyramidHeight1 = glm::length(m_beamEndPosition - m_beamStartPosition);
	GLfloat pyramidHeight2 = m_collimatorOpening.z;

	GLfloat pyramiBaseWidth = m_collimatorOpening.x * pyramidHeight1 / pyramidHeight2;
	GLfloat pyramiBaseHeight = m_collimatorOpening.y * pyramidHeight1 / pyramidHeight2;
	glm::vec3 pyramidBasePoint_1 = glm::vec3(-pyramiBaseHeight / 2, pyramidHeight1,  pyramiBaseWidth / 2);
	glm::vec3 pyramidBasePoint_2 = glm::vec3(-pyramiBaseHeight / 2, pyramidHeight1, -pyramiBaseWidth / 2);
	glm::vec3 pyramidBasePoint_3 = glm::vec3( pyramiBaseHeight / 2, pyramidHeight1, -pyramiBaseWidth / 2);
	glm::vec3 pyramidBasePoint_4 = glm::vec3( pyramiBaseHeight / 2, pyramidHeight1,  pyramiBaseWidth / 2);

	vertices.push_back(pyramidBasePoint_1); // base of a pyramid
	vertices.push_back(pyramidBasePoint_2);
	vertices.push_back(pyramidBasePoint_4);
	vertices.push_back(pyramidBasePoint_4);
	vertices.push_back(pyramidBasePoint_2);
	vertices.push_back(pyramidBasePoint_3);
	vertices.push_back(pyramidBasePoint_1); // front face
	vertices.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
	vertices.push_back(pyramidBasePoint_4);
	vertices.push_back(pyramidBasePoint_2); // left face
	vertices.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
	vertices.push_back(pyramidBasePoint_1);
	vertices.push_back(pyramidBasePoint_3); // back face
	vertices.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
	vertices.push_back(pyramidBasePoint_2); 
	vertices.push_back(pyramidBasePoint_4); // right face
	vertices.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
	vertices.push_back(pyramidBasePoint_3); 

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
}