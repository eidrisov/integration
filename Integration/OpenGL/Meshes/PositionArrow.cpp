#include "PositionArrow.h"

PositionArrow::PositionArrow()
{
	glEnable(GL_LINE_SMOOTH);
	glLineWidth(5.0f);
	generateXaxis();
	generateYaxis();
	generateZaxis();
}

PositionArrow::~PositionArrow()
{
	glDisable(GL_LINE_SMOOTH);
	glDeleteBuffers(1, &vboZ);
	glDeleteBuffers(1, &vboY);
	glDeleteBuffers(1, &vboX);
}

void PositionArrow::generateXaxis()
{
	// x axis
	GLfloat x_axis_vertices[] =
	{
		0.0f, 0.0f, 0.0f,
		1.0f, 0.0f,  0.0f
	};

	glGenBuffers(1, &vboX);
	glBindBuffer(GL_ARRAY_BUFFER, vboX);
	glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(GLfloat), x_axis_vertices, GL_STATIC_DRAW);
}

void PositionArrow::generateYaxis()
{
	//y axis
	GLfloat y_axis_vertices[] =
	{
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f
	};

	glGenBuffers(1, &vboY);
	glBindBuffer(GL_ARRAY_BUFFER, vboY);
	glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(GLfloat), y_axis_vertices, GL_STATIC_DRAW);
}

void PositionArrow::generateZaxis()
{
	// z axis
	GLfloat z_axis_vertices[] =
	{
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f,  0.0f
	};

	glGenBuffers(1, &vboZ);
	glBindBuffer(GL_ARRAY_BUFFER, vboZ);
	glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(GLfloat), z_axis_vertices, GL_STATIC_DRAW);
}

void PositionArrow::drawXaxis()
{
	glBindBuffer(GL_ARRAY_BUFFER, vboX);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), NULL);
	glEnableVertexAttribArray(0);
	glDrawArrays(GL_LINES, 0, 2);
}

void PositionArrow::drawYaxis()
{
	glBindBuffer(GL_ARRAY_BUFFER, vboY);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), NULL);
	glEnableVertexAttribArray(0);
	glDrawArrays(GL_LINES, 0, 2);
}

void PositionArrow::drawZaxis()
{
	glBindBuffer(GL_ARRAY_BUFFER, vboZ);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), NULL);
	glEnableVertexAttribArray(0);
	glDrawArrays(GL_LINES, 0, 2);
}