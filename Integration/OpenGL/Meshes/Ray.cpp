#include "Ray.h"

Ray::Ray()
{
	m_point = glm::vec3(0.0, 0.0, 0.0);
	m_offset = glm::vec3(0.0, 0.0, 0.0);
	m_planeNormal = glm::vec3(0.0, 0.0, 0.0);
	m_intersectionPoint = glm::vec3(0.0, 0.0, 0.0);
	m_cameraPosition = glm::vec3(0.0, 0.0, 0.0);
}

Ray::Ray(GLfloat x, GLfloat y, glm::vec3 offset, glm::vec3 planeNormal, glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition)
{
	m_point = glm::vec3(x, y, -1.0);
	m_offset = offset;
	m_planeNormal = planeNormal;
	m_intersectionPoint = glm::vec3(0.0, 0.0, 0.0);
	m_view = view;
	m_projection = projection;
	m_cameraPosition = cameraPosition;
}

Ray::~Ray()
{

}

void Ray::update(glm::vec2 point, glm::vec3 offset, glm::vec3 planeNormal, glm::mat4 view, glm::vec3 cameraPosition)
{
	m_point = glm::vec3(point, -1.0);
	m_offset = offset;
	m_planeNormal = planeNormal;
	m_view = view;
	m_cameraPosition = cameraPosition;

}

void Ray::setPoint(glm::vec2 point)
{
	m_point = glm::vec3(point, -1.0);
}

void Ray::setOffset(glm::vec3 offset)
{
	m_offset = offset;
}

void Ray::setPlaneNormal(glm::vec3 planeNormal)
{
	m_planeNormal = planeNormal;
}

void Ray::setCameraPosition(glm::vec3 cameraPosition)
{
	m_cameraPosition = cameraPosition;
}

void Ray::setViewMatrix(glm::mat4 view)
{
	m_view = view;
}

void Ray::setProjectionMatrix(glm::mat4 projection)
{
	m_projection = projection;
}

glm::vec3 Ray::getRayDirection()
{
	return m_rayDirection;
}

glm::vec3 Ray::getIntersectionPoint()
{
	return m_intersectionPoint;
}

glm::vec3 Ray::getOffset()
{
	return m_offset;
}

void Ray::create()
{
	glm::vec4 ray_nds = glm::vec4(m_point, 1.0);
	glm::vec4 ray_clip = glm::vec4(ray_nds.x, ray_nds.y, -1.0, 1.0);
	glm::vec4 ray_eye = glm::inverse(m_projection) * ray_clip;
	ray_eye = glm::vec4(ray_eye.x, ray_eye.y, -1.0, 0.0);
	glm::vec3 ray_wor = glm::vec3(glm::inverse(m_view) * ray_eye);
	m_rayDirection = glm::normalize(ray_wor);
}

bool Ray::intersects()
{
	GLfloat denominator = glm::dot(m_planeNormal, m_rayDirection);
	if (fabs(denominator) > 0.0001f) 
	{
		GLfloat t = glm::dot((m_offset - m_cameraPosition), m_planeNormal) / denominator;
		if (t >= 0)
		{
			m_intersectionPoint = m_cameraPosition + t * m_rayDirection;
			return true; 
		}
	}
	return false;
}