#ifndef PHANTOMMESH_H_INCLUDED
#define PHANTOMMESH_H_INCLUDED

#include "GL/glew.h"
#include <wx/wx.h>
#include <vector>
#include <string>
#include "glm/glm.hpp"
#include <glm/gtx/intersect.hpp>
#include "../../Physics/phantom.h"
#include "../../Physics/inputData.h"

struct Vertex
{
	glm::vec3 position;
	glm::vec3 normal;
};

class PhantomMesh
{
public:
	PhantomMesh(cInputData inputSettings);
	~PhantomMesh();

	void loadVertices();
	bool loadOBJ();
	void draw();
	glm::vec3 getNormal(int index);
	glm::vec3 getPosition(int index);
	glm::vec3 getCenterCoordinates();
	glm::vec3 getLeftBackBottomCornerCoor();
	void minMax(glm::vec3 coor);
	void addVertex(glm::vec3 normal, glm::vec3 position);
	void addLeftFace();
	void addRightFace();
	void addFrontFace();
	void addBackFace();
	void addTopFace();
	void addBottomFace();

private:
	void initBuffers();
	bool loaded;
	cInputData m_inputSettings;
	std::string m_filename;
	std::vector<Vertex> vertices;
	GLuint vbo = 0, vao = 0;
	GLfloat xMax = 0, xMin = 0, yMax = 0, yMin = 0, zMax = 0, zMin = 0;
	cPhantom* phantom;
	unsigned char emptyVoxel = 255;
	GLint xSize = 0, ySize = 0, zSize = 0;
	GLfloat voxelSizeX = 1, voxelSizeY = 1, voxelSizeZ = 1;
	glm::vec3 vertex_pos_0, vertex_pos_1, vertex_pos_2, vertex_pos_3, vertex_pos_4, vertex_pos_5, vertex_pos_6, vertex_pos_7;
	glm::vec3 m_collimatorOpening;
};

#endif // PHANTOMMESH_H_INCLUDED
