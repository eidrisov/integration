#include "PhantomMesh.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <math.h>

PhantomMesh::PhantomMesh(cInputData inputSettings)
{
	m_inputSettings = inputSettings;
	m_filename = inputSettings.phantomFilename;
	loaded = false;
	phantom = new cPhantom();
}

PhantomMesh::~PhantomMesh()
{
	delete phantom;
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
}

void PhantomMesh::addVertex(glm::vec3 normal, glm::vec3 position)
{
	Vertex vertex;
	vertex.normal = normal;
	vertex.position = position;
	vertices.push_back(vertex);
	minMax(vertex.position);
}

void PhantomMesh::addLeftFace()
{
	glm::vec3 normal = glm::vec3(-1.0, 0.0, 0.0);
	addVertex(normal, glm::vec3(vertex_pos_1.x, vertex_pos_1.y, vertex_pos_1.z));
	addVertex(normal, glm::vec3(vertex_pos_0.x, vertex_pos_0.y, vertex_pos_0.z));
	addVertex(normal, glm::vec3(vertex_pos_2.x, vertex_pos_2.y, vertex_pos_2.z));
	addVertex(normal, glm::vec3(vertex_pos_1.x, vertex_pos_1.y, vertex_pos_1.z));
	addVertex(normal, glm::vec3(vertex_pos_2.x, vertex_pos_2.y, vertex_pos_2.z));
	addVertex(normal, glm::vec3(vertex_pos_3.x, vertex_pos_3.y, vertex_pos_3.z));
}

void PhantomMesh::addRightFace()
{
	glm::vec3 normal = glm::vec3(1.0, 0.0, 0.0);
	addVertex(normal, glm::vec3(vertex_pos_4.x, vertex_pos_4.y, vertex_pos_4.z));
	addVertex(normal, glm::vec3(vertex_pos_5.x, vertex_pos_5.y, vertex_pos_5.z));
	addVertex(normal, glm::vec3(vertex_pos_7.x, vertex_pos_7.y, vertex_pos_7.z));
	addVertex(normal, glm::vec3(vertex_pos_4.x, vertex_pos_4.y, vertex_pos_4.z));
	addVertex(normal, glm::vec3(vertex_pos_7.x, vertex_pos_7.y, vertex_pos_7.z));
	addVertex(normal, glm::vec3(vertex_pos_6.x, vertex_pos_6.y, vertex_pos_6.z));
}

void PhantomMesh::addFrontFace()
{
	glm::vec3 normal = glm::vec3(0.0, 0.0, -1.0);
	addVertex(normal, glm::vec3(vertex_pos_0.x, vertex_pos_0.y, vertex_pos_0.z));
	addVertex(normal, glm::vec3(vertex_pos_4.x, vertex_pos_4.y, vertex_pos_4.z));
	addVertex(normal, glm::vec3(vertex_pos_6.x, vertex_pos_6.y, vertex_pos_6.z));
	addVertex(normal, glm::vec3(vertex_pos_0.x, vertex_pos_0.y, vertex_pos_0.z));
	addVertex(normal, glm::vec3(vertex_pos_6.x, vertex_pos_6.y, vertex_pos_6.z));
	addVertex(normal, glm::vec3(vertex_pos_2.x, vertex_pos_2.y, vertex_pos_2.z));
}

void PhantomMesh::addBackFace()
{
	glm::vec3 normal = glm::vec3(0.0, 0.0, 1.0);
	addVertex(normal, glm::vec3(vertex_pos_5.x, vertex_pos_5.y, vertex_pos_5.z));
	addVertex(normal, glm::vec3(vertex_pos_1.x, vertex_pos_1.y, vertex_pos_1.z));
	addVertex(normal, glm::vec3(vertex_pos_3.x, vertex_pos_3.y, vertex_pos_3.z));
	addVertex(normal, glm::vec3(vertex_pos_5.x, vertex_pos_5.y, vertex_pos_5.z));
	addVertex(normal, glm::vec3(vertex_pos_3.x, vertex_pos_3.y, vertex_pos_3.z));
	addVertex(normal, glm::vec3(vertex_pos_7.x, vertex_pos_7.y, vertex_pos_7.z));
}

void PhantomMesh::addTopFace()
{
	glm::vec3 normal = glm::vec3(0.0, 1.0, 0.0);
	addVertex(normal, glm::vec3(vertex_pos_2.x, vertex_pos_2.y, vertex_pos_2.z));
	addVertex(normal, glm::vec3(vertex_pos_6.x, vertex_pos_6.y, vertex_pos_6.z));
	addVertex(normal, glm::vec3(vertex_pos_7.x, vertex_pos_7.y, vertex_pos_7.z));
	addVertex(normal, glm::vec3(vertex_pos_2.x, vertex_pos_2.y, vertex_pos_2.z));
	addVertex(normal, glm::vec3(vertex_pos_7.x, vertex_pos_7.y, vertex_pos_7.z));
	addVertex(normal, glm::vec3(vertex_pos_3.x, vertex_pos_3.y, vertex_pos_3.z));
}

void PhantomMesh::addBottomFace()
{
	glm::vec3 normal = glm::vec3(0.0, -1.0, 0.0);
	addVertex(normal, glm::vec3(vertex_pos_1.x, vertex_pos_1.y, vertex_pos_1.z));
	addVertex(normal, glm::vec3(vertex_pos_5.x, vertex_pos_5.y, vertex_pos_5.z));
	addVertex(normal, glm::vec3(vertex_pos_4.x, vertex_pos_4.y, vertex_pos_4.z));
	addVertex(normal, glm::vec3(vertex_pos_1.x, vertex_pos_1.y, vertex_pos_1.z));
	addVertex(normal, glm::vec3(vertex_pos_4.x, vertex_pos_4.y, vertex_pos_4.z));
	addVertex(normal, glm::vec3(vertex_pos_0.x, vertex_pos_0.y, vertex_pos_0.z));
}

void PhantomMesh::loadVertices()
{
	std::map<int, bool> organs = m_inputSettings.checkedOrgans;

	for (GLint z = 0; z < zSize; z++)
	{
		for (GLint y = 0; y < ySize; y++)
		{
			for (GLint x = 0; x < xSize; x++)
			{
				if (phantom->voxel[x][y][z] == emptyVoxel)
				{
					continue;
				}
				else
				{
					vertex_pos_0 = glm::vec3(((GLfloat)x - 1) * voxelSizeX, ((GLfloat)y - 1) * voxelSizeY, ((GLfloat)z - 1) * voxelSizeZ);
					vertex_pos_1 = glm::vec3(((GLfloat)x - 1) * voxelSizeX, ((GLfloat)y - 1) * voxelSizeY, ((GLfloat)z + 1) * voxelSizeZ);
					vertex_pos_2 = glm::vec3(((GLfloat)x - 1) * voxelSizeX, ((GLfloat)y + 1) * voxelSizeY, ((GLfloat)z - 1) * voxelSizeZ);
					vertex_pos_3 = glm::vec3(((GLfloat)x - 1) * voxelSizeX, ((GLfloat)y + 1) * voxelSizeY, ((GLfloat)z + 1) * voxelSizeZ);
					vertex_pos_4 = glm::vec3(((GLfloat)x + 1) * voxelSizeX, ((GLfloat)y - 1) * voxelSizeY, ((GLfloat)z - 1) * voxelSizeZ);
					vertex_pos_5 = glm::vec3(((GLfloat)x + 1) * voxelSizeX, ((GLfloat)y - 1) * voxelSizeY, ((GLfloat)z + 1) * voxelSizeZ);
					vertex_pos_6 = glm::vec3(((GLfloat)x + 1) * voxelSizeX, ((GLfloat)y + 1) * voxelSizeY, ((GLfloat)z - 1) * voxelSizeZ);
					vertex_pos_7 = glm::vec3(((GLfloat)x + 1) * voxelSizeX, ((GLfloat)y + 1) * voxelSizeY, ((GLfloat)z + 1) * voxelSizeZ);

					auto it = organs.find(phantom->voxel[x][y][z]);
					if (it->second)
					{
						if (x != 0 && x != (xSize - 1))
						{
							auto leftNeighbour = organs.find(phantom->voxel[x - 1][y][z]);
							auto rightNeighbour = leftNeighbour;
							if (phantom->voxel[x + 1][y][z] != phantom->voxel[x][y][z]) {
								rightNeighbour = organs.find(phantom->voxel[x + 1][y][z]);
							}
							if (!leftNeighbour->second || (phantom->voxel[x - 1][y][z] == emptyVoxel)) // left face
							{
								addLeftFace();
							}
							if (!rightNeighbour->second || (phantom->voxel[x + 1][y][z] == emptyVoxel)) //right face
							{
								addRightFace();
							}
						}
						else
						{
							if (x == 0) // left face
							{
								addLeftFace();
							}
							if (x == (xSize - 1)) // right face
							{
								addRightFace();
							}
						}

						if (y != 0 && y != (ySize - 1))
						{
							auto bottomNeighbour = organs.find(phantom->voxel[x][y - 1][z]);
							auto topNeighbour = bottomNeighbour;
							if (phantom->voxel[x][y + 1][z] != phantom->voxel[x][y][z]) {
								topNeighbour = organs.find(phantom->voxel[x][y + 1][z]);
							}
							if (!bottomNeighbour->second || (phantom->voxel[x][y - 1][z] == emptyVoxel)) // bottom face
							{
								addBottomFace();
							}
							if (!topNeighbour->second || (phantom->voxel[x][y + 1][z] == emptyVoxel)) // top face
							{
								addTopFace();
							}
						}
						else
						{
							if (y == 0) // bottom face
							{
								addBottomFace();
							}

							if (y == (ySize - 1)) // top face
							{
								addTopFace();
							}
						}

						if (z != 0 && z != (zSize - 1))
						{
							auto frontNeighbour = organs.find(phantom->voxel[x][y][z - 1]);
							auto backNeighbour = frontNeighbour;
							if (phantom->voxel[x][y][z + 1] != phantom->voxel[x][y][z]) {
								backNeighbour = organs.find(phantom->voxel[x][y][z + 1]);
							}
							if (!frontNeighbour->second || (phantom->voxel[x][y][z - 1] == emptyVoxel)) // front face
							{
								addFrontFace();
							}

							if (!backNeighbour->second || (phantom->voxel[x][y][z + 1] == emptyVoxel)) // back face
							{
								addBackFace();
							}
						}
						else
						{
							if (z == 0) // front face
							{
								addFrontFace();
							}

							if (z == (zSize - 1)) // back face
							{
								addBackFace();
							}
						}
					}
				}
			}
		}
	}	
}

bool PhantomMesh::loadOBJ()
{
	wxMessageOutputDebug().Printf("%s", m_filename);
	phantom->readPhantomBin(m_filename);
	xSize = phantom->voxel.nx;
	ySize = phantom->voxel.ny;
	zSize = phantom->voxel.nz;
	voxelSizeX = phantom->phantomSize[0] / xSize;
	voxelSizeY = phantom->phantomSize[1] / ySize;
	voxelSizeZ = phantom->phantomSize[2] / zSize;

	loadVertices();
	initBuffers();
	return (loaded = true);
}

glm::vec3 PhantomMesh::getNormal(int index)
{
    return vertices.at(index).normal;
}

glm::vec3 PhantomMesh::getPosition(int index)
{
    return vertices.at(index).position;
}

void PhantomMesh::minMax(glm::vec3 coor)
{
    if (coor.x > xMax)
    {
        xMax = coor.x;
    }
    if (coor.x < xMin)
    {
        xMin = coor.x;
    }
    if (coor.y > yMax)
    {
        yMax = coor.y;
    }
    if (coor.y < yMin)
    {
        yMin = coor.y;
    }
    if (coor.z > zMax)
    {
        zMax = coor.z;
    }
    if (coor.z < zMin)
    {
        xMin = coor.z;
    }
}

glm::vec3 PhantomMesh::getCenterCoordinates()
{
    GLfloat dx = xMax - xMin;
    GLfloat dy = yMax - yMin;
    GLfloat dz = zMax - zMin;
    return glm::vec3(xMax - dx / 2.0, yMax - dy / 2.0, zMax - dz / 2.0);
    //return glm::vec3(0.265, 0.05571, 0.96894); // female 
    // return glm::vec3(0.271399, 0.06784975, 0.7622043); //  male
}

glm::vec3 PhantomMesh::getLeftBackBottomCornerCoor()
{
	return glm::vec3(-1 * voxelSizeX, -1 * voxelSizeY, -1 * voxelSizeZ);
}

void PhantomMesh::draw()
{
	if (!loaded) return;
	
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, vertices.size());
	glBindVertexArray(0);
}

void PhantomMesh::initBuffers()
{
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);
	glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

	glBindVertexArray(0);
}