#include "RotationArrow.h"

#define NUMBER_OF_VERTICES 32

RotationArrow::RotationArrow()
{
	initArcs();
	generateXaxis();
	generateYaxis();
	generateZaxis();
}

RotationArrow::~RotationArrow()
{
	glDeleteBuffers(1, &vboZ);
	glDeleteBuffers(1, &vboY);
	glDeleteBuffers(1, &vboX);
}

void RotationArrow::initArcs()
{
	GLfloat radius_1 = 0.5;
	GLfloat radius_2 = 0.4;
	for (GLint i = 0; i < (NUMBER_OF_VERTICES + 1); i++) {
		GLfloat angle = M_PI * i / 2 / NUMBER_OF_VERTICES;
		x_axis_vertices.push_back(0.0);    //X coordinate
		x_axis_vertices.push_back(cos(angle) * radius_2);    //Y coordinate
		x_axis_vertices.push_back(sin(angle) * radius_2);                //Z coordinate
		x_axis_vertices.push_back(0.0);    //X coordinate
		x_axis_vertices.push_back(cos(angle) * radius_1);    //Y coordinate
		x_axis_vertices.push_back(sin(angle) * radius_1);                //Z coordinate

		y_axis_vertices.push_back(sin(angle) * radius_2);    //X coordinate
		y_axis_vertices.push_back(0.0);    //Y coordinate
		y_axis_vertices.push_back(cos(angle) * radius_2);                //Z coordinate
		y_axis_vertices.push_back(sin(angle) * radius_1);    //X coordinate
		y_axis_vertices.push_back(0.0);    //Y coordinate
		y_axis_vertices.push_back(cos(angle) * radius_1);                //Z coordinate

		z_axis_vertices.push_back(cos(angle) * radius_2);    //X coordinate
		z_axis_vertices.push_back(sin(angle) * radius_2);    //Y coordinate
		z_axis_vertices.push_back(0.0);                //Z coordinate
		z_axis_vertices.push_back(cos(angle) * radius_1);    //X coordinate
		z_axis_vertices.push_back(sin(angle) * radius_1);    //Y coordinate
		z_axis_vertices.push_back(0.0);                //Z coordinate
	}
}

void RotationArrow::generateXaxis()
{
	glGenBuffers(1, &vboX);
	glBindBuffer(GL_ARRAY_BUFFER, vboX);
	glBufferData(GL_ARRAY_BUFFER, x_axis_vertices.size() * sizeof(GLfloat), &x_axis_vertices[0], GL_STATIC_DRAW);
}

void RotationArrow::generateYaxis()
{
	glGenBuffers(1, &vboY);
	glBindBuffer(GL_ARRAY_BUFFER, vboY);
	glBufferData(GL_ARRAY_BUFFER, y_axis_vertices.size() * sizeof(GLfloat), &y_axis_vertices[0], GL_STATIC_DRAW);
}

void RotationArrow::generateZaxis()
{
	glGenBuffers(1, &vboZ);
	glBindBuffer(GL_ARRAY_BUFFER, vboZ);
	glBufferData(GL_ARRAY_BUFFER, z_axis_vertices.size() * sizeof(GLfloat), &z_axis_vertices[0], GL_STATIC_DRAW);
}

void RotationArrow::drawXaxis()
{
	glBindBuffer(GL_ARRAY_BUFFER, vboX);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), NULL);
	glEnableVertexAttribArray(0);

	glDrawArrays(GL_QUAD_STRIP, 0, 2 * NUMBER_OF_VERTICES + 2);
}

void RotationArrow::drawYaxis()
{
	glBindBuffer(GL_ARRAY_BUFFER, vboY);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), NULL);
	glEnableVertexAttribArray(0);

	glDrawArrays(GL_QUAD_STRIP, 0, 2 * NUMBER_OF_VERTICES + 2);
}

void RotationArrow::drawZaxis()
{
	glBindBuffer(GL_ARRAY_BUFFER, vboZ);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), NULL);
	glEnableVertexAttribArray(0);

	glDrawArrays(GL_QUAD_STRIP, 0, 2 * NUMBER_OF_VERTICES + 2);
}