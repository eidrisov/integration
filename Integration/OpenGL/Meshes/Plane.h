#ifndef PLANE_H_INCLUDED
#define PLANE_H_INCLUDED

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <vector>

class Plane
{
public:
	enum Axis { XZ, XY, YZ };
	Plane();
	Plane(glm::vec3 position, Axis axesAlignment);
	~Plane();
	void draw(glm::vec3 cameraDirection);
	void setPosition(glm::vec3 position);
	void setAxesAlignment(Axis axesAlignment);

private:
	glm::vec3 m_position;
	Axis m_axesAlignment;
	GLfloat m_size;
	GLuint vbo;
	std::vector<glm::vec3> vertices;
};

#endif // PLANE_H_INCLUDED

