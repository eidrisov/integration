#ifndef GANTRYRING_H_INCLUDED
#define GANTRYRING_H_INCLUDED

#define _USE_MATH_DEFINES

#include <GL/glew.h>
#include <cmath>
#include "glm/glm.hpp"
#include <iostream>
#include <vector>

class GantryRing
{
private:
	GLuint vboTop, vboBottom, vboOuter, vboInner;
	GLfloat innerRadius, oterRadius, distance;
	std::vector<GLfloat> top_vertices, bottom_vertices, outer_vertices, inner_vertices;
public:
	GantryRing(double distance);
	~GantryRing();
	void generateTopBuffer();
	void generateBottomBuffer();
	void generateInnerBuffer();
	void generateOuterBuffer();
	void drawInner();
	void drawOuter();
	void drawTop();
	void drawBottom();
	void draw();
	void initTop();
	void initBottom();
	void initInner();
	void initOuter();
};

#endif // GANTRYRING_H_INCLUDED

