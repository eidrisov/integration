#include "CtXRayBeam.h"

CtXRayBeam::CtXRayBeam()
{

}

CtXRayBeam:: ~CtXRayBeam()
{
	glDeleteBuffers(1, &vbo);
}

void CtXRayBeam::draw()
{
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), NULL);
	glEnableVertexAttribArray(0);

	glDrawArrays(GL_TRIANGLES, 0, vertices.size());
}

void CtXRayBeam::setFanAttributes(glm::vec3 fanAttributes)
{
	m_fanAttributes = fanAttributes;
}

void CtXRayBeam::generate()
{
	if (vertices.size() > 0)
	{
		vertices.clear();
		glDeleteBuffers(1, &vbo);
	}

	GLfloat fanAngleWidth = m_fanAttributes.x;
	GLfloat fanAngleThickness = m_fanAttributes.y;
	GLfloat radiusCt = m_fanAttributes.z;

	GLfloat pyramiBaseWidth = radiusCt * glm::sin(glm::pi<float>() / 180 * (90 - fanAngleWidth / 2.0f)) * glm::sin(glm::pi<float>() / 180 * (fanAngleWidth / 2.0f));
	GLfloat pyramiBaseHeight = radiusCt * glm::sin(glm::pi<float>() / 180 * (90 - fanAngleThickness / 2.0f)) * glm::sin(glm::pi<float>() / 180 * (fanAngleThickness / 2.0f));
	glm::vec3 pyramidBasePoint_1 = glm::vec3(radiusCt, -pyramiBaseHeight / 2, pyramiBaseWidth / 2);
	glm::vec3 pyramidBasePoint_2 = glm::vec3(radiusCt,  pyramiBaseHeight / 2, pyramiBaseWidth / 2);
	glm::vec3 pyramidBasePoint_3 = glm::vec3(radiusCt,  pyramiBaseHeight / 2, -pyramiBaseWidth / 2);
	glm::vec3 pyramidBasePoint_4 = glm::vec3(radiusCt, -pyramiBaseHeight / 2, -pyramiBaseWidth / 2);

	vertices.push_back(pyramidBasePoint_1); // base of a pyramid
	vertices.push_back(pyramidBasePoint_2);
	vertices.push_back(pyramidBasePoint_4);
	vertices.push_back(pyramidBasePoint_4);
	vertices.push_back(pyramidBasePoint_2);
	vertices.push_back(pyramidBasePoint_3);
	vertices.push_back(pyramidBasePoint_1); // front face
	vertices.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
	vertices.push_back(pyramidBasePoint_4);
	vertices.push_back(pyramidBasePoint_2); // left face
	vertices.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
	vertices.push_back(pyramidBasePoint_1);
	vertices.push_back(pyramidBasePoint_3); // back face
	vertices.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
	vertices.push_back(pyramidBasePoint_2);
	vertices.push_back(pyramidBasePoint_4); // right face
	vertices.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
	vertices.push_back(pyramidBasePoint_3);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
}