#include "GantryRing.h"

GantryRing::GantryRing(double distance)
{
	this->distance = distance;
	innerRadius = distance;
	oterRadius = distance + 0.03f;
	initTop();
	initBottom();
	initInner();
	initOuter();
	generateTopBuffer();
	generateBottomBuffer();
	generateInnerBuffer();
	generateOuterBuffer();
}

GantryRing::~GantryRing()
{
	glDeleteBuffers(1, &vboTop);
	glDeleteBuffers(1, &vboBottom);
	glDeleteBuffers(1, &vboInner);
	glDeleteBuffers(1, &vboOuter);
}

void GantryRing::generateTopBuffer()
{
	glGenBuffers(1, &vboTop);
	glBindBuffer(GL_ARRAY_BUFFER, vboTop);
	glBufferData(GL_ARRAY_BUFFER, top_vertices.size() * sizeof(GLfloat), &top_vertices[0], GL_STATIC_DRAW);
}

void GantryRing::generateBottomBuffer()
{
	glGenBuffers(1, &vboBottom);
	glBindBuffer(GL_ARRAY_BUFFER, vboBottom);
	glBufferData(GL_ARRAY_BUFFER, bottom_vertices.size() * sizeof(GLfloat), &bottom_vertices[0], GL_STATIC_DRAW);
}

void GantryRing::generateInnerBuffer()
{
	glGenBuffers(1, &vboInner);
	glBindBuffer(GL_ARRAY_BUFFER, vboInner);
	glBufferData(GL_ARRAY_BUFFER, inner_vertices.size() * sizeof(GLfloat), &inner_vertices[0], GL_STATIC_DRAW);
}

void GantryRing::generateOuterBuffer()
{
	glGenBuffers(1, &vboOuter);
	glBindBuffer(GL_ARRAY_BUFFER, vboOuter);
	glBufferData(GL_ARRAY_BUFFER, outer_vertices.size() * sizeof(GLfloat), &outer_vertices[0], GL_STATIC_DRAW);
}

void GantryRing::drawTop()
{
	glBindBuffer(GL_ARRAY_BUFFER, vboTop);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), NULL);
	glEnableVertexAttribArray(0);

	glDrawArrays(GL_QUAD_STRIP, 0, 2 * 36 + 2);
}

void GantryRing::drawBottom()
{
	glBindBuffer(GL_ARRAY_BUFFER, vboBottom);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), NULL);
	glEnableVertexAttribArray(0);

	glDrawArrays(GL_QUAD_STRIP, 0, 2 * 36 + 2);
}

void GantryRing::drawInner()
{
	glBindBuffer(GL_ARRAY_BUFFER, vboInner);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), NULL);
	glEnableVertexAttribArray(0);

	glDrawArrays(GL_TRIANGLES, 0, inner_vertices.size() / 3);
}

void GantryRing::drawOuter()
{
	glBindBuffer(GL_ARRAY_BUFFER, vboOuter);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), NULL);
	glEnableVertexAttribArray(0);

	glDrawArrays(GL_QUAD_STRIP, 0, outer_vertices.size() / 3);
}

void GantryRing::draw()
{
	drawTop();
	drawBottom();
	drawInner();
	drawOuter();
}

void GantryRing::initTop()
{
	for (GLint i = 0; i < 36 + 1; i++) {
		GLfloat angle = 2 * M_PI * i / 36;

		top_vertices.push_back(cos(angle) * oterRadius + distance);    //X coordinate // top
		top_vertices.push_back(0.03);    //Y coordinate
		top_vertices.push_back(sin(angle) * oterRadius);

		top_vertices.push_back(cos(angle) * innerRadius + distance);    //X coordinate
		top_vertices.push_back(0.03);    //Y coordinate
		top_vertices.push_back(sin(angle) * innerRadius);
	}
}

void GantryRing::initBottom()
{
	for (GLint i = 0; i < 36 + 1; i++) {
		GLfloat angle = 2 * M_PI * i / 36;

		bottom_vertices.push_back(cos(angle) * oterRadius + distance);    //X coordinate // top
		bottom_vertices.push_back(-0.03);    //Y coordinate
		bottom_vertices.push_back(sin(angle) * oterRadius);

		bottom_vertices.push_back(cos(angle) * innerRadius + distance);    //X coordinate
		bottom_vertices.push_back(-0.03);    //Y coordinate
		bottom_vertices.push_back(sin(angle) * innerRadius);
	}
}

void GantryRing::initInner()
{
	for (GLint i = 0; i < 36; i++) {
		GLfloat angle_1 = 2 * M_PI * i / 36;
		GLfloat angle_2 = angle_1 + 2 * M_PI / 36;

		GLfloat x1 = cos(angle_1) * innerRadius;
		GLfloat y1 = sin(angle_1) * innerRadius;

		GLfloat x2 = cos(angle_2) * innerRadius;
		GLfloat y2 = sin(angle_2) * innerRadius;

		inner_vertices.push_back(x2 + distance);
		inner_vertices.push_back(0.03);
		inner_vertices.push_back(y2);

		inner_vertices.push_back(x1 + distance);    //X coordinate
		inner_vertices.push_back(0.03);
		inner_vertices.push_back(y1);

		inner_vertices.push_back(x1 + distance);
		inner_vertices.push_back(-0.03);
		inner_vertices.push_back(y1);

		inner_vertices.push_back(x2 + distance);
		inner_vertices.push_back(0.03);
		inner_vertices.push_back(y2);

		inner_vertices.push_back(x1 + distance);    //X coordinate
		inner_vertices.push_back(-0.03);
		inner_vertices.push_back(y1);

		inner_vertices.push_back(x2 + distance);
		inner_vertices.push_back(-0.03);
		inner_vertices.push_back(y2);
	}
}

void GantryRing::initOuter()
{
	for (GLint i = 0; i < 36; i++) {
		GLfloat angle_1 = 2 * M_PI * i / 36;
		GLfloat angle_2 = angle_1 + 2 * M_PI / 36;

		GLfloat x1 = cos(angle_1) * oterRadius;
		GLfloat y1 = sin(angle_1) * oterRadius;

		GLfloat x2 = cos(angle_2) * oterRadius;
		GLfloat y2 = sin(angle_2) * oterRadius;

		inner_vertices.push_back(x2 + distance);
		inner_vertices.push_back(0.03);
		inner_vertices.push_back(y2);

		inner_vertices.push_back(x1 + distance);    //X coordinate
		inner_vertices.push_back(0.03);
		inner_vertices.push_back(y1);

		inner_vertices.push_back(x1 + distance);
		inner_vertices.push_back(-0.03);
		inner_vertices.push_back(y1);

		inner_vertices.push_back(x2 + distance);
		inner_vertices.push_back(0.03);
		inner_vertices.push_back(y2);

		inner_vertices.push_back(x1 + distance);    //X coordinate
		inner_vertices.push_back(-0.03);
		inner_vertices.push_back(y1);

		inner_vertices.push_back(x2 + distance);
		inner_vertices.push_back(-0.03);
		inner_vertices.push_back(y2);
	}
}