#ifndef RAY_H_INCLUDED
#define RAY_H_INCLUDED

#include <GL/glew.h>
#include <gl/gl.h>
#include <glm/glm.hpp>

class Ray
{
private:
	glm::vec3 m_point;
	glm::vec3 m_offset;
	glm::vec3 m_planeNormal;
	glm::vec3 m_intersectionPoint;
	glm::vec3 m_cameraPosition;
	glm::vec3 m_rayDirection;
	glm::mat4 m_view;
	glm::mat4 m_projection;

public:
	Ray();
	~Ray();
	Ray(GLfloat x, GLfloat y, glm::vec3 offset, glm::vec3 planeNormal, glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition);
	void update(glm::vec2 point,glm::vec3 offset, glm::vec3 planeNormal, glm::mat4 view, glm::vec3 cameraPosition);
	void setPoint(glm::vec2 point);
	void setOffset(glm::vec3 offset);
	void setViewMatrix(glm::mat4 view);
	void setProjectionMatrix(glm::mat4 projection);
	void setPlaneNormal(glm::vec3 planeNormal);
	void setCameraPosition(glm::vec3 cameraPosition);
	bool intersects();
	glm::vec3 getRayDirection();
	glm::vec3 getIntersectionPoint();
	glm::vec3 getOffset();
	void create();
};

#endif // RAY_H_INCLUDED