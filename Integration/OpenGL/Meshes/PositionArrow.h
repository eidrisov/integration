#ifndef PositionArrow_H_INCLUDED
#define PositionArrow_H_INCLUDED

#include <GL/glew.h>
#include <iostream>

class PositionArrow
{
private:
	GLuint vboZ, vboY, vboX;
public:
	PositionArrow();
	~PositionArrow();
	void generateXaxis();
	void generateYaxis();
	void generateZaxis();
	void drawXaxis();
	void drawYaxis();
	void drawZaxis();
};

#endif // PositionArrow_H_INCLUDED
