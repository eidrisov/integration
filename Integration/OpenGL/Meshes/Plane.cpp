#include "Plane.h"

Plane::Plane()
{
	m_position = glm::vec3(0, 0, 0);
	m_axesAlignment = Axis::XZ;
	m_size = 5;
}

Plane::Plane(glm::vec3 position, Axis axesAlignment)
{
	m_position = position;
	m_axesAlignment = axesAlignment;
	m_size = 5;
}

Plane::~Plane()
{
	glDeleteBuffers(1, &vbo);
}

void Plane::setPosition(glm::vec3 position)
{
	m_position = position;
}

void Plane::setAxesAlignment(Axis axesAlignment)
{
	m_axesAlignment = axesAlignment;
}

void Plane::draw(glm::vec3 cameraDirection)
{
	vertices.clear();
	glDeleteBuffers(1, &vbo);

	switch (m_axesAlignment)
	{
	case XZ:
	{

		vertices.push_back(glm::vec3(m_position.x - m_size, m_position.y, m_position.z - m_size));
		vertices.push_back(glm::vec3(m_position.x - m_size, m_position.y, m_position.z + m_size));
		vertices.push_back(glm::vec3(m_position.x + m_size, m_position.y, m_position.z + m_size));
		vertices.push_back(glm::vec3(m_position.x + m_size, m_position.y, m_position.z - m_size));
		break;
	}
	case XY:
	{
		vertices.push_back(glm::vec3(m_position.x - m_size, m_position.y - m_size, m_position.z));
		vertices.push_back(glm::vec3(m_position.x - m_size, m_position.y + m_size, m_position.z));
		vertices.push_back(glm::vec3(m_position.x + m_size, m_position.y + m_size, m_position.z));
		vertices.push_back(glm::vec3(m_position.x + m_size, m_position.y - m_size, m_position.z));
		break;
	}
	case YZ:
	{
		vertices.push_back(glm::vec3(m_position.x, m_position.y - m_size, m_position.z - m_size));
		vertices.push_back(glm::vec3(m_position.x, m_position.y + m_size, m_position.z - m_size));
		vertices.push_back(glm::vec3(m_position.x, m_position.y + m_size, m_position.z + m_size));
		vertices.push_back(glm::vec3(m_position.x, m_position.y - m_size, m_position.z + m_size));
		break;
	}
	default:
		break;
	}


	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), NULL);
	glEnableVertexAttribArray(0);

	glDrawArrays(GL_QUADS, 0, 4);
	glBindVertexArray(0);
}