#ifndef XRAYMACHINE_H_INCLUDED
#define XRAYMACHINE_H_INCLUDED

#include <GL/glew.h>
#include <iostream>
#include <vector>

class XRayMachine
{
private:
	GLuint m_vbo;
	GLfloat m_size;
	std::vector<GLfloat>  vertices;

public:
	XRayMachine(GLfloat size);
	~XRayMachine();
	void setSize(GLfloat size);
	void draw();
	void generate();
};

#endif // XRAYMACHINE_H_INCLUDED