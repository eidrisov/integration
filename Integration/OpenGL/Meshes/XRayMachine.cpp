#include "XRayMachine.h"

XRayMachine::XRayMachine(GLfloat size)
{
    m_size = size;
	generate();
}

XRayMachine::~XRayMachine()
{
    vertices.clear();
	glDeleteBuffers(1, &m_vbo);
}

void XRayMachine::setSize(GLfloat size)
{
    m_size = size;
}

void XRayMachine::generate()
{
     vertices = {
        -m_size,-m_size,-m_size, // triangle 1 : begin
        -m_size,-m_size, m_size,
        -m_size, m_size, m_size, // triangle 1 : end
         m_size, m_size,-m_size, // triangle 2 : begin
        -m_size,-m_size,-m_size,
        -m_size, m_size,-m_size, // triangle 2 : end
         m_size,-m_size, m_size,
        -m_size,-m_size,-m_size,
         m_size,-m_size,-m_size,
         m_size, m_size,-m_size,
         m_size,-m_size,-m_size,
        -m_size,-m_size,-m_size,
        -m_size,-m_size,-m_size,
        -m_size, m_size, m_size,
        -m_size, m_size,-m_size,
         m_size,-m_size, m_size,
        -m_size,-m_size, m_size,
        -m_size,-m_size,-m_size,
        -m_size, m_size, m_size,
        -m_size,-m_size, m_size,
         m_size,-m_size, m_size,
         m_size, m_size, m_size,
         m_size,-m_size,-m_size,
         m_size, m_size,-m_size,
         m_size,-m_size,-m_size,
         m_size, m_size, m_size,
         m_size,-m_size, m_size,
         m_size, m_size, m_size,
         m_size, m_size,-m_size,
        -m_size, m_size,-m_size,
         m_size, m_size, m_size,
        -m_size, m_size,-m_size,
        -m_size, m_size, m_size,
         m_size, m_size, m_size,
        -m_size, m_size, m_size,
         m_size,-m_size, m_size
    };

	glGenBuffers(1, &m_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat), &vertices[0], GL_STATIC_DRAW);
}

void XRayMachine::draw()
{
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
    glDrawArrays(GL_TRIANGLES, 0, vertices.size());
}