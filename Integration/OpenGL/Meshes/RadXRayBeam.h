#ifndef RATXRAYBEAM_H_INCLUDED
#define RATXRAYBEAM_H_INCLUDED

#include <GL/glew.h>
#include <iostream>
#include "glm/glm.hpp"
#include <vector>

class RadXRayBeam
{
private:
	GLuint vbo;
	std::vector<glm::vec3> vertices;
	glm::vec3 m_collimatorOpening;
	glm::vec3 m_beamStartPosition, m_beamEndPosition;

public:
	RadXRayBeam();
	~RadXRayBeam();
	void setCollimatorOpening(glm::vec3 collimatorOpening);
	void generate();
	void setBeamStartPosition(glm::vec3 beamStartPosition);
	void setBeamEndPosition(glm::vec3 beamEndPosition);
	void draw();
};

#endif // RATXRAYBEAM_H_INCLUDED

