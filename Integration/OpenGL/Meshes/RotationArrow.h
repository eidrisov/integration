#ifndef RotationArrow_H_INCLUDED
#define RotationArrow_H_INCLUDED

#define _USE_MATH_DEFINES

#include <GL/glew.h>
#include <cmath>
#include "glm/glm.hpp"
#include <iostream>
#include <vector>

class RotationArrow
{
private:
	GLuint vboZ, vboY, vboX;
	std::vector<GLfloat> x_axis_vertices, y_axis_vertices, z_axis_vertices;
public:
	RotationArrow();
	~RotationArrow();
	void generateXaxis();
	void generateYaxis();
	void generateZaxis();
	void drawXaxis();
	void drawYaxis();
	void drawZaxis();
	void initArcs();
};

#endif // RotationArrow_H_INCLUDED

