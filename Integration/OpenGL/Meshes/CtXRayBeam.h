#ifndef CTXRAYBEAM_H_INCLUDED
#define CTXRAYBEAM_H_INCLUDED

#include <GL/glew.h>
#include <iostream>
#include "glm/glm.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <vector>

class CtXRayBeam
{
private:
	glm::vec3 m_fanAttributes;
	GLuint vbo;
	std::vector<glm::vec3> vertices;

public:
	CtXRayBeam();
	virtual ~CtXRayBeam();
	void setFanAttributes(glm::vec3 fanAttributes);
	void generate();
	void draw();
};

#endif // CTXRAYBEAM_H_INCLUDED

