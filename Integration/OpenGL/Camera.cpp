#include "Camera.h"
#include <glm/gtx/transform.hpp>

const float DEF_FOV = 45.0f;

Camera::Camera()
{
	mPosition = glm::vec3(0.0f, 0.0f, 0.0f);
	mTargetPos = glm::vec3(0.0f, 0.0f, 0.0f);
	mUp = glm::vec3(0.0f, 1.0f, 0.0f);
	mRight = glm::vec3(0.0f, 0.0f, 0.0f);
	WORLD_UP = glm::vec3(0.0f, 1.0f, 0.0f);
	mYaw = glm::pi<float>();
	mPitch = 0.0f;
	mFOV = DEF_FOV;
}

glm::mat4 Camera::getViewMatrix() const
{
	return glm::lookAt(mPosition, mTargetPos, mUp);
}

glm::highp_dmat4 Camera::getProjectionMatrix(int windowWidth, int windowHeight) const
{
	return glm::perspective(glm::radians(getFOV()), (float)(windowWidth / windowHeight), 0.1f, 100.0f);
}

const glm::vec3& Camera::getLook() const
{
	return mLook;
}

const glm::vec3& Camera::getRight() const
{
	return mRight;
}

const glm::vec3& Camera::getUp() const
{
	return mUp;
}

FPSCamera::FPSCamera(glm::vec3 position, float yaw, float pitch)
{
	mPosition = position;
	mYaw = yaw;
	mPitch = pitch;
}

void FPSCamera::setPosition(const glm::vec3& position)
{
	mPosition = position;
}

glm::vec3 FPSCamera::getPosition()
{
	return mPosition;
}

glm::vec3 FPSCamera::getCameraDirection()
{
	return glm::normalize(mPosition - mTargetPos);
}

void FPSCamera::move(const glm::vec3& offsetPos)
{
	mPosition += offsetPos;
	updateCameraVectors();
}

void FPSCamera::rotate(float yaw, float pitch)
{
	mYaw += glm::radians(yaw);
	mPitch += glm::radians(pitch);

	mPitch = glm::clamp(mPitch, -glm::pi<float>() / 2.0f + 0.1f, glm::pi<float>() / 2.0f - 0.1f);
	updateCameraVectors();
}

void FPSCamera::updateCameraVectors()
{
	glm::vec3 look;
	look.x = cosf(mPitch) * sinf(mYaw);
	look.y = sinf(mPitch);
	look.z = cosf(mPitch) * cosf(mYaw);

	mLook = glm::normalize(look);

	mRight = glm::normalize(glm::cross(mLook, WORLD_UP));
	mUp = glm::normalize(glm::cross(mRight, mLook));

	mTargetPos = mPosition + mLook;
}