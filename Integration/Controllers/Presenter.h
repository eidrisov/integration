#ifndef COMMUNICATIONMODULE_H_INCLUDED
#define COMMUNICATIONMODULE_H_INCLUDED

#include <wx/wx.h>
#include <wx/display.h>
#include <wx/event.h>
#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <wx/bitmap.h>
#include <thread>
#include <future>
#include "../Physics/SimulationResults.h"
#include "../Views/MainFrame.h"
#include "../Views/GeneralDialog.h"
#include "../Views/RadDialog.h"
#include "../Views/CtDialog.h"
#include "../Helpers/Constants.h"
#include "../Physics/monteCarlo.h"
#include "../Physics/inputData.h"
#include "../Physics/phantom.h"

class MainFrame;
class GeneralDialog;
class RadDialog;
class CtDialog;

class Presenter
{
public:
	Presenter();
	~Presenter();
	void createGeneralDialog();
	void createXRayMachineDialog();
	void showStartSimulation();
	bool simulationFrameExists();
	void startSimulation(std::atomic<bool>* stopThread, int ID_SIMULATION_RESULTS, int ID_FINISHED_THREAD);
	void setWindowPointerToNull(std::string param);
	void saveInputToAFile(wxString path, wxString fileName);
	void saveOutputToAFile(wxString path, wxString fileName);
	void importInput(wxString path, int ID_REDRAW);
	void closeDialogWindows();
	void loadPhantom(cInputData settings);
	void setInputSettings(cInputData input);
	cInputData getInputSettings();
	std::vector<cOrgan> getOrgans();

private:
	int displayWidth = 0, displayHeight = 0;
	wxDisplay* display = nullptr;
	GeneralDialog* generalDialog = nullptr;
	RadDialog* radDialog = nullptr;
	CtDialog* ctDialog = nullptr;
	MainFrame* mainFrame = nullptr;
	cInputData inputSettings;
	SimulationResults simulationResults;
	cPhantom* phantom = nullptr;
};

#endif // COMMUNICATIONMODULE_H_INCLUDED