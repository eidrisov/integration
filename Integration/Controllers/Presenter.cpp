#include "Presenter.h"

Presenter::Presenter()
{
	inputSettings = cInputData();
	phantom = new cPhantom();
	phantom->readPhantomBin(inputSettings.phantomFilename);
	inputSettings.setAllOrgansChecked(phantom->organ);
	inputSettings.setOrganStatus(false);

	display = new wxDisplay();
	displayWidth = display->GetGeometry().GetWidth();
	displayHeight = display->GetGeometry().GetHeight();

	generalDialog = new GeneralDialog(this);
	generalDialog->Show();
}

bool Presenter::simulationFrameExists()
{
	if (mainFrame == nullptr)
	{
		return false;
	}
	else
	{
		return true;
	}
}

void Presenter::createGeneralDialog()
{
	generalDialog = new GeneralDialog(this);
	generalDialog->readInputSettings();
	generalDialog->Show(true);
}

void Presenter::createXRayMachineDialog()
{
	if (inputSettings.systemType == inputSettings.rad)
	{
		radDialog = new RadDialog(this);
		radDialog->readInputSettings();
		radDialog->Show(true);
	}
	else
	{
		ctDialog = new CtDialog(this);
		ctDialog->readInputSettings();
		ctDialog->Show(true);
	}

}

cInputData Presenter::getInputSettings()
{
	return cInputData(inputSettings);
}


void Presenter::startSimulation(std::atomic<bool>* stopThread, int ID_SIMULATION_RESULTS, int ID_FINISHED_THREAD)
{
	bool simulate = true;
	cMonteCarlo* monteCarlo = new cMonteCarlo();

	monteCarlo->setInputData(inputSettings);
	monteCarlo->prepare();
	cPhantom phantom = monteCarlo->getPhantom();

	uint64_t totalPhotons{ monteCarlo->getRemainingPhotons() };

	while (simulate && !stopThread->load())
	{
		uint64_t simPhotons{ totalPhotons - monteCarlo->getRemainingPhotons() };
		mainFrame->setProgressBar(simPhotons * 100 / totalPhotons);
		simulate = monteCarlo->simulate(1000);
	}

	if (!stopThread->load())
	{
		monteCarlo->postProcessing();
		cResults results = monteCarlo->getResults();
		simulationResults.clear();
		simulationResults.setOrgan(phantom.organ);
		simulationResults.setEffectiveDoseTissue(phantom.effDoseTissue);


		for (int i = 0; i < simulationResults.getSizeOfOrgan(); i++)
		{
			if (results.massO.at(i) == 0)
			{
				simulationResults.setDoseOfOrganDose(0.0);
			}
			else
			{
				simulationResults.setDoseOfOrganDose(1000 * results.energyO.at(i) / results.massO.at(i));
			}

			if (results.energyO.at(i) == 0)
			{
				simulationResults.setAccuracyOfOrganDose(0.0);
			}
			else
			{
				simulationResults.setAccuracyOfOrganDose(sqrt(results.energyOVar.at(i)) / results.energyO.at(i) * 100.0);
			}
		}

		for (int i = 0; i < simulationResults.getSizeOfEffectiveDoseTissue(); i++)
		{
			if (results.massT.at(i) == 0)
			{
				simulationResults.setDoseOfEffectiveDose(0.0);
			}
			else
			{
				simulationResults.setDoseOfEffectiveDose(1000 * results.energyT.at(i) / results.massT.at(i));
			}

			if (results.energyT.at(i) == 0)
			{
				simulationResults.setAccuracyOfEffectiveDose(0.0);
			}
			else
			{
				simulationResults.setAccuracyOfEffectiveDose(sqrt(results.energyTVar.at(i)) / results.energyT.at(i) * 100.0);
			}

		}

		simulationResults.setE(results.E);
		simulationResults.setEStdev(results.EStdev);


		wxCommandEvent sendResults(wxEVT_COMMAND_TEXT_UPDATED, ID_SIMULATION_RESULTS);
		sendResults.SetClientData(&simulationResults);
		mainFrame->GetEventHandler()->AddPendingEvent(sendResults);
	}

	delete monteCarlo;

	wxCommandEvent threadIsFinished(wxEVT_COMMAND_TEXT_UPDATED, ID_FINISHED_THREAD);
	threadIsFinished.SetInt(1);
	mainFrame->GetEventHandler()->AddPendingEvent(threadIsFinished);
}

void Presenter::closeDialogWindows()
{
	if (generalDialog != nullptr) { generalDialog->Destroy(); }
}

void Presenter::importInput(wxString path, int ID_REDRAW)
{
	inputSettings.importInput(path.ToStdString());
	if (mainFrame) { mainFrame->updateGLCanvasInputSettings(inputSettings); }
}

void Presenter::saveInputToAFile(wxString path, wxString fileName)
{
	inputSettings.saveInAFile(path.ToStdString());
}

void Presenter::saveOutputToAFile(wxString path, wxString fileName)
{
	if (!simulationResults.isEmpty())
	{
		simulationResults.saveToFile(path.ToStdString());
	}
	else
	{
		mainFrame->showNoResultsMessage();
	}
}

void Presenter::setWindowPointerToNull(std::string param)
{
	if (param == GENERAL_DIALOG)
	{
		generalDialog = nullptr;
	}
	else if (param == MAIN_FRAME)
	{
		mainFrame = nullptr;
	}
	else if (param == RAD_DIALOG)
	{
		radDialog = nullptr;
	}
	else if (param == CT_DIALOG)
	{
		ctDialog = nullptr;
	}
}

void Presenter::setInputSettings(cInputData settings)
{
	inputSettings.copy(settings);
	if (mainFrame) { mainFrame->updateGLCanvasInputSettings(inputSettings); }
	inputSettings.setOrganStatus(false);
}

void Presenter::showStartSimulation()
{
	if (mainFrame == nullptr)
	{
		mainFrame = new MainFrame(this, displayWidth, displayHeight);
		mainFrame->Show(true);
	}
}

void Presenter::loadPhantom(cInputData settings) {
	if (inputSettings.phantomFilename  != settings.phantomFilename)
	{
		if (phantom)
		{
			delete phantom;
			phantom = nullptr;
		}
		phantom = new cPhantom();
		phantom->readPhantomBin(settings.phantomFilename);
	}
	inputSettings.setAllOrgansChecked(phantom->organ);
	inputSettings.setOrganStatus(false);
}

std::vector<cOrgan> Presenter::getOrgans()
{
	return phantom->organ;
}

Presenter::~Presenter()
{
	if (mainFrame) { mainFrame->Destroy(); }
	if (generalDialog) { generalDialog->Destroy(); }
	if (radDialog) { radDialog->Destroy(); }
	if (ctDialog) { ctDialog->Destroy(); }
	if (phantom) { delete phantom; }
	// if (display) { delete display; }
}