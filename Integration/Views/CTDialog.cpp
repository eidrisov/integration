#include "CtDialog.h"

namespace fs = std::filesystem;

BEGIN_EVENT_TABLE(CtDialog, wxFrame)

END_EVENT_TABLE()

CtDialog::CtDialog(Presenter* presenter) :
	wxFrame(NULL, wxID_ANY, wxT("CT dialog"), wxDefaultPosition, wxDefaultSize)
{
	topSizerVert = new wxBoxSizer(wxVERTICAL);
	topSizerHor = new wxBoxSizer(wxHORIZONTAL);
	controlVerSizer = new wxBoxSizer(wxVERTICAL);
	for (int i = 0; i < 9; i++)
	{
		horSizer[i] = new wxBoxSizer(wxHORIZONTAL);
	}

	doubleValidator = new wxFloatingPointValidator<double>(2, NULL, wxNUM_VAL_DEFAULT);

	this->presenter = presenter;
	control = new wxControl(this, wxID_ANY, wxPoint(0, 0), wxSize(this->GetClientSize().x, this->GetClientSize().y));


	shiftStaticText = new wxStaticText(control, wxID_ANY, wxT(""), wxDefaultPosition,
		wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	radiusCTStaticText = new wxStaticText(control, wxID_ANY, wxT("Radius (cm)"), wxDefaultPosition,
		wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
	radiusCT = new wxTextCtrl(control, wxID_ANY, wxT("60.00"), wxDefaultPosition,
		wxSize(textCtrlWidth, textCtrlHeight), wxTE_CENTRE, *doubleValidator);
	radiusCT->SetMaxLength(10);

	horSizer[0]->Add(radiusCTStaticText, 0, wxALL, 5);
	horSizer[1]->Add(shiftStaticText, 0, wxALL, 5);
	horSizer[1]->Add(radiusCT, 0, wxALL, 5);

	fanAngleStaticText[0] = new wxStaticText(control, wxID_ANY, wxT("Fan angle (�)"),
		wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
	fanAngleStaticText[1] = new wxStaticText(control, wxID_ANY, wxT("width: "),
		wxDefaultPosition, wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	fanAngle[0] = new wxTextCtrl(control, wxID_ANY, wxT("30.00"),
		wxDefaultPosition, wxSize(textCtrlWidth, textCtrlHeight), wxTE_CENTRE, *doubleValidator);
	fanAngle[0]->SetMaxLength(10);
	fanAngleStaticText[2] = new wxStaticText(control, wxID_ANY, wxT("thick\nness: "),
		wxDefaultPosition, wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	fanAngle[1] = new wxTextCtrl(control, wxID_ANY, wxT("2.00"),
		wxDefaultPosition, wxSize(textCtrlWidth, textCtrlHeight), wxTE_CENTRE, *doubleValidator);
	fanAngle[1]->SetMaxLength(10);

	horSizer[2]->Add(fanAngleStaticText[0], 0, wxALL, 5);
	horSizer[3]->Add(fanAngleStaticText[1], 0, wxALL, 5);
	horSizer[3]->Add(fanAngle[0], 0, wxALL, 5);
	horSizer[3]->Add(fanAngleStaticText[2], 0, wxALL, 5);
	horSizer[3]->Add(fanAngle[1], 0, wxALL, 5);

	gantryPosStaticText[0] = new wxStaticText(control, wxID_ANY, wxT("Gantry position (cm)"),
		wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
	gantryPosStaticText[1] = new wxStaticText(control, wxID_ANY, wxT("X: "),
		wxDefaultPosition, wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	gantryPos[0] = new wxTextCtrl(control, wxID_ANY, wxT("26.50"),
		wxDefaultPosition, wxSize(textCtrlWidth, textCtrlHeight), wxTE_CENTRE, *doubleValidator);
	gantryPos[0]->SetMaxLength(10);
	gantryPosStaticText[2] = new wxStaticText(control, wxID_ANY, wxT("Y: "),
		wxDefaultPosition, wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	gantryPos[1] = new wxTextCtrl(control, wxID_ANY, wxT("12.20"),
		wxDefaultPosition, wxSize(textCtrlWidth, textCtrlHeight), wxTE_CENTRE, *doubleValidator);
	gantryPos[1]->SetMaxLength(10);
	gantryPosStaticText[3] = new wxStaticText(control, wxID_ANY, wxT("Z: "),
		wxDefaultPosition, wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	gantryPos[2] = new wxTextCtrl(control, wxID_ANY, wxT("160.00"),
		wxDefaultPosition, wxSize(textCtrlWidth, textCtrlHeight), wxTE_CENTRE, *doubleValidator);
	gantryPos[2]->SetMaxLength(10);

	horSizer[4]->Add(gantryPosStaticText[0], 0, wxALL, 5);
	horSizer[5]->Add(gantryPosStaticText[1], 0, wxALL, 5);
	horSizer[5]->Add(gantryPos[0], 0, wxALL, 5);
	horSizer[5]->Add(gantryPosStaticText[2], 0, wxALL, 5);
	horSizer[5]->Add(gantryPos[1], 0, wxALL, 5);
	horSizer[5]->Add(gantryPosStaticText[3], 0, wxALL, 5);
	horSizer[5]->Add(gantryPos[2], 0, wxALL, 5);

	gantryRotStatictext[0] = new wxStaticText(control, wxID_ANY, wxT("Gantry rotation (�)"),
		wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
	gantryRotStatictext[1] = new wxStaticText(control, wxID_ANY, wxT("X: "),
		wxDefaultPosition, wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	gantryRot[0] = new wxTextCtrl(control, wxID_ANY, wxT("0.00"),
		wxDefaultPosition, wxSize(textCtrlWidth, textCtrlHeight), wxTE_CENTRE, *doubleValidator);
	gantryRot[0]->SetMaxLength(10);
	gantryRotStatictext[2] = new wxStaticText(control, wxID_ANY, wxT("Y: "),
		wxDefaultPosition, wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	gantryRot[1] = new wxTextCtrl(control, wxID_ANY, wxT("0.00"),
		wxDefaultPosition, wxSize(textCtrlWidth, textCtrlHeight), wxTE_CENTRE, *doubleValidator);
	gantryRot[1]->SetMaxLength(10);
	gantryRotStatictext[3] = new wxStaticText(control, wxID_ANY, wxT("Z: "),
		wxDefaultPosition, wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	gantryRot[2] = new wxTextCtrl(control, wxID_ANY, wxT("0.00"),
		wxDefaultPosition, wxSize(textCtrlWidth, textCtrlHeight), wxTE_CENTRE, *doubleValidator);
	gantryRot[2]->SetMaxLength(10);

	horSizer[6]->Add(gantryRotStatictext[0], 0, wxALL, 5);
	horSizer[7]->Add(gantryRotStatictext[1], 0, wxALL, 5);
	horSizer[7]->Add(gantryRot[0], 0, wxALL, 5);
	horSizer[7]->Add(gantryRotStatictext[2], 0, wxALL, 5);
	horSizer[7]->Add(gantryRot[1], 0, wxALL, 5);
	horSizer[7]->Add(gantryRotStatictext[3], 0, wxALL, 5);
	horSizer[7]->Add(gantryRot[2], 0, wxALL, 5);

	okButton = new wxButton(control, wxID_ANY, wxT("OK"),
		wxDefaultPosition, wxSize(buttonWidth, buttonHeight), 0, wxDefaultValidator, wxT("OK"));
	okButton->Bind(wxEVT_BUTTON, &CtDialog::onOk, this);

	horSizer[8]->Add(okButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);

	for (int i = 0; i < 8; i++)
	{
		if (i % 2 == 0)
		{
			controlVerSizer->Add(horSizer[i], 0, wxALL | wxALIGN_CENTER, 5);
		}
		else
		{
			controlVerSizer->Add(horSizer[i], 0, wxRIGHT, 5);
		}
	}
	controlVerSizer->Add(horSizer[8], 0, wxALL | wxALIGN_CENTER, 30);

	control->SetSizer(controlVerSizer);
	controlVerSizer->Fit(control);
	controlVerSizer->SetSizeHints(control);

	topSizerHor->Add(control, 1, wxALL | wxEXPAND);
	topSizerVert->Add(topSizerHor, 1, wxALL | wxEXPAND);

	SetSizer(topSizerVert);
	topSizerVert->Fit(this);
	topSizerVert->SetSizeHints(this);

	Centre();
	this->SetWindowStyle(wxDEFAULT_FRAME_STYLE ^ wxRESIZE_BORDER);
	this->Bind(wxEVT_CLOSE_WINDOW, &CtDialog::onClose, this);
}

void CtDialog::onClose(wxCloseEvent& event)
{
	presenter->setWindowPointerToNull(CT_DIALOG);

	if (!presenter->simulationFrameExists())
	{
		presenter->~Presenter();
	}

	event.Skip();
}

void CtDialog::onOk(wxCommandEvent& event)
{
	wxString message = "";
	bool errors = false;

	if (radiusCT->IsEmpty())
	{
		message += "Invalid value: radius CT.\n";
		errors = true;
	}

	if (fanAngle[0]->IsEmpty())
	{
		message += "Invalid value: fan angle width.\n";
		errors = true;
	}

	if (fanAngle[1]->IsEmpty())
	{
		message += "Invalid value: fan angle thickness.\n";
		errors = true;
	}

	if (gantryPos[0]->IsEmpty())
	{
		message += "Invalid value: gantry position X.\n";
		errors = true;
	}

	if (gantryPos[1]->IsEmpty())
	{
		message += "Invalid value: gantry position Y.\n";
		errors = true;
	}

	if (gantryPos[2]->IsEmpty())
	{
		message += "Invalid value: gantry position Z.\n";
		errors = true;
	}

	if (gantryRot[0]->IsEmpty())
	{
		message += "Invalid value: gantry rotation X.\n";
		errors = true;
	}

	if (gantryRot[1]->IsEmpty())
	{
		message += "Invalid value: gantry rotation Y.\n";
		errors = true;
	}

	if (gantryRot[2]->IsEmpty())
	{
		message += "Invalid value: gantry rotation Z.\n";
		errors = true;
	}

	if (errors)
	{
		wxMessageBox(message);
	}
	else
	{
		cInputData input = presenter->getInputSettings();

		double doubleValue = 0;
		radiusCT->GetValue().ToDouble(&doubleValue);
		input.radiusCt = doubleValue / 100.0f;

		fanAngle[0]->GetValue().ToDouble(&doubleValue);
		input.fanAngleWidth = doubleValue * M_PI / 180.0f;
		fanAngle[1]->GetValue().ToDouble(&doubleValue);
		input.fanAngleThickness = doubleValue * M_PI / 180.0f;

		gantryPos[0]->GetValue().ToDouble(&doubleValue);
		input.gantryPos[0] = doubleValue / 100.0f;
		gantryPos[1]->GetValue().ToDouble(&doubleValue);
		input.gantryPos[1] = doubleValue / 100.0f;
		gantryPos[2]->GetValue().ToDouble(&doubleValue);
		input.gantryPos[2] = doubleValue / 100.0f;

		gantryRot[0]->GetValue().ToDouble(&doubleValue);
		input.gantryRot[0] = doubleValue * M_PI / 180.0f;
		gantryRot[1]->GetValue().ToDouble(&doubleValue);
		input.gantryRot[1] = doubleValue * M_PI / 180.0f;
		gantryRot[2]->GetValue().ToDouble(&doubleValue);
		input.gantryRot[2] = doubleValue * M_PI / 180.0f;

		presenter->setInputSettings(input);
	}
}

void CtDialog::readInputSettings()
{
	cInputData input = presenter->getInputSettings();

	if (input.systemType == input.ct)
	{
		radiusCT->ChangeValue(wxString::Format("%.2lf", input.radiusCt * 100.0f));

		fanAngle[0]->ChangeValue(wxString::Format("%.2lf", input.fanAngleWidth * 180.0f / M_PI));
		fanAngle[1]->ChangeValue(wxString::Format("%.2lf", input.fanAngleThickness * 180.0f / M_PI));

		gantryPos[0]->ChangeValue(wxString::Format("%.2lf", input.gantryPos[0] * 100.0f));
		gantryPos[1]->ChangeValue(wxString::Format("%.2lf", input.gantryPos[1] * 100.0f));
		gantryPos[2]->ChangeValue(wxString::Format("%.2lf", input.gantryPos[2] * 100.0f));

		gantryRot[0]->ChangeValue(wxString::Format("%.2lf", input.gantryRot[0] * 180.0f / M_PI));
		gantryRot[1]->ChangeValue(wxString::Format("%.2lf", input.gantryRot[1] * 180.0f / M_PI));
		gantryRot[2]->ChangeValue(wxString::Format("%.2lf", input.gantryRot[2] * 180.0f / M_PI));
	}
}

void CtDialog::destroyStaticTextArray(wxStaticText* arrayOfStaticText)
{
	int size = (sizeof(arrayOfStaticText) / sizeof(*arrayOfStaticText));

	for (int i = 0; i < size; i++)
	{
		delete (arrayOfStaticText + i);
	}
}

void CtDialog::destroyTextCtrlArray(wxTextCtrl* arrayOfTextCtl)
{
	int size = (sizeof(arrayOfTextCtl) / sizeof(*arrayOfTextCtl));

	for (int i = 0; i < size; i++)
	{
		delete (arrayOfTextCtl + i);
	}
}

CtDialog::~CtDialog()
{
	delete shiftStaticText;
	delete doubleValidator;
	delete radiusCT, radiusCTStaticText;
	destroyStaticTextArray(*fanAngleStaticText);
	destroyStaticTextArray(*gantryPosStaticText);
	destroyStaticTextArray(*gantryRotStatictext);
	destroyTextCtrlArray(*fanAngle);
	destroyTextCtrlArray(*gantryPos);
	destroyTextCtrlArray(*gantryRot);
	delete okButton;
	delete control;
}