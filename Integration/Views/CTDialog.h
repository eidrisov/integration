#ifndef CTDIALOG_H_INCLUDED
#define CTDIALOG_H_INCLUDED

#include <wx/wx.h>
#include <wx/frame.h>
#include <string>
#include <iostream>
#include <filesystem>
#include <wx/button.h>
#include <wx/radiobut.h>
#include <wx/textctrl.h>
#include <wx/stattext.h>
#include <wx/valnum.h>
#include <wx/event.h>
#include <math.h>
#include <wx/sizer.h>
#include "../Controllers/Presenter.h"
#include "../Physics/inputData.h"

class Presenter;

class CtDialog : public wxFrame
{
private:
	wxControl* control = nullptr;
	wxButton* okButton = nullptr;
	wxTextCtrl* gantryPos[3], * fanAngle[2], * radiusCT, * gantryRot[3];
	wxStaticText* gantryPosStaticText[4], * fanAngleStaticText[3], * radiusCTStaticText,
		* gantryRotStatictext[4], * shiftStaticText = nullptr;
	int staticTextWidth = 35, staticTextHeight = 30, textCtrlWidth = 70, textCtrlHeight = 23,
		buttonWidth = 75, buttonHeight = 22;
	Presenter* presenter = nullptr;
	wxFloatingPointValidator<double>* doubleValidator = nullptr;
	wxSizer* topSizerHor = nullptr, * topSizerVert = nullptr, * controlVerSizer = nullptr, * horSizer[9];

public:
	CtDialog(Presenter* presenter);
	~CtDialog();
	void onOk(wxCommandEvent& event);
	void destroyStaticTextArray(wxStaticText* arrayOfStaticText);
	void destroyTextCtrlArray(wxTextCtrl* arrayOfTextCtl);
	void onClose(wxCloseEvent& event);
	void readInputSettings();

	DECLARE_EVENT_TABLE()
};

#endif // CTDIALOG_H_INCLUDED