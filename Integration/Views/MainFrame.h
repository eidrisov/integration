#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#include <wx/wx.h>
#include <wx/frame.h>
#include <wx/event.h>
#include <wx/ribbon/bar.h>
#include <wx/ribbon/buttonbar.h>
#include <wx/ribbon/panel.h>
#include <wx/ribbon/page.h>
#include <wx/ribbon/art.h>
#include <wx/toolbar.h>
#include <wx/ribbon/toolbar.h>
#include <wx/artprov.h>
#include <wx/bitmap.h>
#include <wx/sizer.h>
#include <wx/image.h>
#include <wx/notebook.h>
#include <wx/listctrl.h>
#include <thread>
#include <iostream>
#include <string>
#include <future>
#include <wx/filedlg.h>
#include <wx/statusbr.h>
#include "ResultsWindow.h"
#include "../Physics/phantom.h"
#include "MyGLCanvas.h"
#include "../Controllers/Presenter.h"
#include "ListOfOrgansDialog.h"

class MyGLCanvas;
class Presenter;
class ListOfOrgansDialog;

class MainFrame : public wxFrame
{
private:
	MyGLCanvas* myCanvas = nullptr;
	Presenter* m_presenter = nullptr;
	wxRibbonPage* filePage = nullptr, *settingsPage = nullptr, *simulationPage = nullptr;
	wxRibbonBar* ribbonBar = nullptr;
	wxRibbonPanel* filePanel = nullptr, *settingsPanel = nullptr, *simulationPanel = nullptr;
	wxRibbonButtonBar* ribbonButtons = nullptr;
	wxRibbonToolBar* fileBar = nullptr, *settingsBar = nullptr, *simulationBar = nullptr;
	wxSizer* topSizer = nullptr, *ribbonSizer = nullptr, *noteBookSizer = nullptr, *gaugeSizer = nullptr, *simTextSizer = nullptr;
	wxBitmap* generalSettingsIcon, *simulationSpecificlSettingsIcon, * startIcon = nullptr, * stopIcon = nullptr, * exportInputIcon = nullptr, * exportOutputIcon = nullptr,
		* saveInputIcon = nullptr, * importInputIcon = nullptr, *humanBodyIcon = nullptr;
	wxNotebook* myNoteBook = nullptr;
	wxListCtrl* organDoseLC = nullptr, * tissueDoseLC = nullptr;
	wxGauge* gauge = nullptr;
	wxStaticText* simBarText = nullptr;
	std::thread simulationThread;
	std::atomic<bool> stopThread;

public:
	MainFrame(Presenter* presenter, int width = 1000, int height = 800);
	~MainFrame();
	void onClose(wxCloseEvent& event);
	void onGeneralSettings(wxRibbonToolBarEvent& event);
	void onSimulationSpecificSettings(wxRibbonToolBarEvent& event);
	void onStart(wxRibbonToolBarEvent& event);
	void onStop(wxRibbonToolBarEvent& event);
	void addResultPage(wxCommandEvent& event);
	void finishedThread(wxCommandEvent& event);
	void saveInput(wxRibbonToolBarEvent& event);
	void saveOutput(wxRibbonToolBarEvent& event);
	void importInput(wxRibbonToolBarEvent& event);
	void showOrgans(wxRibbonToolBarEvent& event);
	void showNoResultsMessage();
	void updateGLCanvasInputSettings(cInputData inputSettings);
	void setProgressBar(int value);
	wxBitmap* readImage(wxString filename);
	Presenter* getPresenter();

	enum {
		ID_GENERAL_SETTINGS = wxID_HIGHEST + 1,
		ID_SIMULATION_SPECIFIC_SETTINGS,
		ID_START,
		ID_STOP,
		ID_ORGAN_DOSE,
		ID_EFFECTIVE_DOSE,
		ID_SIMULATION_RESULTS,
		ID_FINISHED_THREAD,
		ID_SAVE_INPUT,
		ID_IMPORT_INPUT,
		ID_GL_CANVAS,
		ID_EXPORT_OUTPUT,
		ID_HUMAN_BODY,
		ID_REDRAW
	};

	DECLARE_EVENT_TABLE()
};

#endif // MAIN_H_INCLUDED