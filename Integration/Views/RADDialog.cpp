#include "RadDialog.h"

namespace fs = std::filesystem;

BEGIN_EVENT_TABLE(RadDialog, wxFrame)

END_EVENT_TABLE()

RadDialog::RadDialog(Presenter* presenter) :
	wxFrame(NULL, wxID_ANY, wxT("RAD dialog"), wxDefaultPosition, wxDefaultSize)
{
	topSizerVert = new wxBoxSizer(wxVERTICAL);
	topSizerHor = new wxBoxSizer(wxHORIZONTAL);
	controlVerSizer = new wxBoxSizer(wxVERTICAL);
	for (int i = 0; i < 9; i++)
	{
		horSizer[i] = new wxBoxSizer(wxHORIZONTAL);
	}

	doubleValidator = new wxFloatingPointValidator<double>(2, NULL, wxNUM_VAL_DEFAULT);

	this->presenter = presenter;
	control = new wxControl(this, wxID_ANY, wxPoint(0, 0), wxSize(this->GetClientSize().x, this->GetClientSize().y));

	tubePosStaticText[0] = new wxStaticText(control, wxID_ANY, wxT("Tube position (cm)"),
		wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
	tubePosStaticText[1] = new wxStaticText(control, wxID_ANY, wxT("X: "),
		wxDefaultPosition, wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	tubePos[0] = new wxTextCtrl(control, wxID_ANY, wxT("26.50"),
		wxDefaultPosition, wxSize(textCtrlWidth, textCtrlHeight), wxTE_CENTRE, *doubleValidator);
	tubePosStaticText[2] = new wxStaticText(control, wxID_ANY, wxT("Y: "),
		wxDefaultPosition, wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	tubePos[1] = new wxTextCtrl(control, wxID_ANY, wxT("-150.00"),
		wxDefaultPosition, wxSize(textCtrlWidth, textCtrlHeight), wxTE_CENTRE, *doubleValidator);
	tubePosStaticText[3] = new wxStaticText(control, wxID_ANY, wxT("Z: "),
		wxDefaultPosition, wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	tubePos[2]= new wxTextCtrl(control, wxID_ANY, wxT("125.00"),
		wxDefaultPosition, wxSize(textCtrlWidth, textCtrlHeight), wxTE_CENTRE, *doubleValidator);

	horSizer[0]->Add(tubePosStaticText[0], 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);
	horSizer[1]->Add(tubePosStaticText[1], 0, wxALL, 5);
	horSizer[1]->Add(tubePos[0], 0, wxALL, 5);
	horSizer[1]->Add(tubePosStaticText[2], 0, wxALL, 5);
	horSizer[1]->Add(tubePos[1], 0, wxALL, 5);
	horSizer[1]->Add(tubePosStaticText[3], 0, wxALL, 5);
	horSizer[1]->Add(tubePos[2], 0, wxALL, 5);

	collOpeningStaticText[0] = new wxStaticText(control, wxID_ANY, wxT("Coll opening (cm)"),
		wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
	collOpeningStaticText[1] = new wxStaticText(control, wxID_ANY, wxT("X: "),
		wxDefaultPosition, wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	collOpening[0] = new wxTextCtrl(control, wxID_ANY, wxT("16.00"),
		wxDefaultPosition, wxSize(textCtrlWidth, textCtrlHeight), wxTE_CENTRE, *doubleValidator);
	collOpeningStaticText[2] = new wxStaticText(control, wxID_ANY, wxT("Y: "),
		wxDefaultPosition, wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	collOpening[1] = new wxTextCtrl(control, wxID_ANY, wxT("22.00"),
		wxDefaultPosition, wxSize(textCtrlWidth, textCtrlHeight), wxTE_CENTRE, *doubleValidator);

	horSizer[2]->Add(collOpeningStaticText[0], 0, wxALL, 5);
	horSizer[3]->Add(collOpeningStaticText[1], 0, wxALL, 5);
	horSizer[3]->Add(collOpening[0], 0, wxALL, 5);
	horSizer[3]->Add(collOpeningStaticText[2], 0, wxALL, 5);
	horSizer[3]->Add(collOpening[1], 0, wxALL, 5);

	shiftStaticText = new wxStaticText(control, wxID_ANY, wxT(""), wxDefaultPosition,
		wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	collDistanceStaticText = new wxStaticText(control, wxID_ANY, wxT("Coll distance (cm)"),
		wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
	collDistance = new wxTextCtrl(control, wxID_ANY, wxT("90.00"),
		wxDefaultPosition, wxSize(textCtrlWidth, textCtrlHeight), wxTE_CENTRE, *doubleValidator);

	horSizer[4]->Add(collDistanceStaticText, 0, wxALL, 5);
	horSizer[5]->Add(shiftStaticText, 0, wxALL, 5);
	horSizer[5]->Add(collDistance, 0, wxALL, 5);

	tubeRotStatictext[0] = new wxStaticText(control, wxID_ANY, wxT("Tube rotation (�)"),
		wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
	tubeRotStatictext[1] = new wxStaticText(control, wxID_ANY, wxT("X: "),
		wxDefaultPosition, wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	tubeRot[0] = new wxTextCtrl(control, wxID_ANY, wxT("-90.00"),
		wxDefaultPosition, wxSize(textCtrlWidth, textCtrlHeight), wxTE_CENTRE, *doubleValidator);
	tubeRotStatictext[2] = new wxStaticText(control, wxID_ANY, wxT("Y: "),
		wxDefaultPosition, wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	tubeRot[1] = new wxTextCtrl(control, wxID_ANY, wxT("0.00"),
		wxDefaultPosition, wxSize(textCtrlWidth, textCtrlHeight), wxTE_CENTRE, *doubleValidator);
	tubeRotStatictext[3] = new wxStaticText(control, wxID_ANY, wxT("Z: "),
		wxDefaultPosition, wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	tubeRot[2] = new wxTextCtrl(control, wxID_ANY, wxT("0.00"),
		wxDefaultPosition, wxSize(textCtrlWidth, textCtrlHeight), wxTE_CENTRE, *doubleValidator);

	horSizer[6]->Add(tubeRotStatictext[0], 0, wxALL, 5);
	horSizer[7]->Add(tubeRotStatictext[1], 0, wxALL, 5);
	horSizer[7]->Add(tubeRot[0], 0, wxALL, 5);
	horSizer[7]->Add(tubeRotStatictext[2], 0, wxALL, 5);
	horSizer[7]->Add(tubeRot[1], 0, wxALL, 5);
	horSizer[7]->Add(tubeRotStatictext[3], 0, wxALL, 5);
	horSizer[7]->Add(tubeRot[2], 0, wxALL, 5);

	okButton = new wxButton(control, wxID_ANY, wxT("OK"),
		wxDefaultPosition, wxSize(buttonWidth, buttonHeight), 0, wxDefaultValidator, wxT("OK"));
	okButton->Bind(wxEVT_BUTTON, &RadDialog::onOk, this);

	horSizer[8]->Add(okButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);

	for (int i = 0; i < 8; i++)
	{
		if (i % 2 == 0) 
		{
			controlVerSizer->Add(horSizer[i], 0, wxALL | wxALIGN_CENTER, 5);
		}
		else
		{
			controlVerSizer->Add(horSizer[i], 0, wxRIGHT, 5);
		}
	}
	controlVerSizer->Add(horSizer[8], 0, wxALL | wxALIGN_CENTER, 30);

	control->SetSizer(controlVerSizer);
	controlVerSizer->Fit(control);
	controlVerSizer->SetSizeHints(control);

	topSizerHor->Add(control, 1, wxALL | wxEXPAND);
	topSizerVert->Add(topSizerHor, 1, wxALL | wxEXPAND);

	SetSizer(topSizerVert);
	topSizerVert->Fit(this);
	topSizerVert->SetSizeHints(this);

	Centre();
	this->SetWindowStyle(wxDEFAULT_FRAME_STYLE ^ wxRESIZE_BORDER);
	this->Bind(wxEVT_CLOSE_WINDOW, &RadDialog::onClose, this);
}

void RadDialog::onClose(wxCloseEvent& event)
{
	presenter->setWindowPointerToNull(RAD_DIALOG);

	if (!presenter->simulationFrameExists())
	{
		presenter->~Presenter();
	}

	event.Skip();
}

void RadDialog::onOk(wxCommandEvent& event)
{

	wxString message = "";
	bool errors = false;

	tubePos[0]->Validate();
	tubePos[1]->Validate();
	tubePos[2]->Validate();
	collOpening[0]->Validate();
	collOpening[1]->Validate();
	collDistance->Validate();
	tubeRot[0]->Validate();
	tubeRot[1]->Validate();
	tubeRot[2]->Validate();

	if (tubePos[0]->IsEmpty())
	{
		message += "Invalid value: tube position X.\n";
		errors = true;
	}

	if (tubePos[1]->IsEmpty())
	{
		message += "Invalid value: tube position Y.\n";
		errors = true;
	}

	if (tubePos[2]->IsEmpty())
	{
		message += "Invalid value: tube position Z.\n";
		errors = true;
	}

	if (collOpening[0]->IsEmpty())
	{
		message += "Invalid value: collimator opening X.\n";
		errors = true;
	}

	if (collOpening[1]->IsEmpty())
	{
		message += "Invalid value: collimator opening Y.\n";
		errors = true;
	}

	if (collDistance->IsEmpty())
	{
		message += "Invalid value: collimator distance.\n";
		errors = true;
	}

	if (tubeRot[0]->IsEmpty())
	{
		message += "Invalid value: tube rotation X.\n";
		errors = true;
	}

	if (tubeRot[1]->IsEmpty())
	{
		message += "Invalid value: tube rotation Y.\n";
		errors = true;
	}

	if (tubeRot[2]->IsEmpty())
	{
		message += "Invalid value: tube rotation Z.\n";
		errors = true;
	}

	if (errors)
	{
		wxMessageBox(message);
	}
	else
	{
		cInputData input;

		double doubleValue = 0;
		tubePos[0]->GetValue().ToDouble(&doubleValue);
		input.tubePos[0] = doubleValue / 100.0f;
		tubePos[1]->GetValue().ToDouble(&doubleValue);
		input.tubePos[1] = doubleValue / 100.0f;
		tubePos[2]->GetValue().ToDouble(&doubleValue);
		input.tubePos[2] = doubleValue / 100.0f;

		collOpening[0]->GetValue().ToDouble(&doubleValue);
		input.collOpening[0] = doubleValue / 100.0f;
		collOpening[1]->GetValue().ToDouble(&doubleValue);
		input.collOpening[1] = doubleValue / 100.0f;

		collDistance->GetValue().ToDouble(&doubleValue);
		input.collDistance = doubleValue / 100.0f;

		tubeRot[0]->GetValue().ToDouble(&doubleValue);
		input.tubeRot[0] = doubleValue * M_PI / 180.0f;
		tubeRot[1]->GetValue().ToDouble(&doubleValue);
		input.tubeRot[1] = doubleValue * M_PI / 180.0f;
		tubeRot[2]->GetValue().ToDouble(&doubleValue);
		input.tubeRot[2] = doubleValue * M_PI / 180.0f;

		presenter->setInputSettings(input);
	}
}

void RadDialog::readInputSettings()
{
	cInputData input = presenter->getInputSettings();

	if (input.systemType == input.rad)
	{
		tubePos[0]->ChangeValue(wxString::Format("%.2lf", input.tubePos[0] * 100.0f));
		tubePos[1]->ChangeValue(wxString::Format("%.2lf", input.tubePos[1] * 100.0f));
		tubePos[2]->ChangeValue(wxString::Format("%.2lf", input.tubePos[2] * 100.0f));

		collOpening[0]->ChangeValue(wxString::Format("%.2lf", input.collOpening[0] * 100.0f));
		collOpening[1]->ChangeValue(wxString::Format("%.2lf", input.collOpening[1] * 100.0f));

		collDistance->ChangeValue(wxString::Format("%.2lf", input.collDistance * 100.0f));

		tubeRot[0]->ChangeValue(wxString::Format("%.2lf", input.tubeRot[0] * 180.0f / M_PI));
		tubeRot[1]->ChangeValue(wxString::Format("%.2lf", input.tubeRot[1] * 180.0f / M_PI));
		tubeRot[2]->ChangeValue(wxString::Format("%.2lf", input.tubeRot[2] * 180.0f / M_PI));
	}
}

void RadDialog::destroyStaticTextArray(wxStaticText* arrayOfStaticText)
{
	int size = (sizeof(arrayOfStaticText) / sizeof(*arrayOfStaticText));

	for (int i = 0; i < size; i++)
	{
		delete (arrayOfStaticText + i);
	}
}

void RadDialog::destroyTextCtrlArray(wxTextCtrl* arrayOfTextCtl)
{
	int size = (sizeof(arrayOfTextCtl) / sizeof(*arrayOfTextCtl));

	for (int i = 0; i < size; i++)
	{
		delete (arrayOfTextCtl + i);
	}
}

RadDialog::~RadDialog()
{
	delete shiftStaticText;
	delete doubleValidator;
	delete collDistance, collDistanceStaticText;
	destroyStaticTextArray(*tubePosStaticText);
	destroyStaticTextArray(*collOpeningStaticText);
	destroyStaticTextArray(*tubeRotStatictext);
	destroyTextCtrlArray(*collOpening);
	destroyTextCtrlArray(*tubeRot);
	destroyTextCtrlArray(*tubePos);
	delete okButton;
	delete control;
}