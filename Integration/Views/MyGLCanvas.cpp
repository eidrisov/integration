#include "MyGLCanvas.h"
#include <glm/gtc/type_ptr.hpp>
#include <sstream>
#include <iomanip>

BEGIN_EVENT_TABLE(MyGLCanvas, wxGLCanvas)
EVT_PAINT(MyGLCanvas::paintEvent)
EVT_CHAR(MyGLCanvas::onKeyPressed)
EVT_LEFT_DOWN(MyGLCanvas::onLeftMouseDown)
EVT_LEFT_UP(MyGLCanvas::onLeftMouseUp)
EVT_RIGHT_DOWN(MyGLCanvas::onRightMouseDown)
EVT_RIGHT_UP(MyGLCanvas::onRightMouseUp)
EVT_KEY_UP(MyGLCanvas::onKeyUp)
EVT_MOUSEWHEEL(MyGLCanvas::onMouseWheel)
EVT_SIZE(MyGLCanvas::onResize)
END_EVENT_TABLE()

MyGLCanvas::MyGLCanvas(int ID, wxNotebook* parent, cInputData inputSettings, const wxGLAttributes attributes, wxSize size, wxPoint position) :
	wxGLCanvas(parent, attributes, ID, position, size, 0, wxT("GLCanvas"), wxNullPalette)  // parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, wxT("GLCanvas")
{
	m_inputSettings = inputSettings;
	m_glContext = new wxGLContext(this);
	this->SetCurrent(*m_glContext);
	this->SetBackgroundStyle(wxBG_STYLE_CUSTOM);
	m_windowHeight = size.y;
	m_windowWidth = size.x;
	this->WarpPointer(m_windowWidth / 2, m_windowHeight / 2); // places mouse in the middle of screen
	m_fpsCamera = new FPSCamera(glm::vec3(0.0f, 1.0f, 0.0f));
	m_fpsCamera->move(m_fpsCamera->getLook());
	m_renderTimer = new RenderTimer(this);
	m_renderTimer->Start();
	m_planePosition = m_phantomPosition;
	init();
}


MyGLCanvas::~MyGLCanvas()
{
	if (m_xRayMachine != nullptr) { delete m_xRayMachine; }
	if (m_gantryRing != nullptr) { delete m_gantryRing; }
	delete m_pantomMesh;
	delete m_positionArrows;
	delete m_rotationArrows;
	delete m_renderTimer;
	delete m_glContext;
	delete m_fpsCamera;
	delete m_plane;
	delete m_ray;
}

int MyGLCanvas::init()
{
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		std::cerr << "Glew initialization failed " << std::endl;
		return -1;
	}
	glEnable(GL_MULTISAMPLE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_STENCIL_TEST);
	glViewport(0, 0, m_windowWidth, m_windowHeight);
	gluPerspective(m_fpsCamera->getFOV(), m_windowWidth / m_windowHeight, 0.1f, 100.0f);
	loadXRayMachine(m_inputSettings);
	m_ray = new Ray();
	m_ray->setProjectionMatrix(m_fpsCamera->getProjectionMatrix(m_windowWidth, m_windowHeight));
	m_plane = new Plane(m_planePosition, m_axesAlignment);
	m_positionArrows = new PositionArrow();
	m_rotationArrows = new RotationArrow();
	loadPhantom(m_inputSettings);
	initialPhantomRotationMatrix = glm::rotate(initialPhantomRotationMatrix,  90 * glm::pi<float>() / 180.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	initialPhantomRotationMatrix = glm::rotate(initialPhantomRotationMatrix, -90 * glm::pi<float>() / 180.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	// initialPhantomRotationMatrix = glm::rotate(initialPhantomRotationMatrix, 90 * glm::pi<float>() / 180.0f, glm::vec3(0.0f, 0.0f, 1.0f));

	return 0;
}

void MyGLCanvas::paintEvent(wxPaintEvent& event)
{
	wxPaintDC dc(this);
	draw(dc);
	event.Skip();
}

void MyGLCanvas::paintNow()
{
	wxClientDC dc(this);
	draw(dc);
}

void MyGLCanvas::updateInputSettings(cInputData inputSettings)
{
	if ((m_inputSettings.phantomFilename != inputSettings.phantomFilename) || inputSettings.organStatusChnaged)
	{
		delete m_pantomMesh;
		loadPhantom(inputSettings);
	}

	if (m_inputSettings.systemType != inputSettings.systemType || m_inputSettings.radiusCt != inputSettings.radiusCt)
	{
		if (m_xRayMachine != nullptr) 
		{ 
			delete m_xRayMachine;
			m_xRayMachine = nullptr;
		}
		
		if (m_gantryRing != nullptr) 
		{ 
			delete m_gantryRing;
			m_gantryRing = nullptr;
		};

		loadXRayMachine(inputSettings);
	}
	
	m_inputSettings = inputSettings;
	
	if (m_inputSettings.systemType == m_inputSettings.ct)
	{
		calculateLocalRotationAxes();
	}
}

void MyGLCanvas::loadPhantom(cInputData inputSettings)
{
	m_pantomMesh = new PhantomMesh(inputSettings);
	m_pantomMesh->loadOBJ();
}

void MyGLCanvas::calculateLocalRotationAxes()
{
	rotationAxisHolder = rotationAxis;
	rotationAxisHolder = glm::rotate(rotationAxisHolder, (float)m_inputSettings.gantryRot[0], negativeZ);
	rotationAxisHolder = glm::rotate(rotationAxisHolder, (float)m_inputSettings.gantryRot[1], negativeX);
	rotationAxisHolder = glm::rotate(rotationAxisHolder, (float)m_inputSettings.gantryRot[2], positiveY);
}

void MyGLCanvas::rotateLocalRotationAxes(glm::vec3 axis, GLfloat currentPosition, GLfloat lastPosition)
{
	rotationAxis = glm::rotate(rotationAxis, -(currentPosition - lastPosition) * glm::pi<float>() / 180.0f, axis);
	negativeX = glm::rotate(negativeX, -(currentPosition - lastPosition) * glm::pi<float>() / 180.0f, axis);
	positiveY = glm::rotate(positiveY, -(currentPosition - lastPosition) * glm::pi<float>() / 180.0f, axis);
	negativeZ = glm::rotate(negativeZ, -(currentPosition - lastPosition) * glm::pi<float>() / 180.0f, axis);
	rotationAxisHolder = rotationAxis;
}

void MyGLCanvas::loadXRayMachine(cInputData inputSettings)
{
	if (inputSettings.systemType == inputSettings.ct)
	{
		m_xRayMachine = nullptr;
		m_gantryRing = new GantryRing(inputSettings.radiusCt);
	}
	else
	{
		m_gantryRing = nullptr;
		m_xRayMachine = new XRayMachine(0.2f);
	}
}

void MyGLCanvas::onLeftMouseDown(wxMouseEvent& event)
{
	rightMouseClicked = false;
	m_lastModelPosition = getModelWorldPosition();
	m_xLastPosition = wxGetMousePosition().x;
	m_yLastPosition = wxGetMousePosition().y;
	glReadPixels(wxGetMousePosition().x - this->GetScreenPosition().x, m_windowHeight - (wxGetMousePosition().y - this->GetScreenPosition().y), 1, 1, GL_STENCIL_INDEX, GL_UNSIGNED_BYTE, &m_index);
	// glReadPixels(wxGetMousePosition().x - this->GetScreenPosition().x, m_windowHeight - this->GetScreenPosition().y - wxGetMousePosition().y, 1, 1, GL_STENCIL_INDEX, GL_UNSIGNED_BYTE, &m_index);

	if (m_index == 0)
	{
		leftMouseClicked = false;
		phantomIsSelected = false;
		positionAxisIsSelected = false;
	}
	else if (m_index == 1)
	{ 
		phantomIsSelected = true; 
		leftMouseClicked = true;
	}
	
	if (m_index == 2) // Z
	{
		leftMouseClicked = true;
		positionAxisIsSelected = true;
		m_planePosition = m_lastModelPosition;
		if (m_lastModelPosition.y >= (m_fpsCamera->getPosition().y - 0.5) && m_lastModelPosition.y <= (m_fpsCamera->getPosition().y + 0.5))
		{
			m_axesAlignment = Plane::Axis::YZ;
			m_planeNormal = glm::vec3(1.0, 0.0, 0.0);
		}
		else
		{
			m_axesAlignment = Plane::Axis::XZ;
			m_planeNormal = glm::vec3(0.0, 1.0, 0.0);
		}
	}
	else if (m_index == 3) // Y
	{
		leftMouseClicked = true;
		positionAxisIsSelected = true;
		m_planePosition = m_lastModelPosition;
		if (m_lastModelPosition.z >= (m_fpsCamera->getPosition().z - 0.5) && m_lastModelPosition.z <= (m_fpsCamera->getPosition().z + 0.5))
		{
			m_axesAlignment = Plane::Axis::YZ;
			m_planeNormal = glm::vec3(1.0, 0.0, 0.0);
		}
		else
		{
			m_axesAlignment = Plane::Axis::XY;
			m_planeNormal = glm::vec3(0.0, 0.0, 1.0);
		}
	}
	else if (m_index == 4) // X
	{
		m_axesAlignment = Plane::Axis::XY;
		leftMouseClicked = true;
		positionAxisIsSelected = true;
		m_planePosition = m_lastModelPosition;
		m_planeNormal = glm::vec3(0.0, 0.0, 1.0);
	}
	Bind(wxEVT_MOTION, &MyGLCanvas::onMouseMove, this, wxID_ANY);
	event.Skip();
}

void MyGLCanvas::onLeftMouseUp(wxMouseEvent& event)
{
	positionAxisIsSelected = false;
	m_axesAlignment = Plane::Axis::XZ;
	Unbind(wxEVT_MOTION, &MyGLCanvas::onMouseMove, this, wxID_ANY);
	event.Skip();
}

void MyGLCanvas::onRightMouseDown(wxMouseEvent& event)
{
	m_xLastPosition = wxGetMousePosition().x;
	m_yLastPosition = wxGetMousePosition().y;
	leftMouseClicked = false;
	glReadPixels(wxGetMousePosition().x - this->GetScreenPosition().x, m_windowHeight - (wxGetMousePosition().y - this->GetScreenPosition().y), 1, 1, GL_STENCIL_INDEX, GL_UNSIGNED_BYTE, &m_index);

	if (m_index == 0)
	{
		rightMouseClicked = false;
		phantomIsSelected = false;
	}
	else if (m_index == 1)
	{
		phantomIsSelected = true;
		rightMouseClicked = true;
	}

	if (m_index >= 4 && m_index <= 6)
	{
		rightMouseClicked = true;
	}

	Bind(wxEVT_MOTION, &MyGLCanvas::onMouseMove, this, wxID_ANY);
	event.Skip();
}

void MyGLCanvas::onRightMouseUp(wxMouseEvent& event)
{
	Unbind(wxEVT_MOTION, &MyGLCanvas::onMouseMove, this, wxID_ANY);
	event.Skip();
}

void MyGLCanvas::onMouseMove(wxMouseEvent& event)
{
	if (event.Entering())
	{
		SetFocus();
	}

	switch (m_index)
	{
	case 0: // 0 empty space
	{
		phantomIsSelected = false;
		GLdouble dxMouse, dyMouse;

		dxMouse = m_xLastPosition - (GLdouble)wxGetMousePosition().x;
		dyMouse = m_yLastPosition - (GLdouble)wxGetMousePosition().y;

		m_xLastPosition = wxGetMousePosition().x;
		m_yLastPosition = wxGetMousePosition().y;

		// wxMessageOutputDebug().Printf("%i x position", wxGetMousePosition().x);
		// wxMessageOutputDebug().Printf("%i y position", wxGetMousePosition().y);

		m_fpsCamera->rotate((GLfloat)dxMouse * MOUSE_SENSITIVITY, (GLfloat)dyMouse * MOUSE_SENSITIVITY);
		break;
	}
	case 1:
	{
		phantomIsSelected;
		break;
	}
	default: // phantom or xRayMachine is selected
		switch (m_index)
		{
		case 2: // Z position axis
		{
			GLfloat mouseX = (2.0f * ((GLfloat)(wxGetMousePosition().x - this->GetScreenPosition().x) / m_windowWidth)) - 1.0f;
			GLfloat mouseY = (2.0f * ((GLfloat)(m_windowHeight - (wxGetMousePosition().y - this->GetScreenPosition().y)) / m_windowHeight)) - 1.0f;
			m_ray->update(glm::vec2(mouseX, mouseY), m_planePosition, m_planeNormal, m_fpsCamera->getViewMatrix(), m_fpsCamera->getPosition());
			m_ray->create();

			if (m_ray->intersects())
			{
				glm::vec3 intersection = m_ray->getIntersectionPoint();
				m_phantomPosition.z += (intersection.z - m_lastModelPosition.z);
				m_xRayMachinePosition.z += (intersection.z - m_lastModelPosition.z);

				if (m_axesAlignment == Plane::Axis::YZ)
				{
					m_planePosition.y = intersection.y;
					m_planePosition.z = intersection.z;
				}
				else
				{
					m_planePosition.x = intersection.x;
					m_planePosition.z = intersection.z;
				}
				m_lastModelPosition.z = intersection.z;
			}

			break;
		}
		case 3: // Y position axis
		{
			GLfloat mouseX = (2.0f * ((GLfloat)(wxGetMousePosition().x - this->GetScreenPosition().x) / m_windowWidth)) - 1.0f;
			GLfloat mouseY = (2.0f * ((GLfloat)(m_windowHeight - (wxGetMousePosition().y - this->GetScreenPosition().y)) / m_windowHeight)) - 1.0f;
			m_ray->update(glm::vec2(mouseX, mouseY), m_planePosition, m_planeNormal, m_fpsCamera->getViewMatrix(), m_fpsCamera->getPosition());
			m_ray->create();

			if (m_ray->intersects())
			{
				glm::vec3 intersection = m_ray->getIntersectionPoint();
				m_phantomPosition.y += (intersection.y - m_lastModelPosition.y);
				m_xRayMachinePosition.y += (intersection.y - m_lastModelPosition.y);

				if (m_axesAlignment == Plane::Axis::YZ)
				{
					m_planePosition.y = intersection.y;
					m_planePosition.z = intersection.z;
				}
				else
				{
					m_planePosition.y = intersection.y;
					m_planePosition.x = intersection.x;
				}
				m_lastModelPosition.y = intersection.y;
			}
			break;
		}
		case 4: // X position axis
		{
			GLfloat mouseX = (2.0f * ((GLfloat)(wxGetMousePosition().x - this->GetScreenPosition().x) / m_windowWidth)) - 1.0f;
			GLfloat mouseY = (2.0f * ((GLfloat)(m_windowHeight - (wxGetMousePosition().y - this->GetScreenPosition().y)) / m_windowHeight)) - 1.0f;
			m_ray->update(glm::vec2(mouseX, mouseY), m_planePosition, m_planeNormal, m_fpsCamera->getViewMatrix(), m_fpsCamera->getPosition());
			m_ray->create();

			if (m_ray->intersects())
			{
				glm::vec3 intersection = m_ray->getIntersectionPoint();
				m_phantomPosition.x += (intersection.x - m_lastModelPosition.x);
				m_xRayMachinePosition.x += (intersection.x - m_lastModelPosition.x);

				m_planePosition.x = intersection.x;
				m_planePosition.y = intersection.y;
				m_lastModelPosition.x = intersection.x;
			}
			break;
		}
		case 5: // X rotation axis
		{
			GLfloat xCurrentPosition = wxGetMousePosition().x;
			GLfloat yCurrentPosition = wxGetMousePosition().y;

			rotateLocalRotationAxes(glm::vec3(1.0f, 0.0f, 0.0f), xCurrentPosition, m_xLastPosition);
			calculateLocalRotationAxes();
			m_xRayMachineRotationAngle.x += (xCurrentPosition - m_xLastPosition);
			xRayBeamRotationMatrix *= glm::translate(glm::mat4(1), -(m_phantomPosition - m_phantomPosition)) *
				glm::rotate(glm::mat4(1), (xCurrentPosition - m_xLastPosition) *
					glm::pi<float>() / 180.0f, glm::vec3(1.0f, 0.0f, 0.0f)) *
				glm::translate(glm::mat4(1), m_phantomPosition - m_phantomPosition);
			xRayMachineRotationMatrix *= glm::translate(glm::mat4(1), -(m_phantomPosition - m_phantomPosition)) *
				glm::rotate(glm::mat4(1), (xCurrentPosition - m_xLastPosition) * glm::pi<float>() / 180.0f, glm::vec3(1.0f, 0.0f, 0.0f)) *
				glm::translate(glm::mat4(1), (m_phantomPosition - m_phantomPosition));
			phantomRotationMatrix = glm::rotate(phantomRotationMatrix, (xCurrentPosition - m_xLastPosition) * glm::pi<float>() / 180.0f, glm::vec3(1.0f, 0.0f, 0.0f));

			m_xLastPosition = xCurrentPosition;
			m_yLastPosition = yCurrentPosition;
			break;
		}
		case 6: // Y rotation axis
		{
			GLfloat xCurrentPosition = wxGetMousePosition().x;
			GLfloat yCurrentPosition = wxGetMousePosition().y;

			rotateLocalRotationAxes(glm::vec3(0.0f, 1.0f, 0.0f), yCurrentPosition, m_yLastPosition);
			calculateLocalRotationAxes();
			m_xRayMachineRotationAngle.y += (yCurrentPosition - m_yLastPosition);
			xRayBeamRotationMatrix *= glm::translate(glm::mat4(1), -(m_phantomPosition - m_phantomPosition)) *
				glm::rotate(glm::mat4(1), (yCurrentPosition - m_yLastPosition) * glm::pi<float>() / 180.0f, glm::vec3(0.0f, 1.0f, 0.0f)) *
				glm::translate(glm::mat4(1), m_phantomPosition - m_phantomPosition);
			xRayMachineRotationMatrix *= glm::translate(glm::mat4(1), -(m_phantomPosition - m_phantomPosition)) *
				glm::rotate(glm::mat4(1), (yCurrentPosition - m_yLastPosition) * glm::pi<float>() / 180.0f, glm::vec3(0.0f, 1.0f, 0.0f)) *
				glm::translate(glm::mat4(1), (m_phantomPosition - m_phantomPosition));
			phantomRotationMatrix = glm::rotate(phantomRotationMatrix, (yCurrentPosition - m_yLastPosition) * glm::pi<float>() / 180.0f, glm::vec3(0.0f, 1.0f, 0.0f));

			m_xLastPosition = xCurrentPosition;
			m_yLastPosition = yCurrentPosition;
			break;
		}
		case 7: // Z rotation axis
		{
			GLfloat xCurrentPosition = wxGetMousePosition().x;
			GLfloat yCurrentPosition = wxGetMousePosition().y;

			rotateLocalRotationAxes(glm::vec3(0.0f, 0.0f, 1.0f), xCurrentPosition, m_xLastPosition);
			calculateLocalRotationAxes();
			rotationAxisHolder = rotationAxis;
			m_xRayMachineRotationAngle.z += (xCurrentPosition - m_xLastPosition);
			xRayBeamRotationMatrix *= glm::translate(glm::mat4(1), -(m_phantomPosition - m_phantomPosition)) *
				glm::rotate(glm::mat4(1), (xCurrentPosition - m_xLastPosition) * glm::pi<float>() / 180.0f, glm::vec3(0.0f, 0.0f, 1.0f)) *
				glm::translate(glm::mat4(1), m_phantomPosition - m_phantomPosition);
			xRayMachineRotationMatrix *= glm::translate(glm::mat4(1), -(m_phantomPosition - m_phantomPosition)) *
				glm::rotate(glm::mat4(1), (xCurrentPosition - m_xLastPosition) * glm::pi<float>() / 180.0f, glm::vec3(0.0f, 0.0f, 1.0f)) *
				glm::translate(glm::mat4(1), (m_phantomPosition - m_phantomPosition));
			phantomRotationMatrix = glm::rotate(phantomRotationMatrix, (xCurrentPosition - m_xLastPosition) * glm::pi<float>() / 180.0f, glm::vec3(0.0f, 0.0f, 1.0f));

			m_xLastPosition = xCurrentPosition;
			m_yLastPosition = yCurrentPosition;
			break;
		}
		default:
			break;
		}
		break;
	}

	event.Skip();
}

glm::vec3 MyGLCanvas::getModelWorldPosition()
{
	GLfloat depth;

	glReadPixels(wxGetMousePosition().x - this->GetScreenPosition().x, m_windowHeight - (wxGetMousePosition().y - this->GetScreenPosition().y) - 1, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &depth);

	GLfloat z = depth * 2.0 - 1.0;
	GLfloat testX = (2.0f * ((GLfloat)(wxGetMousePosition().x - this->GetScreenPosition().x) / m_windowWidth)) - 1.0f;
	GLfloat testY = (2.0f * ((GLfloat)(m_windowHeight - (wxGetMousePosition().y - this->GetScreenPosition().y)) / m_windowHeight)) - 1.0f;
	glm::vec4 clipSpacePosition = glm::vec4(testX, testY, z, 1.0);
	glm::vec4 viewSpacePosition = glm::inverse(m_fpsCamera->getProjectionMatrix(m_windowWidth, m_windowHeight)) * clipSpacePosition;
	viewSpacePosition /= viewSpacePosition.w;
	glm::vec4 worldSpacePosition = glm::inverse(m_fpsCamera->getViewMatrix()) * viewSpacePosition;

	return glm::vec3(worldSpacePosition.x, worldSpacePosition.y, worldSpacePosition.z);
}

void MyGLCanvas::onKeyPressed(wxKeyEvent& event)
{

	switch (event.GetKeyCode())
	{
	case (119):                     // lower case w
	{
		m_fpsCamera->move(m_mouseSpeed * m_fpsCamera->getLook());
		m_mouseSpeed += 0.1f;
		break;
	}
	case (97):						// lower case a
	{
		m_fpsCamera->move(m_mouseSpeed * -m_fpsCamera->getRight());
		m_mouseSpeed += 0.1f;
		break;
	}
	case (115):                     // lower case s
	{
		m_fpsCamera->move(m_mouseSpeed * -m_fpsCamera->getLook());
		m_mouseSpeed += 0.1f;
		break;
	}
	case (100):                     // lower case d
	{
		m_fpsCamera->move(m_mouseSpeed * m_fpsCamera->getRight());
		m_mouseSpeed += 0.1f;
		break;
	}
	case (120):						// lower case x
	{
		m_fpsCamera->move(m_mouseSpeed * -m_fpsCamera->getUp());
		m_mouseSpeed += 0.1f;
		break;
	}
	case (121):						// lower case y
	{
		m_fpsCamera->move(m_mouseSpeed * m_fpsCamera->getUp());
		m_mouseSpeed += 0.1f;
		break;
	}
	default:
		break;
	}
}

void MyGLCanvas::onKeyUp(wxKeyEvent& event)
{
	m_mouseSpeed = 0.1;
}

void MyGLCanvas::onMouseWheel(wxMouseEvent& event)
{
	GLdouble fov;

	if (event.GetWheelRotation() > 0)
	{
		fov = m_fpsCamera->getFOV() + ZOOM_SENSITIVITY;
	}
	else
	{
		fov = m_fpsCamera->getFOV() - ZOOM_SENSITIVITY;
	}

	fov = glm::clamp(fov, 1.0, 120.0);

	m_fpsCamera->setFOV((GLfloat)fov);
	event.Skip();
}

void MyGLCanvas::draw(wxDC& dc)
{
	wxGLCanvas::SetCurrent(*m_glContext);
	glEnable(GL_MULTISAMPLE);
	glViewport(0, 0, m_windowWidth, m_windowHeight);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_STENCIL_TEST);
	glClearDepth(1.0f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthFunc(GL_LEQUAL);

	m_rotationAngleAroundPhantom++;
	glm::mat4 phantomModel, view, projection;
	phantomModel = glm::translate(phantomModel, m_phantomPosition);
	view = m_fpsCamera->getViewMatrix();
	projection = m_fpsCamera->getProjectionMatrix(m_windowWidth, m_windowHeight);
	m_color = glm::vec4(0.310f, 0.747f, 0.185f, 1.0f);
	glm::vec3 lightPos = glm::vec3(0.0f, 1.0f, 0.0f);
	glm::vec3 lightColor = glm::vec3(1.0f, 1.0f, 1.0f);

	glm::vec3 phantomCenterCoor = m_pantomMesh->getCenterCoordinates();

	ShaderProgram shaderProgram;
	shaderProgram.loadShaders("./Shaders/phantom.vert", "./Shaders/phantom.frag");

	glClearStencil(0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	shaderProgram.use();
	shaderProgram.setUniform("initialRotationMatrix", initialPhantomRotationMatrix);
	shaderProgram.setUniform("phantomRotationMatrix", phantomRotationMatrix);
	shaderProgram.setUniform("phantomCenterCoor", phantomCenterCoor);
	shaderProgram.setUniform("model", phantomModel);
	shaderProgram.setUniform("view", view);
	shaderProgram.setUniform("projection", projection);
	shaderProgram.setUniform("color", m_color);
	shaderProgram.setUniform("lightColor", lightColor);
	shaderProgram.setUniform("lightPos", m_fpsCamera->getPosition());
	shaderProgram.setUniform("cameraPos", m_fpsCamera->getPosition());

	glStencilMask(0xFF); // Write to stencil buffer
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
	glStencilFunc(GL_ALWAYS, 0, 0xFF);  // Set any stencil to 0

	glStencilFunc(GL_ALWAYS, 1, 0xFF); // Set any stencil to object ID
	m_pantomMesh->draw();
	glStencilFunc(GL_ALWAYS, 0, 0xFF);  // Set any stencil to 0
	shaderProgram.deleteProgram();

	if (phantomIsSelected && leftMouseClicked)
	{
		drawMovementAxes(phantomModel, view, projection);
	}

	if (phantomIsSelected && rightMouseClicked)
	{
		drawRotationAxes(phantomModel, view, projection);
	}

	glm::mat4 xRayMachineModel, phantomOriginCoorRotationMatrix;
	glm::vec3 m_phantomOriginCoor;
	m_phantomOriginCoor = m_pantomMesh->getLeftBackBottomCornerCoor();
	m_phantomOriginCoor = m_phantomOriginCoor - m_pantomMesh->getCenterCoordinates();
	phantomOriginCoorRotationMatrix = glm::rotate(phantomOriginCoorRotationMatrix, 90 * glm::pi<float>() / 180.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	phantomOriginCoorRotationMatrix = glm::rotate(phantomOriginCoorRotationMatrix, -90 * glm::pi<float>() / 180.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	
	if (m_inputSettings.systemType == m_inputSettings.rad)
	{
		m_phantomOriginCoor += glm::vec3(m_inputSettings.tubePos[0],
			m_inputSettings.tubePos[1],
			m_inputSettings.tubePos[2]);
		m_phantomOriginCoor = glm::vec3(phantomModel * glm::inverse(phantomOriginCoorRotationMatrix * phantomRotationMatrix) * glm::vec4(m_phantomOriginCoor, 1.0));
		m_xRayMachinePosition = m_phantomOriginCoor;
		xRayMachineModel = glm::translate(xRayMachineModel, m_xRayMachinePosition);
	}
	else
	{
		m_phantomOriginCoor += glm::vec3(m_inputSettings.gantryPos[0],
			m_inputSettings.gantryPos[1],
			m_inputSettings.gantryPos[2]);
		m_xRayMachinePosition = glm::vec3(m_phantomOriginCoor.x, m_phantomOriginCoor.y + m_inputSettings.radiusCt, m_phantomOriginCoor.z);
		m_phantomOriginCoor = glm::vec3(phantomModel * glm::inverse(phantomOriginCoorRotationMatrix * phantomRotationMatrix) * glm::vec4(m_phantomOriginCoor, 1.0));
		m_xRayMachinePosition = glm::vec3(phantomModel * glm::inverse(phantomOriginCoorRotationMatrix * phantomRotationMatrix) * glm::vec4(m_xRayMachinePosition, 1.0));
		xRayMachineModel = glm::translate(xRayMachineModel, m_xRayMachinePosition);
	}

	m_color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	glm::mat4 initialXRayMachineRotationMatrix;

	if (m_inputSettings.systemType == m_inputSettings.rad)
	{
		initialXRayMachineRotationMatrix = initRotationMatrixRAD(initialXRayMachineRotationMatrix);
	}
	else
	{
		initialXRayMachineRotationMatrix = initRotationMatrixCT(initialXRayMachineRotationMatrix);
	}

	shaderProgram.loadShaders("./Shaders/xRayMachine.vert", "./Shaders/xRayMachine.frag");
	shaderProgram.use();
	shaderProgram.setUniform("rotationMatrix", xRayMachineRotationMatrix);
	shaderProgram.setUniform("initialRotationMatrix", initialXRayMachineRotationMatrix);
	shaderProgram.setUniform("model", xRayMachineModel);
	shaderProgram.setUniform("view", view);
	shaderProgram.setUniform("projection", projection);
	shaderProgram.setUniform("color", m_color);

	if (m_inputSettings.systemType == m_inputSettings.ct)
	{
		glm::mat4 xRayMachineRotaionMatrixAroundPhantom;
		xRayMachineRotaionMatrixAroundPhantom = glm::translate(glm::mat4(1), -(m_xRayMachinePosition - m_phantomOriginCoor)) *
			glm::rotate(xRayMachineRotaionMatrixAroundPhantom, m_rotationAngleAroundPhantom * glm::pi<float>() / 180.0f, rotationAxisHolder) *
			glm::translate(glm::mat4(1), (m_xRayMachinePosition - m_phantomOriginCoor));
		shaderProgram.setUniform("rotaionMatrixAroundPhantom", xRayMachineRotaionMatrixAroundPhantom);
	}

	glStencilFunc(GL_ALWAYS, 0, 0xFF); // Set any stencil to object ID
	if (m_inputSettings.systemType == m_inputSettings.rad)
	{
		m_xRayMachine->draw();
	}
	else
	{
		m_gantryRing->draw();
	}
	glStencilFunc(GL_ALWAYS, 0, 0xFF);  // Set any stencil to 0

	shaderProgram.deleteProgram();

	glDisable(GL_STENCIL_TEST);
	m_color = glm::vec4(1.0f, 1.0f, 1.0f, 0.5f); // transparent

	glm::mat4 xRayBeamModel;
	xRayBeamModel = glm::translate(xRayBeamModel, m_xRayMachinePosition);

	shaderProgram.loadShaders("./Shaders/xRayBeam.vert", "./Shaders/xRayBeam.frag");
	shaderProgram.use();
	shaderProgram.setUniform("initialRotationMatrix", initialXRayMachineRotationMatrix);
	shaderProgram.setUniform("rotationMatrix", xRayBeamRotationMatrix);
	shaderProgram.setUniform("model", xRayBeamModel);
	shaderProgram.setUniform("view", view);
	shaderProgram.setUniform("projection", projection);
	shaderProgram.setUniform("color", m_color);

	if (m_inputSettings.systemType == m_inputSettings.rad)
	{
		drawRADBeam(m_phantomOriginCoor);
	}
	else
	{
		drawCTBeam(m_phantomOriginCoor, shaderProgram);
	}

	shaderProgram.deleteProgram();
	glEnable(GL_STENCIL_TEST);

	if (positionAxisIsSelected)
	{
		drawPlane(view, projection, shaderProgram);
	}

	glFlush();
	wxGLCanvas::SwapBuffers();
}

glm::mat4 MyGLCanvas::initRotationMatrixCT(glm::mat4 initialXRayMachineRotationMatrix)
{
	initialXRayMachineRotationMatrix *= glm::translate(glm::mat4(1), -(m_xRayMachinePosition - glm::vec3(m_xRayMachinePosition.x + m_inputSettings.radiusCt, m_xRayMachinePosition.y, m_xRayMachinePosition.z))) *
		glm::rotate(glm::mat4(1), (float)m_inputSettings.gantryRot[0], glm::vec3(0.0f, 0.0f, 1.0f)) *
		glm::translate(glm::mat4(1), (m_xRayMachinePosition - glm::vec3(m_xRayMachinePosition.x + m_inputSettings.radiusCt, m_xRayMachinePosition.y, m_xRayMachinePosition.z)));
	initialXRayMachineRotationMatrix *= glm::translate(glm::mat4(1), -(m_xRayMachinePosition - glm::vec3(m_xRayMachinePosition.x + m_inputSettings.radiusCt, m_xRayMachinePosition.y, m_xRayMachinePosition.z))) *
		glm::rotate(glm::mat4(1), (float)m_inputSettings.gantryRot[1], glm::vec3(1.0f, 0.0f, 0.0f)) *
		glm::translate(glm::mat4(1), (m_xRayMachinePosition - glm::vec3(m_xRayMachinePosition.x + m_inputSettings.radiusCt, m_xRayMachinePosition.y, m_xRayMachinePosition.z)));
	initialXRayMachineRotationMatrix *= glm::translate(glm::mat4(1), -(m_xRayMachinePosition - glm::vec3(m_xRayMachinePosition.x + m_inputSettings.radiusCt, m_xRayMachinePosition.y, m_xRayMachinePosition.z))) *
		glm::rotate(glm::mat4(1), (float)m_inputSettings.gantryRot[2], glm::vec3(0.0f, -1.0f, 0.0f)) *
		glm::translate(glm::mat4(1), (m_xRayMachinePosition - glm::vec3(m_xRayMachinePosition.x + m_inputSettings.radiusCt, m_xRayMachinePosition.y, m_xRayMachinePosition.z)));
	
	return initialXRayMachineRotationMatrix;
}

glm::mat4 MyGLCanvas::initRotationMatrixRAD(glm::mat4 initialXRayMachineRotationMatrix)
{
	initialXRayMachineRotationMatrix = glm::rotate(initialXRayMachineRotationMatrix,
		(float)m_inputSettings.tubeRot[0],
		glm::vec3(0.0f, 0.0f, 1.0f));
	initialXRayMachineRotationMatrix = glm::rotate(initialXRayMachineRotationMatrix,
		(float)m_inputSettings.tubeRot[1],
		glm::vec3(1.0f, 0.0f, 0.0f));
	initialXRayMachineRotationMatrix = glm::rotate(initialXRayMachineRotationMatrix,
		(float)m_inputSettings.tubeRot[2],
		glm::vec3(0.0f, -1.0f, 0.0f));

	return initialXRayMachineRotationMatrix;
}

void MyGLCanvas::drawPlane(glm::mat4 view, glm::mat4 projection, ShaderProgram shaderProgram)
{
	m_plane->setPosition(m_planePosition);
	m_plane->setAxesAlignment(m_axesAlignment);
	m_color = glm::vec4(0.23f, 0.24f, 0.0f, 0.0f); // transparent
	glm::mat4 planeModel = glm::mat4(1.0);
	shaderProgram.loadShaders("./Shaders/arrow.vert", "./Shaders/arrow.frag");
	shaderProgram.use();
	shaderProgram.setUniform("model", planeModel);
	shaderProgram.setUniform("view", view);
	shaderProgram.setUniform("projection", projection);
	shaderProgram.setUniform("color", m_color);
	glStencilFunc(GL_ALWAYS, 0, 0xFF);
	m_plane->draw(m_fpsCamera->getCameraDirection()); // planeNormal returns correct result
	glStencilFunc(GL_ALWAYS, 0, 0xFF);
	shaderProgram.deleteProgram();
}

void MyGLCanvas::drawCTBeam(glm::vec3 phantomOriginCoor, ShaderProgram shaderProgram) {
	glm::mat4 xRayBeamRotaionMatrixAroundPhantom;
	xRayBeamRotaionMatrixAroundPhantom = glm::translate(glm::mat4(1), -(m_xRayMachinePosition - phantomOriginCoor)) *
		glm::rotate(xRayBeamRotaionMatrixAroundPhantom, m_rotationAngleAroundPhantom * glm::pi<float>() / 180.0f, rotationAxisHolder) *
		glm::translate(glm::mat4(1), (m_xRayMachinePosition - phantomOriginCoor));
	shaderProgram.setUniform("rotaionMatrixAroundPhantom", xRayBeamRotaionMatrixAroundPhantom);

	CtXRayBeam* xRayBeam = new CtXRayBeam;
	xRayBeam->setFanAttributes(glm::vec3(m_inputSettings.fanAngleWidth * 180 / glm::pi<float>(),
		m_inputSettings.fanAngleThickness * 180 / glm::pi<float>(),
		m_inputSettings.radiusCt));
	xRayBeam->generate();
	xRayBeam->draw();
	delete xRayBeam;
}

void MyGLCanvas::drawRADBeam(glm::vec3 phantomOriginCoor)
{
	RadXRayBeam* xRayBeam = new RadXRayBeam();
	xRayBeam->setBeamEndPosition(m_phantomPosition);
	xRayBeam->setBeamStartPosition(phantomOriginCoor);
	xRayBeam->setCollimatorOpening(glm::vec3(
		m_inputSettings.collOpening[0],
		m_inputSettings.collOpening[1],
		m_inputSettings.collDistance));
	xRayBeam->generate();
	xRayBeam->draw();
	delete xRayBeam;
}

void MyGLCanvas::drawRotationAxes(glm::mat4 model, glm::mat4 view, glm::mat4 projection)
{
	ShaderProgram shaderProgram;

	shaderProgram.loadShaders("./Shaders/arrow.vert", "./Shaders/arrow.frag");
	shaderProgram.use();
	shaderProgram.setUniform("model", model);
	shaderProgram.setUniform("view", view);
	shaderProgram.setUniform("projection", projection);
	shaderProgram.setUniform("color", m_color);

	glStencilFunc(GL_ALWAYS, 5, 0xFF);
	m_color = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f); // red
	shaderProgram.setUniform("color", m_color);
	m_rotationArrows->drawXaxis();
	glStencilFunc(GL_ALWAYS, 0, 0xFF);

	glStencilFunc(GL_ALWAYS, 6, 0xFF);
	m_color = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f); // green
	shaderProgram.setUniform("color", m_color);
	m_rotationArrows->drawYaxis();
	glStencilFunc(GL_ALWAYS, 0, 0xFF);

	glStencilFunc(GL_ALWAYS, 7, 0xFF);
	m_color = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f); // blue
	shaderProgram.setUniform("color", m_color);
	m_rotationArrows->drawZaxis();
	glStencilFunc(GL_ALWAYS, 0, 0xFF);
	shaderProgram.deleteProgram();
}

void MyGLCanvas::drawMovementAxes(glm::mat4 model, glm::mat4 view, glm::mat4 projection)
{
	ShaderProgram shaderProgram;

	shaderProgram.loadShaders("./Shaders/arrow.vert", "./Shaders/arrow.frag");
	shaderProgram.use();
	shaderProgram.setUniform("model", model);
	shaderProgram.setUniform("view", view);
	shaderProgram.setUniform("projection", projection);
	shaderProgram.setUniform("color", m_color);


	m_color = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f); // blue
	shaderProgram.setUniform("color", m_color);
	glStencilFunc(GL_ALWAYS, 2, 0xFF);
	m_positionArrows->drawZaxis();
	glStencilFunc(GL_ALWAYS, 0, 0xFF);

	m_color = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f); // green
	shaderProgram.setUniform("color", m_color);
	glStencilFunc(GL_ALWAYS, 3, 0xFF);
	m_positionArrows->drawYaxis();
	glStencilFunc(GL_ALWAYS, 0, 0xFF);

	m_color = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f); // red
	shaderProgram.setUniform("color", m_color);
	glStencilFunc(GL_ALWAYS, 4, 0xFF);
	m_positionArrows->drawXaxis();
	glStencilFunc(GL_ALWAYS, 0, 0xFF);
	shaderProgram.deleteProgram();
}

void MyGLCanvas::onResize(wxSizeEvent& event) {
	m_windowWidth = event.GetSize().x;
	m_windowHeight = event.GetSize().y;
	glViewport(0, 0, m_windowWidth, m_windowHeight);
	gluPerspective(m_fpsCamera->getFOV(), m_windowWidth / m_windowHeight, 0.1f, 100.0f);
	m_ray->setProjectionMatrix(m_fpsCamera->getProjectionMatrix(m_windowWidth, m_windowHeight));

	event.Skip();
}

RenderTimer::RenderTimer(MyGLCanvas* canvas) : wxTimer()
{
	RenderTimer::m_canvas = canvas;
}

void RenderTimer::Notify()
{
	m_canvas->Refresh(false);
};

void RenderTimer::Start()
{
	wxTimer::Start();
};