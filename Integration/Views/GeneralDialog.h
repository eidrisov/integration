#ifndef FIRSTDIALOG_H_INCLUDED
#define FIRSTDIALOG_H_INCLUDED

#include <wx/wx.h>
#include <wx/frame.h>
#include <wx/combobox.h>
#include <wx/event.h>
#include <string>
#include <iostream>
#include <filesystem>
#include <wx/button.h>
#include <wx/radiobut.h>
#include <wx/textctrl.h>
#include <wx/stattext.h>
#include <wx/valnum.h>
#include <wx/sizer.h>
#include <ctime>
#include "../Physics/spectrum.h"
#include "../Controllers/Presenter.h"
#include "../Helpers/Constants.h"
#include "../Physics/inputData.h"

class Presenter;

class GeneralDialog: public wxFrame
{
private:
	wxComboBox* spectrumFileName = nullptr, *phantomFileName = nullptr, *tubeVoltage = nullptr;;
	wxString* spectrumChoices = nullptr, *phantomChoices = nullptr;
	wxControl* control = nullptr;
	cSpectrum* spectrum = nullptr;
	wxButton *okButton = nullptr, *cancelButton = nullptr;
	wxRadioButton* radRB = nullptr, *ctRB = nullptr, *randomSeedRB[3];
	wxTextCtrl* randomSeed = nullptr, * noOfPhontons = nullptr, *currentTimeProduct = nullptr;
	wxStaticText* randomSeedStaticText = nullptr, *noOfPhontonsStaticText = nullptr, * currentTimeProductStaticText = nullptr,
		*tubeVoltageStatictext = nullptr , *phantomStaticText = nullptr, *spectrumStaticText = nullptr;
	int staticTextWidth = 60, staticTextHeight = 50, comboBoxHeight = 40, textCtrlWidth = 140, textCtrlHeight = 23, 
		buttonWidth = 75, buttonHeight = 22;
	Presenter* presenter = nullptr;
	wxFloatingPointValidator<double>* doubleValidator;
	wxIntegerValidator<long long>* uint64Validator;
	wxIntegerValidator<uint32_t>* uint32Validator;
	wxSizer* topSizerHor = nullptr, *topSizerVert = nullptr, *controlVerSizer = nullptr,
		*horSizer_1 = nullptr, *horSizer_2 = nullptr, *horSizer_3 = nullptr, *horSizer_4 = nullptr,
		*horSizer_5 = nullptr, *horSizer_6 = nullptr, * horSizer_7 = nullptr, *randomSeedVertSizer = nullptr;

public:
	GeneralDialog(Presenter* presenter);
	~GeneralDialog();
	void maxLengthEvent(wxCommandEvent& event);
	wxString* getListOfFilesInDirectory(std::string path, int* size);
	wxString getFileName(wxString path);
	void updateVoltageValues(wxCommandEvent& event);
	void okFirstDialog(wxCommandEvent& event);
	void cancelFirstDialog(wxCommandEvent& event);
	void onRandomSeedSelected(wxCommandEvent& event);
	void onClose(wxCloseEvent& event);
	void readInputSettings();

	enum {
		ID_SPECTRUM_FILE_NAME = wxID_HIGHEST + 1
	};

	DECLARE_EVENT_TABLE()
};

#endif // FIRSTDIALOG_H_INCLUDED