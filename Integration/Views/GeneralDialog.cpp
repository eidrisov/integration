#include "GeneralDialog.h"

namespace fs = std::filesystem;

BEGIN_EVENT_TABLE(GeneralDialog, wxFrame)
EVT_COMMAND(ID_SPECTRUM_FILE_NAME, wxEVT_COMBOBOX, GeneralDialog::updateVoltageValues)
END_EVENT_TABLE() 

GeneralDialog::GeneralDialog(Presenter* presenter) :
	wxFrame(NULL, wxID_ANY, wxT("General Dialog"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE, wxT("General Dialog"))
{
	topSizerVert = new wxBoxSizer(wxVERTICAL);
	topSizerHor = new wxBoxSizer(wxHORIZONTAL);
	controlVerSizer = new wxBoxSizer(wxVERTICAL);
	randomSeedVertSizer = new wxBoxSizer(wxVERTICAL);
	horSizer_1 = new wxBoxSizer(wxHORIZONTAL);
	horSizer_2 = new wxBoxSizer(wxHORIZONTAL);
	horSizer_3 = new wxBoxSizer(wxHORIZONTAL);
	horSizer_4 = new wxBoxSizer(wxHORIZONTAL);
	horSizer_5 = new wxBoxSizer(wxHORIZONTAL);
	horSizer_6 = new wxBoxSizer(wxHORIZONTAL);
	horSizer_7 = new wxBoxSizer(wxHORIZONTAL);

	doubleValidator = new wxFloatingPointValidator<double>(2, NULL, wxNUM_VAL_DEFAULT);
	uint64Validator = new wxIntegerValidator<long long>(NULL, wxNUM_VAL_DEFAULT); // does not work with uint64_t
	uint64Validator->SetMin(0);
	uint32Validator = new wxIntegerValidator<uint32_t>(NULL, wxNUM_VAL_DEFAULT);
	
	this->presenter = presenter;
	spectrum = new cSpectrum();

	int size = 0;
	spectrumChoices = getListOfFilesInDirectory("./Spectrums", &size);
	control = new wxControl(this, wxID_ANY, wxPoint(0, 0), wxSize(this->GetClientSize().x, this->GetClientSize().y));
	spectrumStaticText = new wxStaticText(control, wxID_ANY, wxT("spectrum"), wxDefaultPosition,
		wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	spectrumFileName = new wxComboBox(control, ID_SPECTRUM_FILE_NAME, wxT("select spectrum file"), wxDefaultPosition,
		wxDefaultSize, size, spectrumChoices, wxCB_READONLY, wxDefaultValidator, wxT("Spectrum")); // wxCB_DROPDOWN
	// spectrumFileName->Bind(wxEVT_COMBOBOX, &GeneralDialog::updateVoltageValues, this);

	horSizer_1->Add(spectrumStaticText, 0, wxALL, 5);
	horSizer_1->Add(spectrumFileName, 1, wxALL, 5);

	phantomStaticText = new wxStaticText(control, wxID_ANY, wxT("phantom"), wxDefaultPosition,
		wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	phantomChoices = getListOfFilesInDirectory("./Phantoms", &size);
	phantomFileName = new wxComboBox(control, wxID_ANY, wxT("select phantom file"), wxDefaultPosition,
		wxDefaultSize, size, phantomChoices, wxCB_READONLY, wxDefaultValidator, wxT("Phantom"));

	horSizer_2->Add(phantomStaticText, 0, wxALL, 5);
	horSizer_2->Add(phantomFileName, 1, wxALL, 5);

	randomSeedStaticText = new wxStaticText(control, wxID_ANY, wxT("random \nseed"),
		wxDefaultPosition,
		wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	randomSeedRB[0] = new wxRadioButton(control, wxID_ANY, wxT("default"), wxDefaultPosition, wxDefaultSize, wxRB_GROUP);
	randomSeedRB[0]->SetValue(true);
	randomSeedRB[1] = new wxRadioButton(control, wxID_ANY, wxT("current"), wxDefaultPosition, wxDefaultSize);
	randomSeedRB[2] = new wxRadioButton(control, wxID_ANY, wxT("specified"), wxDefaultPosition, wxDefaultSize);
	randomSeed = new wxTextCtrl(control, wxID_ANY, wxT("0"),
		wxDefaultPosition,
		wxSize(textCtrlWidth, textCtrlHeight), wxTE_CENTRE, *uint32Validator);
	randomSeed->Disable();
	randomSeed->GetEventHandler()->Connect(wxEVT_TEXT_MAXLEN, wxObjectEventFunction(&GeneralDialog::maxLengthEvent));

	randomSeedVertSizer->Add(randomSeedRB[0], 0, wxALL, 5);
	randomSeedVertSizer->Add(randomSeedRB[1], 0, wxALL, 5);
	randomSeedVertSizer->Add(randomSeedRB[2], 0, wxALL, 5);

	horSizer_3->Add(randomSeedStaticText, 0, wxALL, 5);
	horSizer_3->Add(randomSeedVertSizer, 0, wxALL, 5);
	horSizer_3->Add(randomSeed, 0, wxALL, 5);
	randomSeedRB[0]->Bind(wxEVT_COMMAND_RADIOBUTTON_SELECTED, &GeneralDialog::onRandomSeedSelected, this);
	randomSeedRB[1]->Bind(wxEVT_COMMAND_RADIOBUTTON_SELECTED, &GeneralDialog::onRandomSeedSelected, this);
	randomSeedRB[2]->Bind(wxEVT_COMMAND_RADIOBUTTON_SELECTED, &GeneralDialog::onRandomSeedSelected, this);

	noOfPhontonsStaticText = new wxStaticText(control, wxID_ANY, wxT("number \nof \nphotons"), 
		wxDefaultPosition,
		wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	noOfPhontons = new wxTextCtrl(control, wxID_ANY, wxT("100000"), 
		wxDefaultPosition,
		wxSize(textCtrlWidth, textCtrlHeight), wxTE_CENTRE, *uint64Validator);
	noOfPhontons->GetEventHandler()->Connect(wxEVT_TEXT_MAXLEN, wxObjectEventFunction(&GeneralDialog::maxLengthEvent));

	horSizer_4->Add(noOfPhontonsStaticText, 0, wxALL, 5);
	horSizer_4->Add(noOfPhontons, 0, wxALL, 5);

	currentTimeProductStaticText = new wxStaticText(control, wxID_ANY, wxT("current time product"), wxDefaultPosition,
		wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	currentTimeProduct = new wxTextCtrl(control, wxID_ANY, wxT("200.00"), wxDefaultPosition,
		wxSize(textCtrlWidth, textCtrlHeight), wxTE_CENTRE, *doubleValidator);
	currentTimeProduct->GetEventHandler()->Connect(wxEVT_TEXT_MAXLEN, wxObjectEventFunction(&GeneralDialog::maxLengthEvent));
	
	horSizer_5->Add(currentTimeProductStaticText, 0, wxALL, 5);
	horSizer_5->Add(currentTimeProduct, 0, wxALL, 5);

	tubeVoltageStatictext = new wxStaticText(control, wxID_ANY, wxT("tube voltage"), wxDefaultPosition,
		wxSize(staticTextWidth, staticTextHeight), wxALIGN_CENTRE_HORIZONTAL);
	tubeVoltage = new wxComboBox(control, wxID_ANY, wxT("voltage"), wxDefaultPosition,
		wxSize(textCtrlWidth, comboBoxHeight),
		0, nullptr, wxCB_READONLY, wxDefaultValidator);

	horSizer_5->Add(tubeVoltageStatictext, 0, wxALL, 5);
	horSizer_5->Add(tubeVoltage, 0, wxALL, 5);

	ctRB = new wxRadioButton(control, wxID_ANY, wxT("CT"), wxDefaultPosition, wxDefaultSize, wxRB_GROUP);
	ctRB->SetValue(true);
	radRB = new wxRadioButton(control, wxID_ANY, wxT("RAD"), wxDefaultPosition, wxDefaultSize);

	horSizer_6->Add(ctRB, 0, wxALL, 5);
	horSizer_6->Add(radRB, 0, wxALL, 5);

	okButton = new wxButton(control, wxID_ANY, wxT("OK"), wxDefaultPosition,
		wxSize(buttonWidth, buttonHeight), 0, wxDefaultValidator, wxT("OK"));
	okButton->Bind(wxEVT_BUTTON, &GeneralDialog::okFirstDialog, this);
	cancelButton = new wxButton(control, wxID_ANY, wxT("Cancel"), wxDefaultPosition,
		wxSize(buttonWidth, buttonHeight), 0, wxDefaultValidator, wxT("Cancel"));
	cancelButton->Bind(wxEVT_BUTTON, &GeneralDialog::cancelFirstDialog, this);

	horSizer_7->Add(okButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);
	horSizer_7->Add(cancelButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);

	controlVerSizer->Add(horSizer_1, 0, wxRIGHT | wxEXPAND, 5);
	controlVerSizer->Add(horSizer_2, 0, wxRIGHT | wxEXPAND, 5);
	controlVerSizer->Add(horSizer_3, 0, wxRIGHT, 5);
	controlVerSizer->Add(horSizer_4, 0, wxRIGHT, 5);
	controlVerSizer->Add(horSizer_5, 0, wxRIGHT, 5);
	controlVerSizer->Add(horSizer_6, 0, wxRIGHT | wxALIGN_CENTER, 5);
	controlVerSizer->Add(horSizer_7, 0, wxALL | wxALIGN_CENTER, 30);

	control->SetSizer(controlVerSizer);
	controlVerSizer->Fit(control);
	controlVerSizer->SetSizeHints(control);

	topSizerHor->Add(control, 1, wxALL | wxEXPAND);
	topSizerVert->Add(topSizerHor, 1, wxALL | wxEXPAND);

	SetSizer(topSizerVert);
	topSizerVert->Fit(this);
	topSizerVert->SetSizeHints(this);

	Centre();
	this->Bind(wxEVT_CLOSE_WINDOW, &GeneralDialog::onClose, this);
}

void GeneralDialog::onClose(wxCloseEvent& event)
{
	presenter->setWindowPointerToNull(GENERAL_DIALOG);

	if (!presenter->simulationFrameExists())
	{
		presenter->~Presenter();
	}

	event.Skip();
}

void GeneralDialog::cancelFirstDialog(wxCommandEvent& event)
{
	presenter->setWindowPointerToNull(GENERAL_DIALOG);
	this->Destroy();

	if (!presenter->simulationFrameExists())
	{
		this->Destroy();
		presenter->~Presenter();
	}
}

void GeneralDialog::onRandomSeedSelected(wxCommandEvent& event)
{
	if (randomSeedRB[2]->GetValue())
	{
		randomSeed->Enable();
	}
	else {
		randomSeed->Disable();
		randomSeed->SetValue(wxT("0"));
	}
}

void GeneralDialog::okFirstDialog(wxCommandEvent& event)
{
	wxString message = "";
	bool errors = false;
	
	currentTimeProduct->Validate();
	noOfPhontons->Validate();
	randomSeed->Validate();
	
	if (spectrumFileName->GetSelection() == -1) 
	{ 
		message += "Spectrum file is not selected.\n";
		errors = true;
	}

	if (phantomFileName->GetSelection() == -1) 
	{ 
		message += "Phantom file is not selected.\n"; 
		errors = true;
	}

	if (tubeVoltage->GetSelection() == -1) 
	{
		message += "Tube voltage is not selected.\n";
		errors = true;
	}
	if (currentTimeProduct->IsEmpty()) { 
		message += "Invalid value: current time product.\n"; 
		errors = true;
	}

	if (noOfPhontons->IsEmpty()) { 
		message += "Invalid value: number of photons.\n"; 
		errors = true;
	}

	if (randomSeed->IsEmpty()) { 
		message += "Invalid value: random seed.\n"; 
		errors = true;
	}
	
	if (errors)
	{
		wxMessageBox(message);
	}
	else
	{
		cInputData input = presenter->getInputSettings();

		if (radRB->GetValue())
		{
			input.systemType = input.rad;
		}
		else
		{
			input.systemType = input.ct;
		}

		input.spectrumFilename = "./Spectrums/" + spectrumFileName->GetValue().ToStdString();
		std::string tempPhantomFilename = input.phantomFilename;
		input.phantomFilename = "./Phantoms/" + phantomFileName->GetValue().ToStdString();
		tubeVoltage->GetValue().ToDouble(&input.tubeVoltage);
		currentTimeProduct->GetValue().ToDouble(&input.currentTimeProduct);
		noOfPhontons->GetValue().ToULongLong(&input.noOfPhotons);

		if (randomSeedRB[0]->GetValue())
		{
			input.randomSeedType = RANDOM_SEED_TYPE_DEFAULT;
			input.randomSeed = 0;
		}
		else if (randomSeedRB[1]->GetValue()) {
			input.randomSeedType = RANDOM_SEED_TYPE_CURRENT;
			input.randomSeed = std::time(0);
		}
		else {
			input.randomSeedType = RANDOM_SEED_TYPE_SPECIFIED;
			std::string value = randomSeed->GetValue().ToStdString();
			input.randomSeed = std::stoul(value);
		}

		presenter->loadPhantom(input);
		if (tempPhantomFilename != ("./Phantoms/" + phantomFileName->GetValue().ToStdString()))
		{
			input.setAllOrgansChecked(presenter->getOrgans());
		}
		presenter->setInputSettings(input);
		presenter->showStartSimulation();
		this->Destroy();
		presenter->setWindowPointerToNull(GENERAL_DIALOG);
	}
}

void GeneralDialog::readInputSettings()
{
	cInputData input = presenter->getInputSettings();

	std::string holder;
	holder = "./Spectrums/";
	int index = spectrumFileName->FindString(input.spectrumFilename.erase(0, holder.length()));
	spectrumFileName->SetSelection(index);

	holder = "./Phantoms/";
	index = phantomFileName->FindString(input.phantomFilename.erase(0, holder.length()));
	phantomFileName->SetSelection(index);

	if (input.randomSeedType == RANDOM_SEED_TYPE_DEFAULT)
	{
		randomSeedRB[0]->SetValue(true);
		randomSeed->ChangeValue("0");
		randomSeed->Disable();
	}
	else if (input.randomSeedType == RANDOM_SEED_TYPE_CURRENT)
	{
		randomSeedRB[1]->SetValue(true);
		randomSeed->ChangeValue("0");
		randomSeed->Disable();
	}
	else
	{
		randomSeedRB[2]->SetValue(true);
		randomSeed->ChangeValue(wxString::Format("%I32u", input.randomSeed));
		randomSeed->Enable();
	}

	noOfPhontons->ChangeValue(wxString::Format("%I64u", input.noOfPhotons));
	currentTimeProduct->ChangeValue(wxString::Format("%.2lf", input.currentTimeProduct));

	wxCommandEvent event(wxEVT_COMBOBOX, spectrumFileName->GetId());
	event.SetString(wxString(input.spectrumFilename));
	wxEvtHandler::ProcessEvent(event);
	index = tubeVoltage->FindString(wxString::Format("%lf", input.tubeVoltage));
	tubeVoltage->SetSelection(index);
	
	if (input.systemType == input.rad)
	{
		radRB->SetValue(true);
	}
	else
	{
		ctRB->SetValue(true);
	}
}

void GeneralDialog::updateVoltageValues(wxCommandEvent& event)
{
	std::vector<double> voltages;
	std::string test("test");
	spectrum->getVoltages("./Spectrums/" + event.GetString().ToStdString(), test, voltages);
	wxString* choices = new wxString[voltages.size()];

	if (!tubeVoltage->IsListEmpty())
	{
		tubeVoltage->Clear();
	}

	for (size_t i = 0; i < voltages.size(); i++)
	{
		*(choices + i) = wxString::Format(wxT("%lf"), voltages.at(i));
		tubeVoltage->Insert(*(choices + i), i);
	}

	event.Skip();
}

wxString* GeneralDialog::getListOfFilesInDirectory(std::string path, int* size)
{
	*size = 0;
	wxString* choices = nullptr;

	for (const auto& entry : fs::directory_iterator(path)) {
		(*size)++;
		wxString* temp = new wxString[*size];
		*(temp + (*size - 1)) = wxString(getFileName(entry.path().string()));

		if (choices == nullptr)
		{
			choices = temp;
		}
		else
		{
			// memcpy(temp, choices, (*size - 1) * sizeof(wxString));
			for (int i = 0; i < (*size - 1); i++)
			{
				*(temp + i) = *(choices + i);
			}
			delete[] choices;
			choices = temp;
		}
	}
	return choices;
}

wxString GeneralDialog::getFileName(wxString path)
{
	int lastDelimeterIndex = 0;

	for (size_t i = 0; i < path.Length(); i++)
	{
		if (path[i] == '\\' || path[i] == '/')
		{
			lastDelimeterIndex = i;
		}
	}

	return path.substr(lastDelimeterIndex + 1, path.Length());
}

void GeneralDialog::maxLengthEvent(wxCommandEvent& event)
{
	event.Skip();
	return;
}

GeneralDialog::~GeneralDialog()
{
	delete spectrum;
	delete doubleValidator, uint64Validator, uint32Validator;
	delete randomSeed, randomSeedStaticText;
	delete noOfPhontons, noOfPhontonsStaticText;
	delete currentTimeProduct, currentTimeProductStaticText;
	delete tubeVoltage, tubeVoltageStatictext;
	delete radRB, ctRB;
	delete okButton, cancelButton;
	delete spectrumFileName, spectrumChoices, spectrumStaticText;
	delete phantomFileName, phantomChoices, phantomStaticText;
	delete control;
}