#include "ResultsWindow.h"

ResultsWindow::ResultsWindow(wxNotebook* parent, std::vector<double> energyO, std::vector<cOrgan> organ) :
	wxPanel(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL | wxNO_BORDER, wxT("PANEL"))
{
	wxListCtrl* listControl = new wxListCtrl(parent, ID_RESULTS, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_SINGLE_SEL);
	listControl->InsertColumn(0, wxT("Organ"), wxLIST_FORMAT_LEFT, wxLIST_AUTOSIZE_USEHEADER);
	listControl->InsertColumn(1, wxT("absorbed energies"), wxLIST_FORMAT_LEFT, wxLIST_AUTOSIZE_USEHEADER);


	for (int i = 0; i < organ.size(); i++)
	{
		long itemIndex = listControl->InsertItem(0, wxString(("%i"), i));
		listControl->SetItem(itemIndex, 0, organ.at(i).name);
		listControl->SetItem(itemIndex, 1, wxString::Format(("%lf"), energyO.at(i)));
	}
}

ResultsWindow::~ResultsWindow()
{
	delete listControl;
}