#ifndef MYGLCANVAS_H_INCLUDED
#define MYGLCANVAS_H_INCLUDED

#include <GL/glew.h>
#include <wx/wx.h>
#include <wx/frame.h>
#include <wx/glcanvas.h>
#include <gl/gl.h>
#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include "../OpenGL/ShaderProgram.h"
#include "../OpenGL/Camera.h"
#include <wx/timer.h>
#include <wx/event.h>
#include <wx/stopwatch.h>
#include <wx/notebook.h>
#include "../Physics/phantom.h"
#include "../OpenGL/Meshes/PhantomMesh.h"
#include "../OpenGL/Meshes/PositionArrow.h"
#include "../OpenGL/Meshes/RotationArrow.h"
#include "../OpenGL/Meshes/Plane.h"
#include "../OpenGL/Meshes/Ray.h"
#include "../OpenGL/Meshes/XRayMachine.h"
#include "../OpenGL/Meshes/RadXRayBeam.h"
#include "../OpenGL/Meshes/CtXRayBeam.h"
#include "../Helpers/Constants.h"
#include "../Controllers/Presenter.h"
#include "../Physics/inputData.h"
#include "../OpenGL/Meshes/GantryRing.h"

class Presenter;
class MyGLCanvas;

class RenderTimer : public wxTimer
{
private:
	MyGLCanvas* m_canvas;

public:
	RenderTimer(MyGLCanvas* canvas);
	void Notify();
	void Start();
};

class MyGLCanvas : public wxGLCanvas
{
public:
	MyGLCanvas(int ID, wxNotebook* parent, cInputData inputData, const wxGLAttributes attributes, wxSize size, wxPoint position);
	~MyGLCanvas();
	void draw(wxDC& dc);
	void drawRotationAxes(glm::mat4 model, glm::mat4 view, glm::mat4 projection);
	void drawMovementAxes(glm::mat4 model, glm::mat4 view, glm::mat4 projection);
	void paintEvent(wxPaintEvent& evt);
	void paintNow();
	int init();
	void onKeyPressed(wxKeyEvent& event);
	void onKeyUp(wxKeyEvent& event);
	void onMouseMove(wxMouseEvent& event);
	void onLeftMouseDown(wxMouseEvent& event);
	void onLeftMouseUp(wxMouseEvent& event);
	void onRightMouseDown(wxMouseEvent& event);
	void onRightMouseUp(wxMouseEvent& event);
	void onMouseWheel(wxMouseEvent& event);
	void onResize(wxSizeEvent& event);
	glm::vec3 getModelWorldPosition();
	void updateInputSettings(cInputData inputData);
	void loadPhantom(cInputData inputSettings);
	void loadXRayMachine(cInputData inputSettings);
	void calculateLocalRotationAxes();
	void rotateLocalRotationAxes(glm::vec3 axis, GLfloat currentPosition, GLfloat lastPosition);
	void drawCTBeam(glm::vec3 phantomOriginCoor, ShaderProgram shaderProgram);
	void drawRADBeam(glm::vec3 phantomOriginCoor);
	void drawPlane(glm::mat4 view, glm::mat4 projectionm, ShaderProgram shaderProgram);
	glm::mat4 initRotationMatrixCT(glm::mat4 initialXRayMachineRotationMatrix);
	glm::mat4 initRotationMatrixRAD(glm::mat4 initialXRayMachineRotationMatrix);

private:
	GLint m_windowHeight;
	GLint m_windowWidth;
	cInputData m_inputSettings;
	wxString m_xRayMachineType;
	wxGLContext* m_glContext= nullptr;
	GLfloat m_degree = 0.0f;
	RenderTimer* m_renderTimer;
	FPSCamera* m_fpsCamera;
	GLfloat m_mouseSpeed = 0.1f;
	GLfloat m_xLastPosition = 400.0, m_yLastPosition = 300.0;
	PhantomMesh* m_pantomMesh;
	glm::vec4 m_color = glm::vec4(0.310f, 0.747f, 0.185f, 1.0f);
	glm::vec3 m_phantomPosition = glm::vec3(0.0f, 1.0f, -5.0f);
	glm::mat4 initialPhantomRotationMatrix, phantomRotationMatrix, xRayBeamRotationMatrix, axisTransformation, xRayMachineRotationMatrix;
	glm::vec3 m_phantomRotationAngle = glm::vec3(0.0, 0.0, 0.0);
	GLubyte m_index = 0;
	glm::vec3 m_lastModelPosition;
	Ray* m_ray;
	Plane* m_plane;
	glm::vec3 m_planePosition;
	glm::vec3 m_planeNormal;
	Plane::Axis m_axesAlignment = Plane::Axis::XZ;
	PositionArrow* m_positionArrows;
	RotationArrow* m_rotationArrows;
	GLuint m_pressedAxisCode;
	XRayMachine* m_xRayMachine = nullptr;
	GantryRing* m_gantryRing = nullptr;
	glm::vec3 m_xRayMachinePosition;
	glm::vec3 m_xRayMachineRotationAngle = glm::vec3(0.0, 0.0, 0.0);
	GLfloat m_rotationAngleAroundPhantom = 0.0f;
	glm::vec3 rotationAxis = glm::vec3(0.0, 1.0f, 0.0f);
	glm::vec3 rotationAxisHolder = glm::vec3(0.0, 1.0f, 0.0f);
	glm::vec3 negativeX = glm::vec3(-1.0f, 0.0f, 0.0f);
	glm::vec3 negativeZ = glm::vec3(0.0f, 0.0f, -1.0f);
	glm::vec3 positiveY= glm::vec3(0.0f, 1.0f, 0.0f);
	const GLfloat MOUSE_SENSITIVITY = 0.05f;
	const GLdouble ZOOM_SENSITIVITY = -3.0;
	bool positionAxisIsSelected = false;
	bool phantomIsSelected = false;
	bool rightMouseClicked = false;
	bool leftMouseClicked = false;
	bool drawXRayBeam = false;


	DECLARE_EVENT_TABLE()
};

#endif // MYGLCANVAS_H_INCLUDED