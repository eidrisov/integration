#include "ListOfOrgansDialog.h"

BEGIN_EVENT_TABLE(ListOfOrgansDialog, wxDialog)

END_EVENT_TABLE()

ListOfOrgansDialog::ListOfOrgansDialog(MainFrame* parent, std::vector<cOrgan> organ) :
	wxDialog(parent, wxID_ANY, wxT("List of organs"), wxDefaultPosition, wxSize(400, 500), wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER)
{
	this->SetMinSize(wxSize(400, 400));
	presenter = parent->getPresenter();

	topSizer = new wxBoxSizer(wxVERTICAL);
	horSizer_1 = new wxBoxSizer(wxHORIZONTAL);
	horSizer_2 = new wxBoxSizer(wxHORIZONTAL);
	horSizer_3 = new wxBoxSizer(wxHORIZONTAL);

	wxArrayString org;

	for (int i = 0; i < organ.size(); i++)
	{
		org.Add(wxString(organ.at(i).name));
	}

	checkListBox = new wxCheckListBox();
	checkListBox->Create(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, org);
	horSizer_1->Add(checkListBox, 1, wxEXPAND);
	
	checkAll = new wxButton(this, wxID_ANY, wxT("Check all"), wxDefaultPosition);
	checkAll->Bind(wxEVT_BUTTON, &ListOfOrgansDialog::onCheckAll, this);
	uncheckAll = new wxButton(this, wxID_ANY, wxT("Unheck all"), wxDefaultPosition);
	uncheckAll->Bind(wxEVT_BUTTON, &ListOfOrgansDialog::onUncheckAll, this);
	okButton = new wxButton(this, wxID_ANY, wxT("OK"), wxDefaultPosition);
	okButton->Bind(wxEVT_BUTTON, &ListOfOrgansDialog::onOk, this);
	cancelButton = new wxButton(this, wxID_ANY, wxT("Cancel"), wxDefaultPosition);
	cancelButton->Bind(wxEVT_BUTTON, &ListOfOrgansDialog::onCancel, this);

	horSizer_2->Add(checkAll, 1, wxALL | wxALIGN_CENTER_VERTICAL);
	horSizer_2->Add(uncheckAll, 1, wxALL | wxALIGN_CENTER_VERTICAL);
	horSizer_3->Add(okButton, 1, wxALL | wxALIGN_CENTER_VERTICAL);
	horSizer_3->Add(cancelButton, 1, wxALL | wxALIGN_CENTER_VERTICAL);

	topSizer->Add(horSizer_1, 1, wxEXPAND);
	topSizer->Add(horSizer_2, 0, wxEXPAND);
	topSizer->Add(horSizer_3, 0, wxEXPAND);

	updateCheckBoxStatus();

	SetSizer(topSizer);
}

void ListOfOrgansDialog::updateCheckBoxStatus()
{
	cInputData inputSettings = presenter->getInputSettings();

	for (int i = 0; i < inputSettings.checkedOrgans.size(); i++)
	{
		auto it = inputSettings.checkedOrgans.find(i);
		checkListBox->Check(it->first, it->second);
	}
}

void ListOfOrgansDialog::onOk(wxCommandEvent& event)
{
	cInputData inputSettings = presenter->getInputSettings();
	unsigned int count = checkListBox->GetCount();

	for (int i = 0; i < count; i++)
	{
		auto it = inputSettings.checkedOrgans.find(i);
		if (checkListBox->IsChecked(i) != it->second)
		{
			inputSettings.setOrganStatus(true);

			if (checkListBox->IsChecked(i))
			{
				it->second = true;
			}
			else
			{
				it->second = false;
			}
		}
	}

	presenter->setInputSettings(inputSettings);
	this->Destroy();
}

void ListOfOrgansDialog::onCancel(wxCommandEvent& event)
{
	this->Destroy();
}

void ListOfOrgansDialog::onCheckAll(wxCommandEvent& event)
{
	unsigned int count = checkListBox->GetCount();

	for (int i = 0; i < count; i++)
	{
		checkListBox->Check(i, true);
	}
}

void ListOfOrgansDialog::onUncheckAll(wxCommandEvent& event)
{
	unsigned int count = checkListBox->GetCount();

	for (int i = 0; i < count; i++)
	{
		checkListBox->Check(i, false);
	}
}

ListOfOrgansDialog::~ListOfOrgansDialog()
{

}
