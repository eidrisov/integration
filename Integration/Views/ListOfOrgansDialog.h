#ifndef LISTOFORGANS_H_INCLUDED
#define LISTOFORGANS_H_INCLUDED

#include <wx/wx.h>
#include <wx/frame.h>
#include <wx/event.h>
#include <wx/dialog.h>
#include <wx/checkbox.h>
#include <wx/checklst.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/arrstr.h>
#include <wx/button.h>
#include "MainFrame.h"
#include "../Physics/phantom.h"
#include "../Controllers/Presenter.h"

class MainFrame;
class Presenter;

class ListOfOrgansDialog : public wxDialog
{
private:
	wxSizer* topSizer = nullptr, * horSizer_1 = nullptr, * horSizer_2 = nullptr, * horSizer_3 = nullptr;
	wxPanel* panel = nullptr;
	wxCheckListBox* checkListBox = nullptr;
	wxButton* okButton = nullptr, * cancelButton = nullptr, * checkAll = nullptr, * uncheckAll = nullptr;
	Presenter* presenter = nullptr;

public:
	ListOfOrgansDialog(MainFrame* parent, std::vector<cOrgan> organ);
	~ListOfOrgansDialog();
	void onOk(wxCommandEvent& event);
	void onCancel(wxCommandEvent& event);
	void onCheckAll(wxCommandEvent& event);
	void onUncheckAll(wxCommandEvent& event);
	void updateCheckBoxStatus();

	enum {
		ID_OK = wxID_HIGHEST + 1,
		ID_CANCEL
	};

	DECLARE_EVENT_TABLE()
};

#endif // LISTOFORGANS_H_INCLUDED

