#ifndef RESULTSWINDOW_H_INCLUDED
#define RESULTSWINDOW_H_INCLUDED

#include <wx/wx.h>
#include <wx/panel.h>
#include <iostream>
#include <string>
#include <wx/frame.h>
#include <wx/notebook.h>
#include <wx/listctrl.h>
#include "../Physics/inputData.h"
#include "../Physics/phantom.h"

class ResultsWindow : public wxPanel
{
public:
	ResultsWindow(wxNotebook* parent, std::vector<double> energyO, std::vector<cOrgan> organ);
	~ResultsWindow();

private:
	wxListCtrl* listControl = nullptr;

	enum {
		ID_RESULTS = wxID_HIGHEST + 1
	};
};

#endif // RESULTSWINDOW_H_INCLUDED