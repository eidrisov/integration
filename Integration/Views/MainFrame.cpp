#include "MainFrame.h"

BEGIN_EVENT_TABLE(MainFrame, wxFrame)
EVT_RIBBONTOOLBAR_CLICKED(ID_GENERAL_SETTINGS, MainFrame::onGeneralSettings)
EVT_RIBBONTOOLBAR_CLICKED(ID_SIMULATION_SPECIFIC_SETTINGS, MainFrame::onSimulationSpecificSettings)
EVT_RIBBONTOOLBAR_CLICKED(ID_START, MainFrame::onStart)
EVT_RIBBONTOOLBAR_CLICKED(ID_STOP, MainFrame::onStop)
EVT_RIBBONTOOLBAR_CLICKED(ID_SAVE_INPUT, MainFrame::saveInput)
EVT_RIBBONTOOLBAR_CLICKED(ID_EXPORT_OUTPUT, MainFrame::saveOutput)
EVT_RIBBONTOOLBAR_CLICKED(ID_IMPORT_INPUT, MainFrame::importInput)
EVT_RIBBONTOOLBAR_CLICKED(ID_HUMAN_BODY, MainFrame::showOrgans)
EVT_COMMAND(ID_SIMULATION_RESULTS, wxEVT_COMMAND_TEXT_UPDATED, MainFrame::addResultPage)
EVT_COMMAND(ID_FINISHED_THREAD, wxEVT_COMMAND_TEXT_UPDATED, MainFrame::finishedThread)
END_EVENT_TABLE()

MainFrame::MainFrame(Presenter* presenter, int width, int height) :
	wxFrame(NULL, wxID_ANY, wxT("Simulation"), wxPoint(0, 0), wxSize(width, height))
{
	m_presenter = presenter;

	wxGLAttributes m_attributes;
	m_attributes.PlatformDefaults().Depth(24).Stencil(8).DoubleBuffer().MinRGBA(8, 8, 8, 8).SampleBuffers(1).Samplers(4).FrameBuffersRGB().Level(0).EndList();
	generalSettingsIcon = readImage(wxString("./Icons/general-settings.png"));
	simulationSpecificlSettingsIcon = readImage(wxString("./Icons/simulation-specific-settings.png"));
	startIcon = readImage(wxString("./Icons/start.png"));
	stopIcon = readImage(wxString("./Icons/stop-red.png"));
	saveInputIcon = readImage(wxString("./Icons/actions-document-save.png"));
	importInputIcon = readImage(wxString("./Icons/import.png"));
	exportInputIcon = readImage(wxString("./Icons/export-to-file.png"));
	exportOutputIcon = readImage(wxString("./Icons/export.png"));
	humanBodyIcon = readImage(wxString("./Icons/human-body.png"));

	topSizer = new wxBoxSizer(wxVERTICAL);
	ribbonSizer = new wxBoxSizer(wxHORIZONTAL);
	noteBookSizer = new wxBoxSizer(wxHORIZONTAL);
	gaugeSizer = new wxBoxSizer(wxHORIZONTAL);
	simTextSizer = new wxBoxSizer(wxHORIZONTAL);

	ribbonBar = new wxRibbonBar(this, -1, wxDefaultPosition,wxDefaultSize, wxRIBBON_BAR_FLOW_HORIZONTAL
		| wxRIBBON_BAR_SHOW_PAGE_LABELS
		| wxRIBBON_BAR_SHOW_PANEL_EXT_BUTTONS
		| wxRIBBON_BAR_SHOW_TOGGLE_BUTTON
	);

	filePage = new wxRibbonPage(ribbonBar, wxID_ANY, wxT("File"), wxNullBitmap);
	settingsPage = new wxRibbonPage(ribbonBar, wxID_ANY, wxT("Settings"), wxNullBitmap);
	simulationPage = new wxRibbonPage(ribbonBar, wxID_ANY, wxT("Simulations"), wxNullBitmap);

	filePanel = new wxRibbonPanel(filePage, wxID_ANY, wxT(""),
		wxNullBitmap, wxDefaultPosition, wxDefaultSize,
		wxRIBBON_PANEL_DEFAULT_STYLE);
	settingsPanel = new wxRibbonPanel(settingsPage, wxID_ANY, wxT(""),
		wxNullBitmap, wxDefaultPosition, wxDefaultSize,
		wxRIBBON_PANEL_DEFAULT_STYLE);
	simulationPanel = new wxRibbonPanel(simulationPage, wxID_ANY, wxT(""),
		wxNullBitmap, wxDefaultPosition, wxDefaultSize,
		wxRIBBON_PANEL_DEFAULT_STYLE);


	fileBar = new wxRibbonToolBar(filePanel, wxID_ANY);
	fileBar->AddTool(ID_SAVE_INPUT, *saveInputIcon, wxNullBitmap, "Save input");
	fileBar->AddTool(ID_IMPORT_INPUT, *importInputIcon, wxNullBitmap, "Import input");
	// toolBar->AddTool(wxID_ANY, *exportInputIcon, "Export input.");
	fileBar->AddTool(ID_EXPORT_OUTPUT, *exportOutputIcon, wxNullBitmap, "Export output");
	
	settingsBar = new wxRibbonToolBar(settingsPanel, wxID_ANY);
	settingsBar->AddTool(ID_GENERAL_SETTINGS, *generalSettingsIcon, wxNullBitmap, "General dialog");
	settingsBar->AddTool(ID_SIMULATION_SPECIFIC_SETTINGS, *simulationSpecificlSettingsIcon, wxNullBitmap, "Simulation dialog");
	
	simulationBar = new wxRibbonToolBar(simulationPanel, wxID_ANY);
	simulationBar->AddTool(ID_START, *startIcon, wxNullBitmap, "Start");
	simulationBar->AddTool(ID_STOP, *stopIcon, wxNullBitmap, "Stop");
	simulationBar->AddTool(ID_HUMAN_BODY, *humanBodyIcon, wxNullBitmap, "Select organs");

	ribbonBar->Realize();
	ribbonSizer->Add(ribbonBar, 1, wxEXPAND);

	myNoteBook = new wxNotebook(this, wxID_ANY, wxDefaultPosition, wxSize(600, 600), 0, wxString("NoteBook"));
	myCanvas = new MyGLCanvas(ID_GL_CANVAS, myNoteBook, presenter->getInputSettings(), m_attributes, wxSize(600, 600), wxDefaultPosition);
	myNoteBook->AddPage(myCanvas, wxString("Phantom"));
	noteBookSizer->Add(myNoteBook, 1, wxEXPAND);

	simBarText = new wxStaticText(this, wxID_ANY, wxT("Simulation: "), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTER_HORIZONTAL);
	simBarText->SetBackgroundColour(wxColor(255, 255, 255));
	simTextSizer->Add(simBarText, 1, wxEXPAND);

	gauge = new wxGauge(this, wxID_ANY, 100, wxDefaultPosition, wxSize(60, 10), wxGA_SMOOTH, wxDefaultValidator, wxString("Simulation"));
	gaugeSizer->Add(gauge, 1, wxEXPAND);

	topSizer->Add(ribbonSizer, 0, wxEXPAND);
	topSizer->Add(noteBookSizer, 1, wxEXPAND);
	topSizer->Add(simTextSizer, 0, wxEXPAND);
	topSizer->Add(gaugeSizer, 0, wxEXPAND);

	SetSizer(topSizer);
	topSizer->Fit(this);
	topSizer->SetSizeHints(this);

	// wxMessageOutputDebug().Printf("init height: %d", this->GetSize().GetHeight());
	this->Maximize(true);
	this->Bind(wxEVT_CLOSE_WINDOW, &MainFrame::onClose, this);
}

MainFrame::~MainFrame()
{
	delete myNoteBook;
	delete generalSettingsIcon;
	delete simulationSpecificlSettingsIcon;
	delete startIcon;
	delete stopIcon;
	delete saveInputIcon;
	delete importInputIcon;
	delete exportOutputIcon;
	delete humanBodyIcon;
}

void MainFrame::showOrgans(wxRibbonToolBarEvent& event)
{
	ListOfOrgansDialog* listOfOrgansDialog = new ListOfOrgansDialog(this, m_presenter->getOrgans());
	listOfOrgansDialog->ShowModal();
}

void MainFrame::updateGLCanvasInputSettings(cInputData inputSettings)
{
	myCanvas->updateInputSettings(inputSettings);
}

void MainFrame::importInput(wxRibbonToolBarEvent& event)
{
	wxFileDialog fileDialog(this, wxT(""), wxEmptyString, wxEmptyString, "CSV files (*.csv)|*.csv|TXT files (*.txt)|*.txt", wxFD_OPEN);

	if (fileDialog.ShowModal() == wxID_OK)
	{
		m_presenter->importInput(fileDialog.GetPath(), ID_REDRAW);
	}
}

void MainFrame::saveInput(wxRibbonToolBarEvent& event)
{
	wxFileDialog fileDialog(this, wxT(""), wxEmptyString, wxEmptyString, "CSV files (*.csv)|*.csv|TXT files (*.txt)|*.txt", wxFD_SAVE);

	if (fileDialog.ShowModal() == wxID_OK)
	{
		std::thread saveInputToFileThread(&Presenter::saveInputToAFile, m_presenter, fileDialog.GetPath(), fileDialog.GetFilename());
		saveInputToFileThread.detach();
	}
}

void MainFrame::saveOutput(wxRibbonToolBarEvent& event)
{
	wxFileDialog fileDialog(this, wxT(""), wxEmptyString, wxEmptyString, "CSV files (*.csv)|*.csv|TXT files (*.txt)|*.txt", wxFD_SAVE);

	if (fileDialog.ShowModal() == wxID_OK)
	{
		std::thread saveOutputToFileThread(&Presenter::saveOutputToAFile, m_presenter, fileDialog.GetPath(), fileDialog.GetFilename());
		saveOutputToFileThread.detach();
	}
}

void MainFrame::addResultPage(wxCommandEvent& event)
{
	SimulationResults simulationResults = *((SimulationResults*)event.GetClientData());
	
	for (int i = myNoteBook->GetPageCount() - 1  ; i > 0 ; i--)
	{
		wxWindow* myNoteBookPage = myNoteBook->GetPage(i);
		if (myNoteBookPage->GetId() != ID_GL_CANVAS)
		{
			myNoteBook->DeletePage(i);
		}
	}

	organDoseLC = new wxListCtrl(myNoteBook, ID_ORGAN_DOSE, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_SINGLE_SEL);
	organDoseLC->InsertColumn(0, wxT("Organ"), wxLIST_FORMAT_LEFT, wxLIST_AUTOSIZE);
	organDoseLC->InsertColumn(1, wxT("Dose (mGy)"), wxLIST_FORMAT_LEFT, wxLIST_AUTOSIZE);
	organDoseLC->InsertColumn(2, wxT("Accuracy (%)"), wxLIST_FORMAT_LEFT, wxLIST_AUTOSIZE);

	tissueDoseLC = new wxListCtrl(myNoteBook, ID_EFFECTIVE_DOSE, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_SINGLE_SEL);
	tissueDoseLC->InsertColumn(0, wxT("Organ"), wxLIST_FORMAT_LEFT, wxLIST_AUTOSIZE);
	tissueDoseLC->InsertColumn(1, wxT("Dose (mSv)"), wxLIST_FORMAT_LEFT, wxLIST_AUTOSIZE);
	tissueDoseLC->InsertColumn(2, wxT("Accuracy (%)"), wxLIST_FORMAT_LEFT, wxLIST_AUTOSIZE);

	for (int i = 0; i < simulationResults.getSizeOfOrgan(); i++)
	{
		long itemIndex = organDoseLC->InsertItem(0, wxString(("%i"), i));
		double dose = simulationResults.getDoseOfOrganDose(i);
		organDoseLC->SetItem(itemIndex, 0, simulationResults.getOrgan(i).name);
		organDoseLC->SetItem(itemIndex, 1, wxString::Format(("%lf"), dose));
		if (dose == 0)
		{
			organDoseLC->SetItem(itemIndex, 2, wxString("NA"));
		} 
		else
		{
			organDoseLC->SetItem(itemIndex, 2, wxString::Format(("%lf"), simulationResults.getAccuracyOfOrganDose(i)));
		}
	}

	organDoseLC->SetColumnWidth(0, wxLIST_AUTOSIZE);
	organDoseLC->SetColumnWidth(1, wxLIST_AUTOSIZE);
	organDoseLC->SetColumnWidth(2, wxLIST_AUTOSIZE);

	for (int i = 0; i < simulationResults.getSizeOfEffectiveDoseTissue(); i++)
	{
		long itemIndex = tissueDoseLC->InsertItem(0, wxString(("%i"), i));
		double dose = simulationResults.getDoseOfEffectiveDose(i);
		tissueDoseLC->SetItem(itemIndex, 0, simulationResults.getEffectiveDoseTissue(i).name);
		tissueDoseLC->SetItem(itemIndex, 1, wxString::Format(("%lf"), dose));
		if (dose == 0)
		{
			tissueDoseLC->SetItem(itemIndex, 2, wxString("NA"));
		}
		else
		{
			tissueDoseLC->SetItem(itemIndex, 2, wxString::Format(("%lf"), simulationResults.getAccuracyOfEffectiveDose(i)));
		}
	}

	long itemIndex = tissueDoseLC->InsertItem(0, wxString(("%i"), simulationResults.getSizeOfEffectiveDoseTissue()));
	tissueDoseLC->SetItem(itemIndex, 0, wxString("Effective dose"));
	tissueDoseLC->SetItem(itemIndex, 1, wxString::Format(("%lf"), 1000 * simulationResults.getE()));
	tissueDoseLC->SetItem(itemIndex, 2, wxString::Format(("%lf"), simulationResults.getEStdev() / simulationResults.getE() * 100.0f));

	tissueDoseLC->SetColumnWidth(0, wxLIST_AUTOSIZE);
	tissueDoseLC->SetColumnWidth(1, wxLIST_AUTOSIZE);
	tissueDoseLC->SetColumnWidth(2, wxLIST_AUTOSIZE);

	myNoteBook->AddPage(organDoseLC, L"Organ dose");
	myNoteBook->AddPage(tissueDoseLC, L"Tissue dose");
}

void MainFrame::onGeneralSettings(wxRibbonToolBarEvent& event)
{
	m_presenter->createGeneralDialog();
}

void MainFrame::onSimulationSpecificSettings(wxRibbonToolBarEvent& event)
{
	m_presenter->createXRayMachineDialog();
}

void MainFrame::onStart(wxRibbonToolBarEvent& event)
{
	gauge->SetValue(0);
	simulationBar->EnableTool(ID_START, false);
	settingsBar->EnableTool(ID_GENERAL_SETTINGS, false);
	settingsBar->EnableTool(ID_SIMULATION_SPECIFIC_SETTINGS, false);
	stopThread.store(false);
	simulationThread = std::thread(&Presenter::startSimulation, m_presenter, &stopThread, ID_SIMULATION_RESULTS, ID_FINISHED_THREAD);
	simulationThread.detach();
}

void MainFrame::onStop(wxRibbonToolBarEvent& event)
{
	stopThread.store(true);
}

void MainFrame::finishedThread(wxCommandEvent& event)
{
	simulationBar->EnableTool(ID_START, true);
	settingsBar->EnableTool(ID_GENERAL_SETTINGS, true);
	settingsBar->EnableTool(ID_SIMULATION_SPECIFIC_SETTINGS, true);

	if (!stopThread.load())
	{
		simBarText->SetLabelText(wxString("Simulation (100%):"));
		simBarText->SetSize(this->GetSize().x, wxDefaultSize.y);
		gauge->SetValue(100);
	}
}

void MainFrame::setProgressBar(int value)
{
	simBarText->SetLabelText(wxString("Simulation ") + wxString::Format(wxT("(%i%%):"), value));
	simBarText->SetSize(this->GetSize().x, wxDefaultSize.y);
	gauge->SetValue(value);
}

wxBitmap* MainFrame::readImage(wxString filename)
{
	wxInitAllImageHandlers();
	wxImage image(filename, wxBITMAP_TYPE_PNG, -1);
	// return new wxBitmap(image, 48, 48);
	return new wxBitmap(image);
}

void MainFrame::showNoResultsMessage()
{
	wxMessageBox("No results are available.");
}

Presenter* MainFrame::getPresenter()
{
	return m_presenter;
}

void MainFrame::onClose(wxCloseEvent& event)
{
	m_presenter->closeDialogWindows();
	m_presenter->setWindowPointerToNull(MAIN_FRAME);
	event.Skip();
}