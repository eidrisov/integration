#ifndef RADDIALOG_H_INCLUDED
#define RADDIALOG_H_INCLUDED

#include <wx/wx.h>
#include <wx/frame.h>
#include <string>
#include <iostream>
#include <filesystem>
#include <wx/button.h>
#include <wx/radiobut.h>
#include <wx/textctrl.h>
#include <wx/stattext.h>
#include <wx/valnum.h>
#include <wx/event.h>
#include <wx/sizer.h>
#include "../Controllers/Presenter.h"
#include "../Physics/inputData.h"

class Presenter;

class RadDialog : public wxFrame
{
private:
	wxControl* control = nullptr;
	wxButton* okButton = nullptr;
	wxTextCtrl* tubePos[3], *collOpening[2], *collDistance, *tubeRot[3];
	wxStaticText* tubePosStaticText[4], *collOpeningStaticText[3], *collDistanceStaticText,
		*tubeRotStatictext[4], * shiftStaticText = nullptr;
	int padding = 10, staticTextWidth = 30, staticTextHeight = 30, textCtrlWidth = 70, textCtrlHeight = 23,
		buttonWidth = 75, buttonHeight = 22;
	Presenter* presenter = nullptr;
	wxFloatingPointValidator<double>* doubleValidator = nullptr;
	wxSizer* topSizerHor = nullptr, * topSizerVert = nullptr, * controlVerSizer = nullptr,
		* horSizer[9];

public:
	RadDialog(Presenter* presenter);
	~RadDialog();
	void onOk(wxCommandEvent& event);
	void onBack(wxCommandEvent& event);
	void destroyStaticTextArray(wxStaticText* arrayOfStaticText);
	void destroyTextCtrlArray(wxTextCtrl* arrayOfTextCtl);
	void onClose(wxCloseEvent& event);
	void readInputSettings();

	DECLARE_EVENT_TABLE()
};

#endif // RADDIALOG_H_INCLUDED