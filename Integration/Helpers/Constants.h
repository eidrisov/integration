#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <iostream>
#include <string>

extern const std::string SPECTRUM_FILE_NAME;
extern const std::string TUBE_VOLTAGE;
extern const std::string CURRENT_TIME_PRODUCT;
extern const std::string NUMBER_OF_PHOTONS;
extern const std::string RANDOM_SEED_TYPE;
extern const std::string RANDOM_SEED_TYPE_DEFAULT;
extern const std::string RANDOM_SEED_TYPE_CURRENT;
extern const std::string RANDOM_SEED_TYPE_SPECIFIED;
extern const std::string RANDOM_SEED;
extern const std::string PHANTOM_FILE_NAME;
extern const std::string SYSTEM_TYPE;
extern const std::string TUBE_POS;
extern const std::string TUBE_POS_X;
extern const std::string TUBE_POS_Y;
extern const std::string TUBE_POS_Z;
extern const std::string COLLIMATOR_OPENING;
extern const std::string COLLIMATOR_OPENING_X;
extern const std::string COLLIMATOR_OPENING_Y;
extern const std::string COLLIMATOR_DISTANCE;
extern const std::string TUBE_ROT;
extern const std::string TUBE_ROT_X;
extern const std::string TUBE_ROT_Y;
extern const std::string TUBE_ROT_Z;
extern const std::string RADIUS_CT;
extern const std::string FAN_ANGLE_THICKNESS;
extern const std::string FAN_ANGLE_WIDTH;
extern const std::string GANTRY_POS;
extern const std::string GANTRY_POS_X;
extern const std::string GANTRY_POS_Y;
extern const std::string GANTRY_POS_Z;
extern const std::string GANTRY_ROT;
extern const std::string GANTRY_ROT_X;
extern const std::string GANTRY_ROT_Y;
extern const std::string GANTRY_ROT_Z;
extern const std::string CT;
extern const std::string RAD;
extern const std::string DOUBLE_TYPE;
extern const std::string STRING_TYPE;
extern const std::string UINT64_T_TYPE;
extern const std::string GENERAL_INPUT;
extern const std::string RAD_INPUT;
extern const std::string CT_INPUT;
extern const std::string RAD_DIALOG;
extern const std::string CT_DIALOG;
extern const std::string GENERAL_DIALOG;
extern const std::string MAIN_FRAME;
extern const std::string ORGAN_INPUT;

#endif // CONSTANTS_H